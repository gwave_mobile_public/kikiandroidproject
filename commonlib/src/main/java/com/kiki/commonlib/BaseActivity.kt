package com.kiki.commonlib

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

abstract class BaseActivity : AppCompatActivity() {

    lateinit var mContext:Context
    private var toolbar: Toolbar?=null
    private var leftTitle: TextView? = null
    private var rightTitle: TextView? = null
    private var rightIcon: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext=this
       if (showToolBar()){
           setContentView(R.layout.ac_base_fit_layout)
           initDefaultView()
           toolbar?.visibility= View.VISIBLE
       }else{
           setContentView(R.layout.ac_base_not_fit_layout)
           initDefaultView()
           toolbar?.visibility= View.GONE
       }
        diyWindow()
        init()
    }

    /** init  */
    abstract fun init()

   private fun initDefaultView(){
        toolbar = findViewById(R.id.toolbar_base)
       Log.i("Activity", "initDefaultView: ---->${toolbar==null}")
        leftTitle = findViewById(R.id.left_tv)
        rightTitle = findViewById(R.id.right_tv)
        rightIcon = findViewById(R.id.aciv_right)
        val container = findViewById<FrameLayout>(R.id.fl_activity_child_container)
        toolbar?.setNavigationOnClickListener { leftBackClicked() }
        val layoutView = getLayoutView() ?: throw RuntimeException("需要重写 getLayoutView 方法")
        layoutView?.let {
            container.addView(layoutView)
        }
    }


    open fun showToolBar():Boolean {
        return true
    }


    // 动态设置tooBar 左边的标题
   open fun setLeftTitle(textId: Int) {
        if (leftTitle != null) {
            leftTitle?.text = getString(textId)
        }
    }

    // 动态设置tooBar 左边的标题
    open fun setLeftTitle(leftTitleString: String?) {
        if (leftTitle != null) {
            leftTitle?.text = leftTitleString
        }
    }


    // 动态设置tooBar 最右边的标题
    open fun setRightTitle(titleText: String?) {
        if (rightTitle != null) {
            rightTitle?.text = titleText
        }
    }

    open fun setRightTitle(textId: Int) {
        if (rightTitle != null) {
            rightTitle?.text = getString(textId)
        }
    }

    open  fun setRightTitleColor(colorId: Int) {
        if (rightTitle != null) {
            rightTitle?.setTextColor(ContextCompat.getColor(this,colorId))
        }
    }

    open  fun leftBackClicked(){
        finish()
    }

    abstract fun getLayoutView(): View

    /** Set transparent immersion bar : white backgrand black text  */
    open fun diyWindow() {
        // other one write
        val window = window
        if (keepScreenOn()) {
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
        window.statusBarColor = Color.TRANSPARENT
        setNavigationBarColor(Color.WHITE)
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR)
    }

    /** 是否保持屏幕常亮的钩子  */
    private fun keepScreenOn(): Boolean {
        return false
    }

    private fun setNavigationBarColor(colorId: Int) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            window.navigationBarColor = colorId
        }
    }

    // 修改顶部系统导航栏颜色
    open fun setStatusBarColor(colorId: Int) {
        // other one write
        window.addFlags(FLAG_TRANSLUCENT_NAVIGATION)
        window.statusBarColor = colorId
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }
}