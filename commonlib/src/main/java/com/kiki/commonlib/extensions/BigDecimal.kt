package com.kiki.commonlib.extensions

import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import java.text.DecimalFormat

// BigDecimal 取有效位数
@JvmOverloads
fun BigDecimal.validDigit(validNum: Int = 6): BigDecimal {
  val mc = MathContext(validNum, RoundingMode.HALF_UP)
  val data = BigDecimal(this.toPlainString(), mc)
  return if (data.compareTo(BigDecimal.ZERO) == 0) {
    BigDecimal("0.00")
  } else {
    data
  }
}

// 千分位格式化，保留两位小数
@JvmOverloads
fun BigDecimal.numFormat(): String {
  val df = DecimalFormat("#,##0.00")
  return df.format(BigDecimal(this.toPlainString()))
}

@JvmOverloads
fun BigDecimal.getScale(scaleNum: Int): String {
  return this.setScale(scaleNum, RoundingMode.DOWN).toPlainString()
}
