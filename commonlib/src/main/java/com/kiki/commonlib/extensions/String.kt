package com.kiki.commonlib.extensions

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

const val SHOW_DECIMAL_PLACES = 8

/**
 * 裁切多余位数的小数
 * @param decimal 保留的小数位数
 */
@JvmOverloads
fun String.interceptDecimal(decimal: Int = SHOW_DECIMAL_PLACES): String {
  val amountNumber: Int = this.lastIndexOf('.')
  if (amountNumber == -1) {
    return this
  }
  val amountDecimal: Int = this.length - amountNumber - 1
  return if (amountDecimal > decimal) {
    this.substring(0, amountNumber + decimal + 1)
  } else {
    this
  }
}

/**
 * 截取字符串后 N 位
 */
@JvmOverloads
fun String.cutTheLast(digits: Int = 4): String {
  if (this.length > digits) {
    return this.substring(this.length - digits)
  } else {
    return this
  }
}

/**
 * 截取字符串前 N 位
 */
@JvmOverloads
fun String.cutTheFirst(digits: Int = 10, lastInterval: String): String {
  if (digits < this.length) {
    return this.substring(0, digits) + lastInterval
  } else {
    return this
  }
}

fun String.catLastChar(char: Char): String {
  if (this.isNotEmpty() && this.last() == char) {
    return this.substring(0, this.length - 1)
  }
  return this
}

/**
 * 字符串长度大于 [firstCount] + [lastCount] ,则截取前 [firstCount] 个字符，后 [lastCount] 个字符，中间用 [interval] 填充
 */
@JvmOverloads
fun String.intervalOmit(firstCount: Int, lastCount: Int = firstCount, interval: String = "..."): String {
  return if (this.length < firstCount + lastCount) {
    this
  } else {
    "${this.substring(0, firstCount)}$interval${this.substring(this.length - lastCount)}"
  }
}
