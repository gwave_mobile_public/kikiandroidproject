package com.kiki.commonlib.extensions

import android.app.Activity
import android.content.Intent


/**
 * 作者：王豪
 * 日期：2021/6/30
 * 描述：对Activity的扩展
 */

inline fun <reified T> Activity.startActivity(noinline lambda: (Intent.() -> Unit)? = null) {
  val intent = Intent(this, T::class.java)
  lambda?.invoke(intent)
  startActivity(intent)
}

fun <T> Activity.lazyOptArgument(key: String, default: T) = lazy {
  val result = intent?.getSerializableExtra(key) ?: default
  result as T
}
