package com.kiki.commonlib.extensions

import java.math.BigInteger

/**
 *
 * @Author:         peter Qin
 *
 */

fun uInt256Max(): String {
  return BigInteger(
      "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff",
      16)
      .toString()
}
