package com.kiki.commonlib.extensions

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * @Description:     java类作用描述
 * @Author:         peter Qin
 *
 */

@SuppressLint("SimpleDateFormat")
fun Long.toFormatTime(): String {
  return SimpleDateFormat("yyyy/MM/dd HH:mm").format(Date(this * 1000))
}
