package com.kiki.commonlib
import android.util.Log
import android.view.View
import com.kiki.commonlib.databinding.ActivityCommonMainBinding

class CommonMainActivity : BaseActivity() {

    private lateinit var binding:ActivityCommonMainBinding

    override fun init() {
        Log.i("Activity", "init: ")
    }

    override fun getLayoutView(): View {
        binding = ActivityCommonMainBinding.inflate(layoutInflater)
        return binding.root
    }

}

