import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Build

/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/10/27 at 4:18 下午
 * @Description:
 */

object Utils {

    fun getVersionName(context: Context): String? {
        val pm = context.packageManager
        try {
            val packageInfo = pm.getPackageInfo(context.packageName, 0)
            return packageInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return null
    }


    fun getAppName(context: Context): String? {
        val pm = context.packageManager
        try {
            val packageInfo = pm.getPackageInfo(context.packageName, 0)
            val applicationInfo: ApplicationInfo = packageInfo.applicationInfo
            val labelRes: Int = applicationInfo.labelRes
            return context.resources.getString(labelRes)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return null
    }

    fun getSystemModel(): String? {
        return Build.MODEL
    }

    fun getDeviceBrand(): String? {
        return Build.BRAND
    }

    fun getSystemVersion(): String? {
        return Build.VERSION.RELEASE
    }

}