# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-keep public class com.kikitrade.kikikline.view.** { *; }
-keep public class com.kikitrade.kikikline.config.KKCryptoChartConstants { public *;}
-keep public class com.kikitrade.kikikline.config.ChartManager { public *;}
-keep public class com.kikitrade.kikikline.config.CryptoChartGlobalConfig { public *;}
-keep public class com.kikitrade.kikikline.model.** { *; }
-keep public class com.kikitrade.kikikline.websocket.** { *; }

# 未指定成员，仅仅保持类名不被混淆
-keep public class * extends android.app.Service

-keep class android.support.** {*;}
-dontwarn android.support.**
-keep interface android.support.** { *; }

-keep class androidx.** {*;}
-keep interface androidx.** {*;}
-keep class * extends androidx.**  { *; }
-dontwarn androidx.**

-keep class **.R$* {*;}
-keep class **.R{ *; }

# 保持 native 方法不被混淆
-keepclasseswithmembernames class * {
    native <methods>;
}


 # 保持枚举 enum 类不被混淆
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

#实现了android.os.Parcelable接口类的任何类，以及其内部定义的Creator内部类类型的public final静态成员变量，都不能被混淆和删除
-keep class * implements android.os.Parcelable {    # 保持Parcelable不被混淆
  public static final android.os.Parcelable$Creator *;
}


#-dontwarn com.squareup.okhttp3.**
#-keep class com.squareup.okhttp3.** { *;}
#-keep interface com.squareup.okhttp3.** { *;}
#-dontwarn okio.**
#-keep class okio.**{*;}
#-keep interface okio.**{*;}


-keep public class com.icechao.klinelib.** { *; }

#-obfuscationdictionary filename.txt
#-classobfuscationdictionary filename.txt
#-packageobfuscationdictionary filename.txt