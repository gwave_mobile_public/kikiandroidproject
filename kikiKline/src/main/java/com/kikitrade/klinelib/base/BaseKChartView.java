package com.kikitrade.klinelib.base;

import android.animation.ValueAnimator;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import androidx.core.view.GestureDetectorCompat;

import com.kikitrade.kikikline.config.KikiShowType;
import com.kikitrade.kikikline.model.KChartNewBean;
import com.kikitrade.kikikline.utils.Utils;
import com.kikitrade.klinelib.adapter.BaseKLineChartAdapter;
import com.kikitrade.klinelib.callback.IMaxMinDeal;
import com.kikitrade.klinelib.callback.OnSelectedChangedListener;
import com.kikitrade.klinelib.callback.PriceLabelInLineClickListener;
import com.kikitrade.klinelib.callback.SlidListener;
import com.kikitrade.klinelib.formatter.DateFormatter;
import com.kikitrade.klinelib.formatter.IDateTimeFormatter;
import com.kikitrade.klinelib.formatter.IValueFormatter;
import com.kikitrade.klinelib.formatter.IYValueFormatter;
import com.kikitrade.klinelib.formatter.ValueFormatter;
import com.kikitrade.klinelib.idraw.IDrawShape;
import com.kikitrade.klinelib.model.KLineEntity;
import com.kikitrade.klinelib.render.KDJRender;
import com.kikitrade.klinelib.render.MACDRender;
import com.kikitrade.klinelib.render.MainRender;
import com.kikitrade.klinelib.render.RSIRender;
import com.kikitrade.klinelib.render.VolumeRender;
import com.kikitrade.klinelib.render.WRRender;
import com.kikitrade.klinelib.utils.Constants;
import com.kikitrade.klinelib.utils.LogUtil;
import com.kikitrade.klinelib.utils.Status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*************************************************************************
 * Description   : 绘制分类,BaseKLineChartView只绘制所有视图通用的图形 其他区域的绘制分别由对应的绘制类完成
 *
 * @PackageName  : com.icechao.klinelib.base
 * @FileName     : BaseKLineChartView.java
 * @Author       : chao
 * @Date         : 2019/4/8
 * @Email        : icechliu@gmail.com
 * @version      : V2
 *************************************************************************/
public abstract class BaseKChartView extends ScrollAndScaleView {

    /**
     * 当前正在绘制的图形
     */
    protected IDrawShape drawShape;
    /**
     * 是否以动画的方式绘制最后一根线
     */
    protected boolean isAnimationLast = false;
    /**
     * 是否以动画的方式绘制最后一根线
     */
    protected boolean loadDataWithAnim = false;

    protected Bitmap logoBitmap;

    /**
     * 最大最小值处理工具类
     */
    protected IMaxMinDeal maxMinDeal;

    /**
     * 是否正在显示loading
     */
    protected boolean isShowLoading;

    /**
     * 动画执行时长
     */
    protected long duration = 400;


    /**
     * 绘制K线时画板平移的距离
     */
    private float canvasTranslateX;

    protected float viewWidth, viewHeight;
    protected float renderWidth;

    /**
     * 整体上部的padding
     */
    protected int chartPaddingTop;

    /**
     * 最视图和子试图上方padding
     */
    protected float childViewPaddingTop;
    /**
     * 整体底部padding
     */
    protected int chartPaddingBottom;
    /**
     * y轴的缩放比例
     */
    protected double mainScaleY = 1;

    /**
     * 成交量y轴缩放比例
     */
    protected double volScaleY = 1;


    protected double indexMacdScaleY, indexKdjScaleY, indexRsiScaleY, indexWrScaleY = 1;

    /**
     * 主视图的最大值
     */
    protected double mainMaxValue = Float.MAX_VALUE;

    /**
     * 主视图的最小值
     */
    protected double mainMinValue = Float.MIN_VALUE;

    /**
     * 主视图K线的的最大值
     */
    protected float mainHighMaxValue;

    /**
     * 主视图的K线的最小值
     */
    protected float mainLowMinValue;

    /**
     * X轴最大值坐标索引
     */
    protected int mainMaxIndex;

    /**
     * X轴最小值坐标索引
     */
    protected int mainMinIndex;

    /**
     * 成交量最大值
     */
    protected double volMaxValue = Float.MAX_VALUE;


    /**
     * 不同附图指标的成交量最大值
     */
    protected double indexMACDMaxValue = Float.MAX_VALUE;
    protected double indexKDJMaxValue = Float.MAX_VALUE;
    protected double indexRSIMaxValue = Float.MAX_VALUE;
    protected double indexWRMaxValue = Float.MAX_VALUE;

    /**
     * 不同附图指标的成交量最小值
     */
    protected float indexMACDMinValue = Float.MAX_VALUE;
    protected float indexKDJMinValue = Float.MAX_VALUE;
    protected float indexRSIMinValue = Float.MAX_VALUE;
    protected float indexWRMinValue = Float.MAX_VALUE;


    /**
     * 当前显示K线最左侧的索引
     */
    protected int screenLeftIndex = 0;

    /**
     * 当前显示K线最右侧的索引
     */
    protected int screenRightIndex = 0;

    /**
     * K线宽度
     */
    protected float chartItemWidth;

    /**
     * K线网格行数
     */
    protected int gridRows;

    /**
     * K线网格列数
     */
    protected int gridColumns;

    /**
     * 尾线画笔
     */
    protected Paint lineEndPointPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 尾线下填充画笔
     */
    protected Paint lineEndFillPointPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 视图背景画笔
     */
    protected Paint backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * logo paint
     */
    protected Paint logoPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * price line right box paint
     */
    protected Paint rightPriceBoxPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 网络画笔
     */
    protected Paint gridPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 十字线交点小点画笔
     */
    protected Paint selectedCrossPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 十字线交点大圆画笔
     */
    protected Paint selectedBigCrossPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 文字画笔
     */
    protected Paint commonTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 在价格线上的Label 画笔
     */
    protected Paint labelInPriceLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 选中时X坐标画笔
     */
    protected Paint selectedXLabelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);


    protected Paint selectedYLabelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * Label文字画笔
     */
    protected Paint yLabelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    protected Paint xLabelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);


    /**
     * 十字线横线画笔
     */
    protected Paint selectedXLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 十字线竖线画笔
     */
    protected Paint selectedYLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 十字线Y坐标显示背景画笔
     */
    protected Paint selectedPriceBoxBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 十字线Y坐标显示背景画笔
     */
    protected Paint selectorXBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 十字线坐标显示边框画笔
     */
    protected Paint selectorXFramePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 价格线画笔
     */
    protected Paint priceLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 当前价格边框画笔
     */
    protected Paint priceLineBoxPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 当前价格背景画笔
     */
    protected Paint priceLineBoxBgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 价格线右侧虚线画笔
     */
    protected Paint priceLineRightPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 价格线右侧虚线画笔
     */
    protected Paint priceLineRightTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    /**
     * 价格框高度
     */
    protected float priceLabelInLineBoxHeight = 40;

    /**
     * 价格框圆角半径
     */
    protected float priceLabelInLineBoxRadius = 20;

    /**
     * 价格框内边距
     */
    protected float priceLineBoxPadidng = 20;

    /**
     * 价格线图形的高
     */
    protected float priceShapeHeight = 20;

    /**
     * 价格线图形的宽
     */
    protected float priceShapeWidth = 10;

    /**
     * 价格线文字与图形的间隔
     */
    protected float priceBoxShapeTextMargin = 10;

    /**
     * 价格线的文字框距离屏幕右侧的边距
     */
    protected float priceLineBoxMarginRight = 120;

    /**
     * 主视视图
     */
    protected MainRender mainRender;

    /**
     * 交易量视图
     */
    protected VolumeRender volumeRender;

    protected KikiShowType showType = KikiShowType.NormalKline;


    /**
     * 子视图
     */
//    protected BaseRender indexRender;

    /**
     * 子视图的集合,为了做附图多选使用
     */
    protected List<BaseRender> selectedIndexRenderList = new ArrayList<>();


    /**
     * 当前K线的最新价
     */
    protected float lastPrice, lastVol;


    /**
     * K线数据适配器
     */
    protected BaseKLineChartAdapter<? extends KLineEntity> dataAdapter;

    /**
     * 最大最小值计算模式
     */
    protected @Status.MaxMinCalcModel
    int calcModel = Status.CALC_NORMAL_WITH_SHOW;

    /**
     * 量视图是否显示为线
     */
    protected @Status.KlineStatus
    int klineStatus;

    /**
     * 量视图是否显示为线
     */
    protected @Status.VolChartStatus
    int volChartStatus;

    /**
     * 数据
     */
    protected float[] points;


    /**
     * 数组间隔
     */
    private int indexInterval;

    /**
     * 统一文字高度
     */
    protected float textHeight;

    /**
     * 统一文字基础线
     */
    protected float baseLine;
    /**
     * 统一文字基础线
     */
    protected float legendMarginLeft;

    /**
     * 统一文字Decent
     */
    protected float textDecent;

    /**
     * y轴label向上偏移的距离
     */
    protected int mainYMoveUpInterval = 5;

    /**
     * Y轴label与右边缘距离
     */
    protected float yLabelMarginBorder;

    /**
     * 分时线阴影的半径
     */
    protected float endShadowLayerWidth;

    /**
     * 十字线相交点圆半径
     */
    protected float selectedPointRadius;

    /**
     * 滑动监听
     */
    protected SlidListener slidListener;

    /**
     * refresh time limit
     */
    protected long time;

    /**
     * 显示十字线的点
     */
    protected boolean showCrossPoint;

    /**
     * 强制隐藏信息框
     */
    protected boolean hideMarketInfo;

    /**
     * 最大最小值缩放系数
     */
    protected double maxMinCoefficient;


    /**
     *
     */
    protected @Status.KlineSupportStatus
    int klineSupportStatus;
    /**
     * 显示价格线的的价格Label
     */
    protected boolean showPriceLabelInLine;

    /**
     * 显示价格线
     */
    protected boolean showPriceLine;

    /**
     * x,y 轴 被选中 圆角
     */

    protected float selectedDateBoxRadius;


    /**
     * x,y 轴被选中，竖直方向间距
     */
    protected float selectedXYBackgroundHorizontalPadding;

    /**
     * 显示K线右侧价格线虚线
     */
    protected boolean showRightDotPriceLine;

    protected int priceLineMiddleColor;

    /**
     * 当前价格没有波动时的颜色
     */
    protected int priceLineRightColor;

    /**
     * X,y 轴 添加背景竖直方向padding
     */
    protected float selectedXYBackgroundVerticalPadding;

    /**
     * 当前数据个数
     */
    protected int itemsCount;


    /**
     * 指标视图
     */
    protected Map<Integer, BaseRender> indexRenders = new HashMap<>();


    /**
     * 日期格式化
     */
    protected IDateTimeFormatter dateTimeFormatter = new DateFormatter();

    /**
     * K线显示动画
     */
    protected ValueAnimator showAnim;

    /**
     * 空白区域
     */
    protected float overScrollRange = 0;

    /**
     * 选中变化监听
     */
    protected OnSelectedChangedListener selectedChangedListener = null;

    /**
     * 主视图
     */
    protected Rect mainRect;

    /**
     * 量视图
     */
    protected Rect volRect;

    /**
     * 子视图
     */
    protected Rect indexMACDRect, indexKDJRect, indexRSIRect, indexWRRect;

    protected List<KChartNewBean> list;


    /**
     * 分时线尾部点半径
     */
    protected float lineEndRadius;
    /**
     * 分时线尾部点半径
     */
    protected float lineEndMaxMultiply;

    /**
     * 分时线填充渐变的上部颜色
     */
    protected int timeLineFillTopColor;

    /**
     * 分时线填充渐变的下部颜色
     */
    protected int timeLineFillBottomColor;

    /**
     * 十字线Y轴的宽度
     */
    protected float selectedWidth;

    /**
     * 十字线Y轴的颜色
     */
    protected int selectedYColor = -1;

    /**
     * 背景色渐变上部颜色
     */
    protected int backGroundFillTopColor;

    /**
     * 背景色渐变下部颜色
     */
    protected int backGroundFillBottomColor;

    /**
     * 网格行间距
     */
    protected float rowSpace;

    /**
     * 网格列间距
     */
    protected float columnSpace;

    /**
     * 展示区域的高度
     */
    protected float displayHeight = 0;

    /**
     * 主图占比
     */
    protected float mainPercent = 80f;

    /**
     * 交易量图占比
     */
    protected float volPercent = 20f;

    /**
     * 开始绘制,设置false不进行绘制
     */
    protected boolean needRender = false;

    /**
     * 子图占比
     */
    protected float IndexPercent = 20f;

    private static final int CLICK_MARGIN = 80;

    /**
     * 是否适配X label
     */
    protected boolean betterX;
    protected boolean betterSelectedX;

    /**
     * 是否十字线跟随手指移动(Y轴)
     */
    protected @Status.TouchModel
    int crossTouchModel;


    /**
     * 当前主视图显示的指标
     */
    protected @Status.MainStatus
    int mainStatus = Status.MAIN_MA;
    /**
     * Y轴显示模式
     */
    protected @Status.YLabelShowModel
    int yLabelModel = Status.LABEL_NONE_GRID;

    /**
     * 子视图显示模式
     */
    protected @Status.ChildStatus
    int chartShowStatue = Status.MAIN_VOL;

    /**
     * 选中价格框横向padding
     */
    protected float selectedPriceBoxHorizontalPadding;
    /**
     * 选中日期框横向padding
     */
    protected float selectedDateBoxHorizontalPadding;

    /**
     * 选中日期框纵向padding
     */
    protected float dateBoxVerticalPadding;
    /**
     * 选中价格框纵向padding
     */
    protected float selectedPriceBoxVerticalPadding;

    protected float priceDotLineItemWidth;
    protected float priceDotLineItemSpace;

    protected float yLabelX;

    protected Paint yLabelBackgroundPaint;

    /**
     * 设置Y轴独立时是否与视图同高
     */
    protected boolean yLabelBackgroundHeightSameChart;

    /**
     * logo水印位置控制
     */
    protected float logoPaddingLeft, logoPaddingBottom, logoTop;

    /**
     * Y轴独立显示时的空间大小
     */
    protected float labelSpace = 0f;


    /**
     * 最新数据变化的执行动画
     */
    private ValueAnimator valueAnimator;

    /**
     * 设置 X 轴 左边距间距
     */
    protected float xLayoutMarginStart;

    protected float xLayoutMarginEnd;

    protected float yLayoutMarginEnd;

    protected int gridNums;

    protected boolean needUnitFormat = false;


    private float selectedY = 0;
    private float dataLength = 0;
    // for all place get format
    protected IValueFormatter baseValueFormatter = new ValueFormatter();
    protected Typeface typeface;

    public BaseKChartView(Context context) {
        super(context);
        init();
    }

    public BaseKChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseKChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * 初始化
     */
    protected void init() {
        setWillNotDraw(false);

        indexInterval = Constants.getCount();
        gestureDetector = new GestureDetectorCompat(getContext(), this);
        scaleDetector = new ScaleGestureDetector(getContext(), this);
        showAnim = ValueAnimator.ofFloat(0f, 1f);
        showAnim.setDuration(duration);
        showAnim.addUpdateListener(animation -> invalidate());
        selectorXFramePaint.setStyle(Paint.Style.STROKE);
        priceLinePaint.setAntiAlias(true);
        priceLineRightPaint.setStyle(Paint.Style.STROKE);
        rightPriceBoxPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        priceLineBoxPaint.setStyle(Paint.Style.STROKE);
        selectedXLinePaint.setStyle(Paint.Style.STROKE);
        xLabelPaint.setTextAlign(Paint.Align.CENTER);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        //如果没设置chartPaddingBottom
        if (0 == chartPaddingBottom) {
            chartPaddingBottom = (int) (dateBoxVerticalPadding * 2
                    + selectorXFramePaint.getStrokeWidth() * 2 + textHeight);
        }
        displayHeight = h - chartPaddingTop - (dateBoxVerticalPadding * 2
                + selectorXFramePaint.getStrokeWidth() * 2 + textHeight);
        float tempMainHeight = displayHeight * mainPercent / 100f;
        rowSpace = tempMainHeight / gridRows;
        this.viewWidth = w;
        this.viewHeight = h;

        initRect();
    }

    /**
     * 初始化视图区域
     * 主视图
     * 成交量视图
     * 子视图
     */
    protected void initRect() {
        switch (yLabelModel) {
            case Status.LABEL_NONE_GRID:
                this.renderWidth = viewWidth - labelSpace;
                columnSpace = renderWidth / gridColumns;
                break;
            case Status.LABEL_WITH_GRID:
                this.renderWidth = viewWidth;
                columnSpace = viewWidth / gridColumns;
                break;
        }
        int tempMainHeight, tempVolHeight, tempChildHeight;
        switch (chartShowStatue) {
            case Status.MAIN_VOL:
                mainPercent = 80f;
                volPercent = 20f;
                IndexPercent = 0f;
                tempMainHeight = (int) (displayHeight * mainPercent / 100f);
                tempVolHeight = (int) (displayHeight * volPercent / 100f);
                mainRect = new Rect(0, chartPaddingTop, (int) renderWidth, chartPaddingTop + tempMainHeight);
                volRect = new Rect(0, (int) (mainRect.bottom + childViewPaddingTop), (int) renderWidth, mainRect.bottom + tempVolHeight);
                break;
            case Status.MAIN_ONLY:
                mainRect = new Rect(0, chartPaddingTop, (int) renderWidth, (int) (chartPaddingTop + displayHeight));
                volRect = null;
                break;
            case Status.MAIN_INDEX:
                tempMainHeight = (int) (displayHeight * mainPercent / 100f);
                tempChildHeight = (int) (displayHeight * IndexPercent / 100f);
                mainRect = new Rect(0, chartPaddingTop, (int) renderWidth, chartPaddingTop + tempMainHeight);
//                indexRect = new Rect(0, (int) (mainRect.bottom + childViewPaddingTop), (int) renderWidth, mainRect.bottom + tempChildHeight);
                volRect = null;
                break;
            default:
            case Status.MAIN_VOL_INDEX:
                /*
                 *  附图指标 比例设置
                 *          主  vol 副
                 *  1；比例：65：20：15
                 *  2；比例：60：16：12：12
                 *  3；比例：55：12：11：11：11
                 *  4；比例：50：10：10：10：10：10
                 *
                 */
                if (selectedIndexRenderList != null && selectedIndexRenderList.size() > 0) {
                    int size = selectedIndexRenderList.size();
                    switch (size) {
                        case 1:
                            mainPercent = 65f;
                            volPercent = 20f;
                            IndexPercent = 15f;
                            break;
                        case 2:
                            mainPercent = 60f;
                            volPercent = 16f;
                            IndexPercent = 12f;
                            break;
                        case 3:
                            mainPercent = 55f;
                            volPercent = 12f;
                            IndexPercent = 11f;
                            break;
                        case 4:
                            mainPercent = 50f;
                            volPercent = 10f;
                            IndexPercent = 10f;
                            break;
                    }

                }
                // 这里我们固定每个附图指标的高度固定
                tempChildHeight = (int) (displayHeight * IndexPercent / 100f);
                tempVolHeight = (int) (displayHeight * volPercent / 100f);
                tempMainHeight = (int) (displayHeight * mainPercent / 100f);
                mainRect = new Rect(0, chartPaddingTop, (int) renderWidth, chartPaddingTop + tempMainHeight);
                volRect = new Rect(0, (int) (mainRect.bottom + childViewPaddingTop), (int) renderWidth, mainRect.bottom + tempVolHeight);
                // 这里为每一个附图指标的 Rect 初始化
                int top = 0;
                int bottom = 0;
                if (selectedIndexRenderList != null && selectedIndexRenderList.size() > 0) {

                    for (int i = 0; i < selectedIndexRenderList.size(); i++) {
                        BaseRender render = selectedIndexRenderList.get(i);
                        top = (int) (volRect.bottom + childViewPaddingTop * 2) + tempChildHeight * i;
                        bottom = volRect.bottom + tempChildHeight * (i + 1);
                        if (render instanceof MACDRender) {
                            indexMACDRect = new Rect(0, top, (int) renderWidth, bottom);
                        } else if (render instanceof KDJRender) {
                            indexKDJRect = new Rect(0, top, (int) renderWidth, bottom);
                        } else if (render instanceof RSIRender) {
                            indexRSIRect = new Rect(0, top, (int) renderWidth, bottom);
                        } else if (render instanceof WRRender) {
                            indexWRRect = new Rect(0, top, (int) renderWidth, bottom);
                        }
                    }
                }
                break;
        }
        if (null != logoBitmap) {
            logoTop = mainRect.bottom - logoBitmap.getHeight() - logoPaddingBottom;
        }
        invalidate();
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (needRender) {
            renderBackground(canvas);
            renderGrid(canvas);
            renderLogo(canvas);
            if (!isShowLoading &&
                    0 != viewWidth &&
                    0 != itemsCount &&
                    null != points &&
                    points.length != 0) {
                try {
                    calcValues();
                    renderXLabels(canvas);
                    renderK(canvas);
                    renderValue(canvas, getShowSelected() ? getSelectedIndex() : itemsCount - 1);
                } catch (Exception e) {
                    e.printStackTrace();
                    onReceiveError();
                }
            }
        }
    }

    protected abstract void onReceiveError();

    protected void lastChange() {
        int tempIndex = (itemsCount - 1) * indexInterval;
        if (isAnimationLast) {
            generaterAnimator(lastVol, points[tempIndex + Constants.INDEX_VOL],
                    animation -> lastVol = (Float) animation.getAnimatedValue());
            generaterAnimator(lastPrice, points[tempIndex + Constants.INDEX_CLOSE], animation -> {
                lastPrice = (Float) animation.getAnimatedValue();
                if (klineStatus != Status.KLINE_SHOW_TIME_LINE) {
                    animInvalidate();
                }
            });
            float[] tempData = Arrays.copyOfRange(points, tempIndex, points.length);
            mainRender.startAnim(BaseKChartView.this, tempData);
            volumeRender.startAnim(BaseKChartView.this, tempData);
        } else {
            mainRender.resetValues();
            volumeRender.resetValues();
            if (points.length >= tempIndex + Constants.INDEX_VOL)
                lastVol = points[tempIndex + Constants.INDEX_VOL];
            if (points.length >= tempIndex + Constants.INDEX_CLOSE)
                lastPrice = points[tempIndex + Constants.INDEX_CLOSE];
        }
    }

    protected void renderLogo(Canvas canvas) {
        if (null != logoBitmap) {
            canvas.drawBitmap(logoBitmap, logoPaddingLeft, logoTop, logoPaint);
        }
    }

    /**
     * Y轴上值的格式化,默认使用主视图formartter
     */
    protected IYValueFormatter valueFormatter = new IYValueFormatter() {
        @Override
        public String format(double max, double min, double value) {
            return mainRender.getValueFormatter().format(value);
        }
    };

    /**
     * 绘制整体背景
     *
     * @param canvas canvas
     */
    protected void renderBackground(Canvas canvas) {
        float mid = viewWidth / 2, bottom;
        int mainBottom = mainRect.bottom;
        backgroundPaint.setShader(new LinearGradient(mid, 0, mid, mainBottom, backGroundFillTopColor, backGroundFillBottomColor, Shader.TileMode.CLAMP));
        canvas.drawRect(0, 0, viewWidth + labelSpace, mainBottom, backgroundPaint);
        switch (chartShowStatue) {
            case Status.MAIN_VOL:
                bottom = volRect.bottom + chartPaddingBottom;
                backgroundPaint.setShader(new LinearGradient(mid, mainBottom, mid, bottom, backGroundFillTopColor, backGroundFillBottomColor, Shader.TileMode.CLAMP));
                canvas.drawRect(0, mainBottom, viewWidth, bottom, backgroundPaint);
                break;
            case Status.MAIN_INDEX:
//                bottom = indexRect.bottom + chartPaddingBottom;
//                backgroundPaint.setShader(new LinearGradient(mid, mainBottom, mid, bottom, backGroundFillTopColor, backGroundFillBottomColor, Shader.TileMode.CLAMP));
//                canvas.drawRect(0, mainBottom, viewWidth, indexRect.bottom, backgroundPaint);
                break;
            case Status.MAIN_VOL_INDEX:
                bottom = volRect.bottom;
                backgroundPaint.setShader(new LinearGradient(mid, mainBottom, mid, bottom, backGroundFillTopColor, backGroundFillBottomColor, Shader.TileMode.CLAMP));
                canvas.drawRect(0, mainBottom, viewWidth, bottom, backgroundPaint);
//                float indexBottom = indexRect.bottom;
//                backgroundPaint.setShader(new LinearGradient(mid, bottom, mid, indexBottom, backGroundFillTopColor, backGroundFillBottomColor, Shader.TileMode.CLAMP));
//                canvas.drawRect(0, bottom, viewWidth, indexBottom, backgroundPaint);
                break;
            case Status.MAIN_ONLY:
                backgroundPaint.setShader(new LinearGradient(mid, 0, mid, mainBottom, backGroundFillTopColor, backGroundFillBottomColor, Shader.TileMode.CLAMP));
                canvas.drawRect(0, 0, viewWidth, mainBottom, backgroundPaint);
                break;
        }
        if (yLabelModel == Status.LABEL_NONE_GRID && null != yLabelBackgroundPaint && labelSpace > 0) {
            canvas.drawRect(viewWidth - labelSpace,
                    0,
                    viewWidth,
                    yLabelBackgroundHeightSameChart ? viewHeight - chartPaddingBottom : viewHeight,
                    yLabelBackgroundPaint);
        }

    }

    /**
     * 设置当前K线总数据个数
     *
     * @param itemCount items count
     */
    protected void setItemsCount(int itemCount) {
        this.itemsCount = itemCount;
        dataLength = 0;
        mainRender.setItemCount(itemsCount);
        mainRender.resetValues();
        volumeRender.setItemCount(itemsCount);
        volumeRender.resetValues();
        fixScrollEnable(getDataLength());
    }

    /**
     * 计算主View的value对应的Y值
     *
     * @param value value
     * @return location Y
     */
    public float getMainY(float value) {
        double v = mainRect.top + ((mainMaxValue - value) * mainScaleY);
        if (v > mainRect.bottom) {
            v = mainRect.bottom;
        } else if (v < mainRect.top) {
            v = mainRect.top;
        }
        return (float) v;
    }

    /**
     * 计算成交量View的Y值
     *
     * @param value value
     * @return location y
     */
    public float getVolY(float value) {
        return (float) (volRect.top + baseLine - childViewPaddingTop + ((volMaxValue - value) * volScaleY));
    }

    /**
     * 计算子View的Y值
     *
     * @param value value
     * @return location y
     */
    public float getChildY(BaseRender render, float value) {

        if (render instanceof MACDRender) {
            return (float) (indexMACDRect.top + ((indexMACDMaxValue - value) * indexMacdScaleY));
        } else if (render instanceof KDJRender) {
            return (float) (indexKDJRect.top + ((indexKDJMaxValue - value) * indexKdjScaleY));
        } else if (render instanceof RSIRender) {
            return (float) (indexRSIRect.top + ((indexRSIMaxValue - value) * indexRsiScaleY));
        } else if (render instanceof WRRender) {
            return (float) (indexWRRect.top + ((indexWRMaxValue - value) * indexWrScaleY));
        }
        return 0f;
    }

    /**
     * 画网格
     *
     * @param canvas canvas
     */
    protected void renderGrid(Canvas canvas) {
        float right = renderWidth;
        float tempMainHeight = displayHeight * mainPercent / 100f;
        rowSpace = tempMainHeight / gridRows;
        //主图 画横线
        for (int i = 0; i <= gridRows; i++) {
            float y = rowSpace * i + chartPaddingTop;
            canvas.drawLine(0, y, right, y, gridPaint);
        }
        //主图 画竖线
        renderColumnLine(canvas, chartPaddingTop, mainRect.bottom);
        if (chartShowStatue == Status.MAIN_ONLY) {
            return;
        }
        // 画vol 区域网格
        canvas.drawLine(0, volRect.bottom, renderWidth, volRect.bottom, gridPaint);
        renderColumnLine(canvas, mainRect.bottom, volRect.bottom);

        // 画子视图的背景网格
        if (selectedIndexRenderList != null && selectedIndexRenderList.size() > 0) {
            for (BaseRender render : selectedIndexRenderList) {
                if (render instanceof MACDRender) {
                    renderColumnLine(canvas, indexMACDRect.top - childViewPaddingTop, indexMACDRect.bottom);
                    canvas.drawLine(0, indexMACDRect.bottom, renderWidth, indexMACDRect.bottom, gridPaint);
                } else if (render instanceof KDJRender) {
                    renderColumnLine(canvas, indexKDJRect.top - childViewPaddingTop, indexKDJRect.bottom);
                    canvas.drawLine(0, indexKDJRect.bottom, renderWidth, indexKDJRect.bottom, gridPaint);
                } else if (render instanceof RSIRender) {
                    renderColumnLine(canvas, indexRSIRect.top - childViewPaddingTop, indexRSIRect.bottom);
                    canvas.drawLine(0, indexRSIRect.bottom, renderWidth, indexRSIRect.bottom, gridPaint);
                } else if (render instanceof WRRender) {
                    renderColumnLine(canvas, indexWRRect.top - childViewPaddingTop, indexWRRect.bottom);
                    canvas.drawLine(0, indexWRRect.bottom, renderWidth, indexWRRect.bottom, gridPaint);
                }
            }
        }

    }

    private void renderColumnLine(Canvas canvas, float startY, float stopY) {
        for (int i = 0; i <= gridColumns; i++) {
            float stopX = columnSpace * i;
            canvas.drawLine(stopX, startY, stopX, stopY, gridPaint);
        }
    }


    /**
     * 绘制k线图
     *
     * @param canvas canvas
     */
    protected void renderK(Canvas canvas) {
        canvas.save();
        canvas.translate(canvasTranslateX, 0);
        switch (yLabelModel) {
            case Status.LABEL_NONE_GRID:
                canvas.save();
                canvas.clipRect(-canvasTranslateX, chartPaddingTop - textHeight, -canvasTranslateX + renderWidth, chartPaddingTop + displayHeight + textHeight);
                renderKCandle(canvas);
                mainRender.renderMaxMinValue(canvas, this, getX(mainMaxIndex), mainHighMaxValue, getX(mainMinIndex), mainLowMinValue);
                canvas.restore();
                break;
            case Status.LABEL_WITH_GRID:
                renderKCandle(canvas);
                mainRender.renderMaxMinValue(canvas, this, getX(mainMaxIndex), mainHighMaxValue, getX(mainMinIndex), mainLowMinValue);
                break;
        }

        renderYLabels(canvas);
        if (getShowSelected()) {
            renderSelected(canvas, getX(getSelectedIndex()));
        } else if (drawShapeEnable && null != drawShape) {
            drawShape.render(canvas,
                    xToTranslateX(0),
                    xToTranslateX(getChartWidth()),
                    0, (int) viewHeight);
        }
        canvas.restore();
    }

    protected void renderKCandle(Canvas canvas) {
        for (int i = screenLeftIndex; i <= screenRightIndex && i >= 0; i++) {
            float curX = getX(i), lastX;
            int lastTemp = 0;
            int nextTemp = indexInterval * i + indexInterval;
            float[] values;
            if (i == 0) {
                lastX = curX;
                values = Arrays.copyOfRange(points, lastTemp, indexInterval);
            } else {
                lastX = getX(i - 1);
                lastTemp = indexInterval * i - indexInterval;
                values = Arrays.copyOfRange(points, lastTemp, nextTemp);
            }
            switch (chartShowStatue) {
                case Status.MAIN_VOL_INDEX:
                    iteratorSelectedIndexRendersRender(canvas, lastX, curX, this, i, values);
                case Status.MAIN_VOL:
                    volumeRender.render(canvas, lastX, curX, this, i, values);
                case Status.MAIN_ONLY:
                    mainRender.render(canvas, lastX, curX, this, i, values);
                    break;
                case Status.MAIN_INDEX:
                    iteratorSelectedIndexRendersRender(canvas, lastX, curX, this, i, values);
                    mainRender.render(canvas, lastX, curX, this, i, values);
                    break;
            }
        }
    }

    //  遍历所选指标集合,render
    private void iteratorSelectedIndexRendersRender(Canvas canvas, float lastX, float curX, BaseKChartView baseKChartView, int i, float[] values) {
        if (selectedIndexRenderList == null || selectedIndexRenderList.size() == 0) {
            return;
        }
        for (BaseRender next : selectedIndexRenderList) {
            next.render(canvas, lastX, curX, baseKChartView, i, values);
        }
    }

    //  遍历所选指标集合,renderText
    private void iteratorSelectedIndexRendersRenderText(Canvas canvas, float x, float v, int position, float[] tempValues) {
        if (selectedIndexRenderList == null || selectedIndexRenderList.size() == 0) {
            return;
        }
        for (int i = 0; i < selectedIndexRenderList.size(); i++) {
            BaseRender render = selectedIndexRenderList.get(i);
            if (render instanceof MACDRender) {
                render.renderText(canvas, this, x,
                        indexMACDRect.top + baseLine - childViewPaddingTop, position, tempValues);
            } else if (render instanceof KDJRender) {
                render.renderText(canvas, this, x,
                        indexKDJRect.top + baseLine - childViewPaddingTop, position, tempValues);
            } else if (render instanceof RSIRender) {
                render.renderText(canvas, this, x,
                        indexRSIRect.top + baseLine - childViewPaddingTop, position, tempValues);
            } else {
                render.renderText(canvas, this, x,
                        indexWRRect.top + baseLine - childViewPaddingTop, position, tempValues);
            }
        }
    }


    public void renderSelected(Canvas canvas, float x) {
        float y, textWidth;
        String text;
        //十字线竖线
        float halfWidth = selectedWidth / 2 * scaleX;
        float left = x - halfWidth;
        float right = x + halfWidth;
        float bottom = displayHeight + chartPaddingTop;
        Path path = new Path();
        path.moveTo(left, chartPaddingTop);
        path.lineTo(right, chartPaddingTop);
        path.lineTo(right, bottom);
        path.lineTo(left, bottom);
        path.close();
        if (-1 != selectedYColor) {
            LinearGradient linearGradient = new LinearGradient(x, chartPaddingTop, x, bottom,
                    new int[]{Color.TRANSPARENT, selectedYColor, selectedYColor, Color.TRANSPARENT},
                    new float[]{0f, 0.2f, 0.8f, 1f}, Shader.TileMode.CLAMP);
            selectedYLinePaint.setShader(linearGradient);
        }
        canvas.drawPath(path, selectedYLinePaint);
        //画X值
        String date = formatDateTime(dataAdapter.getDate(getSelectedIndex()));
        textWidth = commonTextPaint.measureText(date);
        //向下多移动出一个像素(如果有必要可以设置多移动网络线宽度)
        y = chartPaddingTop + displayHeight + 1 + selectedXYBackgroundVerticalPadding;
        float temp = textWidth / 2;
        left = x - temp - selectedXYBackgroundHorizontalPadding;

        right = (float) (x + temp + selectedXYBackgroundHorizontalPadding * 1.5);

        bottom = (float) (y + textHeight + selectedXYBackgroundVerticalPadding * 2);

        if (currentX <= CLICK_MARGIN) {
            left += textWidth / 2 + 10;
            right += textWidth / 2 + 10;
        }
        if (getViewWidth() - currentX <= CLICK_MARGIN) {
            right -= textWidth / 2;
            left -= textWidth / 2;
        }

        if (selectedDateBoxRadius > 0) {
            RectF reactF = new RectF(left, y, right, bottom);
            canvas.drawRoundRect(reactF, selectedDateBoxRadius, selectedDateBoxRadius, selectorXBackgroundPaint);
        } else {
            // 默认的背景色
            canvas.drawRect(left, y, right, bottom, selectorXBackgroundPaint);
            canvas.drawRect(left, y, right, bottom, selectorXFramePaint);
        }
        // x 轴写字
        canvas.drawText(date, left + selectedXYBackgroundHorizontalPadding, y + baseLine + selectedXYBackgroundVerticalPadding, selectedXLabelPaint);

        //十字线Y值判断
        //十字线横线
        //  没用
        if (crossTouchModel == Status.TOUCH_FOLLOW_FINGERS) {
            y = selectedY;
            if (selectedY < mainRect.top + chartPaddingTop) {
                return;
            } else if (selectedY < mainRect.bottom) {
                text = mainRender.getValueFormatter().format((float) (mainMinValue + (mainMaxValue - mainMinValue) / (mainRect.bottom - chartPaddingTop) * (mainRect.bottom - selectedY)));
            } else if (null != volRect && selectedY < volRect.top) {
                return;
            } else if (null != volRect && selectedY < volRect.bottom) {
                text = volumeRender.getValueFormatter().format((volMaxValue / volRect.height() * (volRect.bottom - selectedY)));
            } else {
                return;
            }
        } else {
            if (getSelectedIndex() * indexInterval + Constants.INDEX_CLOSE < 0 || getSelectedIndex() * indexInterval + Constants.INDEX_CLOSE > points.length - 1) {
                return;
            }
            text = mainRender.getValueFormatter().format(points[getSelectedIndex() * indexInterval + Constants.INDEX_CLOSE]);
            y = getMainY(points[getSelectedIndex() * indexInterval + Constants.INDEX_CLOSE]);
        }
        // 需要带单位的格式化 显示方式
//        if (needUnitFormat) {
//            text = Utils.formatNum(new BigDecimal(text), 2);
//        }

        //十字线交点
        if (showCrossPoint) {
            canvas.drawCircle(x, y, chartItemWidth / 3 * 2, selectedBigCrossPaint);
            canvas.drawCircle(x, y, selectedPointRadius, selectedCrossPaint);
        }
        switch (yLabelModel) {
            case Status.LABEL_NONE_GRID:
                textWidth = labelSpace;
                x = -canvasTranslateX + this.viewWidth - textWidth;
                break;
            case Status.LABEL_WITH_GRID:
                textWidth = commonTextPaint.measureText(text);
                x = -canvasTranslateX + this.viewWidth - textWidth - 1 - 2 * selectedPriceBoxHorizontalPadding - selectedPriceBoxVerticalPadding;
                break;
        }
        temp = textHeight / 2 + selectedXYBackgroundVerticalPadding;

        float xBottom = y - temp;
        float xTop = y + temp;

        float newx = x - yLayoutMarginEnd;


        textWidth = selectedYLabelPaint.measureText(text);

        float xRight = newx + textWidth + selectedXYBackgroundHorizontalPadding * 2;

        // 选中的那条线
        canvas.drawLine(-canvasTranslateX, y, -canvasTranslateX + renderWidth - textWidth, y, selectedXLinePaint);

        RectF reactF = new RectF(newx, xTop, xRight, xBottom);
        //  画一个圆角矩形
        canvas.drawRoundRect(reactF, selectedDateBoxRadius, selectedDateBoxRadius, selectedPriceBoxBackgroundPaint);
        // y轴 写字
        canvas.drawText(text, newx + selectedXYBackgroundHorizontalPadding, xBottom + baseLine + selectedXYBackgroundVerticalPadding, selectedYLabelPaint);

    }

    /**
     * 绘制所有的Labels
     *
     * @param canvas canvas
     */
    protected void renderXLabels(Canvas canvas) {
        float y = chartPaddingTop + displayHeight + baseLine + dateBoxVerticalPadding;
        float halfWidth = chartItemWidth / 2 * getScaleX();
        float startX = getX(screenLeftIndex) - halfWidth;
        float stopX = getX(screenRightIndex) + halfWidth;
        int startLabelCount;
        int endLabelCount;
        if (betterX) {
            //X轴最左侧的值
            float translateX = xToTranslateX(0);
            if (translateX <= stopX) {
                String text = formatDateTime(dataAdapter.getDate(screenLeftIndex));
                canvas.drawText(text, commonTextPaint.measureText(text) / 2 + xLayoutMarginStart, y, xLabelPaint);
            }
            //X轴最右侧的值
            translateX = xToTranslateX(renderWidth);
            if (translateX >= startX && translateX <= stopX) {
                String text = formatDateTime(dataAdapter.getDate(screenRightIndex));
                canvas.drawText(text, renderWidth - commonTextPaint.measureText(text) / 2 - xLayoutMarginEnd, y, xLabelPaint);
            }
            startLabelCount = 1;
            endLabelCount = gridNums;
        } else {
            startLabelCount = 0;
            endLabelCount = gridNums + 1;
        }

        columnSpace = renderWidth / gridNums;
        for (; startLabelCount < endLabelCount; startLabelCount++) {
            float tempX = columnSpace * startLabelCount;
            float translateX = xToTranslateX(tempX);
            if (translateX >= startX && translateX <= stopX) {
                int index = indexOfTranslateX(translateX);
                String text = formatDateTime(dataAdapter.getDate(index));
                canvas.drawText(text, tempX, y, xLabelPaint);
            }
        }
    }

    /**
     * 绘制Y轴上的所有label
     *
     * @param canvas canvas
     */
    protected void renderYLabels(Canvas canvas) {
        double rowValue;
        int gridRowCount;
        String maxVol, childLabel;
        float tempLeft = -canvasTranslateX;
        switch (chartShowStatue) {
            case Status.MAIN_VOL_INDEX:
                gridRowCount = gridRows;
                rowValue = (mainMaxValue - mainMinValue) / gridRowCount;
                //Y轴上网络的值
                for (int i = 0; i <= gridRowCount; i++) {
                    String text = valueFormatter.format(mainMaxValue, mainMinValue, (float) (mainMaxValue - i * rowValue));
                    float v = rowSpace * i + chartPaddingTop;
                    canvas.drawText(text, tempLeft + yLabelX, v - mainYMoveUpInterval, yLabelPaint);
                }
                maxVol = volumeRender.getValueFormatter().format(volMaxValue);
                canvas.drawText(maxVol, tempLeft + yLabelX, volRect.top + childViewPaddingTop * 2, yLabelPaint);
                //子图Y轴label
                if (selectedIndexRenderList != null && selectedIndexRenderList.size() > 0) {

                    for (BaseRender render : selectedIndexRenderList) {
                        if (render instanceof MACDRender) {
                            childLabel = getBaseValueFormatter().format(indexMACDMaxValue);
                            canvas.drawText(childLabel, tempLeft + yLabelX, indexMACDRect.top + childViewPaddingTop * 2, yLabelPaint);
                            String macdMin = getBaseValueFormatter().format(indexMACDMinValue);
                            canvas.drawText(macdMin, tempLeft + yLabelX, indexMACDRect.bottom - childViewPaddingTop * 2, yLabelPaint);
                        } else if (render instanceof KDJRender) {
                            childLabel = render.getValueFormatter().format(indexKDJMaxValue);
                            canvas.drawText(childLabel, tempLeft + yLabelX, indexKDJRect.top + childViewPaddingTop * 2, yLabelPaint);
                            String kdjMin = render.getValueFormatter().format(indexKDJMinValue);
                            canvas.drawText(kdjMin, tempLeft + yLabelX, indexKDJRect.bottom - childViewPaddingTop * 2, yLabelPaint);
                        } else if (render instanceof RSIRender) {
                            childLabel = render.getValueFormatter().format(indexRSIMaxValue);
                            canvas.drawText(childLabel, tempLeft + yLabelX, indexRSIRect.top + childViewPaddingTop * 2, yLabelPaint);
                            String rsiMin = render.getValueFormatter().format(indexRSIMinValue);
                            canvas.drawText(rsiMin, tempLeft + yLabelX, indexRSIRect.bottom - childViewPaddingTop * 2, yLabelPaint);
                        } else if (render instanceof WRRender) {
                            childLabel = render.getValueFormatter().format(indexWRMaxValue);
                            canvas.drawText(childLabel, tempLeft + yLabelX, indexWRRect.top + childViewPaddingTop * 2, yLabelPaint);
                            String wrMin = render.getValueFormatter().format(indexWRMinValue);
                            canvas.drawText(wrMin, tempLeft + yLabelX, indexWRRect.bottom - childViewPaddingTop * 2, yLabelPaint);
                        }
                    }
                }

                break;
            case Status.MAIN_VOL:
                gridRowCount = gridRows;
                rowValue = (mainMaxValue - mainMinValue) / gridRowCount;
                //Y轴上网络的值
                for (int i = 0; i <= gridRowCount; i++) {
                    String text = valueFormatter.format(mainMaxValue, mainMinValue, (float) (mainMaxValue - i * rowValue));
                    float v = rowSpace * i + chartPaddingTop;
                    canvas.drawText(text, tempLeft + yLabelX, v - mainYMoveUpInterval, yLabelPaint);

                }
                maxVol = volumeRender.getValueFormatter().format(volMaxValue);
                canvas.drawText(maxVol, tempLeft + yLabelX, volRect.top + baseLine - childViewPaddingTop, yLabelPaint);
                break;
            case Status.MAIN_ONLY:
            case Status.MAIN_INDEX:
                gridRowCount = gridRows + 1;
                rowValue = (mainMaxValue - mainMinValue) / gridRowCount;
                //Y轴上网格的值
                for (int i = 0; i <= gridRowCount; i++) {
                    String text = valueFormatter.format(mainMaxValue, mainMinValue, (float) (mainMaxValue - i * rowValue));
//                    需要格式化 单位的修改，暂时注释掉
//                    if (needUnitFormat) {
//                        text = Utils.formatNum(new BigDecimal((mainMaxValue - i * rowValue)), 2);
//                    } else {
//                        text = valueFormatter.format(mainMaxValue, mainMinValue, (float) (mainMaxValue - i * rowValue));
//                    }
                    float v = 0;
                    if (i == gridRowCount) {
                        v = rowSpace * i;
                    } else {
                        v = rowSpace * i + chartPaddingTop;
                    }
                    canvas.drawText(text, tempLeft + yLabelX, v - mainYMoveUpInterval, yLabelPaint);
                }
                break;
        }
        renderPriceLine(canvas, tempLeft + viewWidth);
    }


    protected void renderPriceLine(Canvas canvas, float tempRight) {
        float y = getMainY(lastPrice);
        String priceText = valueFormatter.format(mainMaxValue, mainMinValue, lastPrice);
        float textWidth = commonTextPaint.measureText(priceText);
        float textLeft = tempRight - textWidth - yLabelMarginBorder;
        float klineRight = getX(screenRightIndex);
        if (screenRightIndex == itemsCount - 1 && klineRight < textLeft) {
            if (showType != KikiShowType.DataKline) {
                renderDotLine(canvas, klineRight, textLeft - priceLineMarginPriceLabel, y, priceLineRightPaint);
            }
            if (klineStatus == Status.KLINE_SHOW_TIME_LINE && klineRight < tempRight - labelSpace) {
                drawEndPoint(canvas, klineRight);
                canvas.drawCircle(klineRight, y, lineEndRadius, lineEndFillPointPaint);
            }
            renderRightPriceLabel(canvas, y, priceText, textWidth, textLeft);
        } else {
            if (showPriceLine) {
                if (yLabelModel == Status.LABEL_WITH_GRID) {
                    renderDotLine(canvas, 0, tempRight - priceLineMarginPriceLabel,
                            y, priceLinePaint);
                    if (showPriceLabelInLine) {
                        float halfPriceBoxHeight = priceLabelInLineBoxHeight / 2;
                        priceLabelInLineBoxRight = tempRight - priceLineBoxMarginRight;
                        priceText = mainRender.getValueFormatter().format(lastPrice);
                        textWidth = commonTextPaint.measureText(priceText);
                        priceLabelInLineBoxLeft = priceLabelInLineBoxRight - textWidth
                                - priceShapeWidth - priceLineBoxPadidng * 2
                                - priceBoxShapeTextMargin;
                        priceLabelInLineBoxTop = y - halfPriceBoxHeight;
                        priceLabelInLineBoxBottom = y + halfPriceBoxHeight;
                        renderPriceLabelInPriceLine(canvas,
                                priceLabelInLineBoxLeft,
                                priceLabelInLineBoxTop,
                                priceLabelInLineBoxRight,
                                priceLabelInLineBoxBottom,
                                priceLabelInLineBoxRadius,
                                y,
                                priceText);
                    }
                } else {
                    if (showRightDotPriceLine) {
                        textLeft = tempRight - textWidth - yLabelMarginBorder;
                        renderDotLine(canvas, 0, tempRight - labelSpace, y, priceLinePaint);
                        renderRightPriceLabel(canvas, y, priceText, textWidth, textLeft);
                    }
                }
            }
        }
    }

    protected PriceLabelInLineClickListener labelInLineClickListener = new PriceLabelInLineClickListener();
    protected boolean priceLabelInLineClickable;
    protected float priceLabelInLineBoxRight, priceLabelInLineBoxLeft,
            priceLabelInLineBoxTop, priceLabelInLineBoxBottom;

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        if (showPriceLine && priceLabelInLineClickable) {
            float x = xToTranslateX(e.getX());
            float y = e.getY();
            if (priceLabelInLineBoxTop < y && priceLabelInLineBoxBottom > y
                    && priceLabelInLineBoxLeft < x && priceLabelInLineBoxRight > x) {
                return labelInLineClickListener.onPriceLabelClick(this);
            }
        }
        return super.onSingleTapUp(e);

    }

    /**
     * 横屏价格线
     *
     * @param canvas canvas
     * @param y      y
     */

    protected void renderDotLine(Canvas canvas, float left, float right, float y, Paint paint) {
        float dotWidth = priceDotLineItemWidth + priceDotLineItemSpace;
        paint.setColor(priceLineMiddleColor);
        for (; left < right; left += dotWidth) {
            canvas.drawLine(left, y, left + priceDotLineItemWidth, y, paint);
        }
    }

    /**
     * 价格线上的价格label
     *
     * @param canvas    canvas
     * @param y         y
     * @param priceText priceText
     */
    protected void renderPriceLabelInPriceLine(Canvas canvas, float boxLeft, float boxTop,
                                               float boxRight, float boxBottom, float rectRadius,
                                               float y, String priceText) {
        labelInPriceLinePaint.setColor(Color.WHITE);
        canvas.drawRoundRect(new RectF(boxLeft, boxTop, boxRight, boxBottom), rectRadius,
                rectRadius, priceLineBoxBgPaint);
        float temp = priceShapeHeight / 2;
        float shapeLeft = boxRight - priceShapeWidth - priceLineBoxPadidng;
        //价格线三角形
        Path shape = new Path();
        shape.moveTo(shapeLeft, y - temp);
        shape.lineTo(shapeLeft, y + temp);
        shape.lineTo(shapeLeft + priceShapeWidth, y);
        shape.close();
        canvas.drawPath(shape, labelInPriceLinePaint);
        canvas.drawText(priceText, boxLeft + priceLineBoxPadidng,
                (y + (textHeight / 2 - textDecent)), labelInPriceLinePaint);
    }

    /**
     * 绘制最后一个呼吸灯效果
     *
     * @param canvas canvas
     * @param stopX  x
     */
    public void drawEndPoint(Canvas canvas, float stopX) {
        RadialGradient radialGradient = new RadialGradient(stopX, getMainY(lastPrice),
                endShadowLayerWidth, lineEndPointPaint.getColor(), Color.TRANSPARENT,
                Shader.TileMode.CLAMP);
        lineEndPointPaint.setShader(radialGradient);
        canvas.drawCircle(stopX, getMainY(lastPrice), lineEndRadius * lineEndMaxMultiply,
                lineEndPointPaint);
    }

    protected float priceLineMarginPriceLabel = 5;

    /**
     * 价格线label  右侧
     *
     * @param canvas      canvas
     * @param y           y
     * @param priceString priceString
     * @param textWidth   textWidth
     * @param textLeft    textLeft
     */
    protected void renderRightPriceLabel(Canvas canvas, float y, String priceString,
                                         float textWidth, float textLeft) {
        if (showType == KikiShowType.DataKline) {
            return;
        }
        float halfTextHeight = textHeight / 2;
        float top = y - halfTextHeight;
        canvas.drawRect(new Rect((int) textLeft, (int) top, (int) (textLeft + textWidth),
                (int) (y + halfTextHeight)), rightPriceBoxPaint);
        priceLineRightTextPaint.setColor(priceLineRightColor);
        canvas.drawText(priceString, textLeft, top + baseLine, priceLineRightTextPaint);
    }

    /**
     * 数据总长
     *
     * @return float
     */
    protected float getDataLength() {
        if (dataLength == 0) {
            return chartItemWidth * getScaleX() * (itemsCount - 1) + overScrollRange;
        } else {
            return dataLength;
        }
    }

    /**
     * 开启循环刷新绘制
     */
    public void startFreshPage() {
        if (null != valueAnimator && valueAnimator.isRunning()) {
            return;
        }
        valueAnimator = ValueAnimator.ofFloat(lineEndRadius, lineEndRadius * lineEndMaxMultiply);
        valueAnimator.setRepeatMode(ValueAnimator.REVERSE);
        valueAnimator.setDuration(duration);
        valueAnimator.setRepeatCount(10000);
        valueAnimator.addUpdateListener(animation -> {
            endShadowLayerWidth = (Float) animation.getAnimatedValue();
            animInvalidate();
        });
        valueAnimator.start();
    }

    /**
     * 关闭循环刷新
     */
    public void stopFreshPage() {
        if (null != valueAnimator && valueAnimator.isRunning()) {
            valueAnimator.cancel();
        }
        valueAnimator = null;
    }

    /**
     * 画值
     *
     * @param canvas   canvas
     * @param position 显示某个点的值
     */
    protected void renderValue(Canvas canvas, int position) {
        int temp = indexInterval * position;
        if (temp < points.length && position >= 0) {
            float[] tempValues = Arrays.copyOfRange(points, temp, temp + indexInterval);
            float x = legendMarginLeft;
            switch (chartShowStatue) {
                case Status.MAIN_INDEX:
                    mainRender.renderText(canvas, this, x,
                            mainRect.top + baseLine - textHeight / 2, position, tempValues);
                    iteratorSelectedIndexRendersRenderText(canvas, x,
                            mainRect.bottom + baseLine, position, tempValues);
                    break;
                case Status.MAIN_VOL_INDEX:
                    iteratorSelectedIndexRendersRenderText(canvas, x, volRect.bottom + baseLine, position, tempValues);
                case Status.MAIN_VOL:
                    volumeRender.renderText(canvas, this, x,
                            volRect.top + childViewPaddingTop * 2, position, tempValues);
                case Status.MAIN_ONLY:
                    mainRender.renderText(canvas, this, x,
                            mainRect.top + baseLine - textHeight / 2, position, tempValues);
                    break;
            }
        }
    }


    /**
     * 计算当前选中item的X的坐标
     *
     * @param x index of selected item
     * @return index
     */
    protected int calculateSelectedX(float x) {
        int tempIndex = indexOfTranslateX(xToTranslateX(x));
        if (tempIndex > screenRightIndex) {
            tempIndex = screenRightIndex;
        } else if (tempIndex < screenLeftIndex) {
            tempIndex = screenLeftIndex;
        }
        return tempIndex;

    }

    @Override
    public void onSelectedChange(MotionEvent e) {
        if (null != points && points.length > 0) {
            int index = calculateSelectedX(e.getX());
            setSelectedIndex(index);
            selectedY = e.getY();
            int temp = index * indexInterval;
            if (null != selectedChangedListener) {
                selectedChangedListener.onSelectedChanged(this, getSelectedIndex(), Arrays.copyOfRange(points, temp, temp + indexInterval));
            }
            invalidate();
        }
    }

    /**
     * 获取画板的最小位移
     *
     * @return translate value
     */
    public float getMinTranslate() {
        float dataLength = getDataLength();
        if (dataLength > renderWidth) {
            float minValue = -(dataLength - renderWidth);
            return (overScrollRange == 0) ? minValue - chartItemWidth * getScaleX() / 2 : minValue;
        } else {
            return chartItemWidth * scaleX / 2;
        }
    }

    /**
     * 获取平移的最大值
     *
     * @return 最大值
     */
    public float getMaxTranslate() {
        float dataLength = getDataLength();
        if (dataLength > renderWidth) {
            return klineStatus == Status.KLINE_SHOW_TIME_LINE ? 0 : chartItemWidth * getScaleX() / 2;
        }
        return renderWidth - dataLength + overScrollRange -
                (klineStatus == Status.KLINE_SHOW_TIME_LINE ? 0 : chartItemWidth * getScaleX() / 2);
    }

    // 滑动监听
    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        changeTranslated(canvasTranslateX + (l - oldl));
        if (klineStatus == Status.KLINE_SHOW_TIME_LINE
                && getX(screenRightIndex) + canvasTranslateX <= renderWidth) {
            startFreshPage();
        } else {
            stopFreshPage();
        }
        animInvalidate();
    }

    @Override
    protected void onScaleChanged(float scale, float oldScale) {
        //通过 放大和左右左右个数设置左移
        if (scale != oldScale) {
            dataLength = 0;
            float tempWidth = chartItemWidth * scale;
            float newCount = (renderWidth / tempWidth);
            float oldCount = (renderWidth / chartItemWidth / oldScale);
            float difCount = (newCount - oldCount) / 2;
            float dataLength = getDataLength();
            if (screenLeftIndex > 0) {
                changeTranslated(canvasTranslateX / oldScale * scale + difCount * tempWidth);
            } else if (dataLength < renderWidth) {
                changeTranslated(-(renderWidth - dataLength));
            } else {
                changeTranslated(getMaxTranslate());
            }
            fixScrollEnable(dataLength);
            fixRenderShape(scale, oldScale);
            animInvalidate();
        }
    }

    /**
     * 如果有画线,当缩放发生改变时需要对画线进行计算优化
     *
     * @param scale    新缩放
     * @param oldScale 老缩放
     */
    private void fixRenderShape(float scale, float oldScale) {
        if (null != drawShape) {
            drawShape.scaleChange(scale, oldScale);
        }
    }

    protected void fixScrollEnable(float dataLength) {
        if (drawShapeEnable) {
            setScrollEnable(false);
        } else if (autoFixScrollEnable) {
            if (dataLength <= renderWidth && isScrollEnable()) {
                setScrollEnable(false);
            } else if (!isScrollEnable() && dataLength > renderWidth) {
                setScrollEnable(true);
            }
        }
    }

    protected boolean autoFixScrollEnable = true;


    /**
     * 设置当前平移
     *
     * @param translateX canvasTranslateX
     */
    public void changeTranslated(float translateX) {
        if (translateX < getMinTranslate()) {
            translateX = getMinTranslate();
            if (!getForceStopSlid() && null != slidListener
                    && itemsCount > (renderWidth / chartItemWidth)) {
                setForceStopSlid(true);
                slidListener.onSlidRight();
            }
        } else if (translateX > getMaxTranslate()) {
            translateX = getMaxTranslate();
            if (!getForceStopSlid() && null != slidListener
                    && itemsCount > (renderWidth / chartItemWidth)) {
                setForceStopSlid(true);
                slidListener.onSlidLeft();
            }
        }
        this.canvasTranslateX = translateX;
    }

    private static final String TAG = "BaseKChartView";

    /**
     * 计算当前显示的数据以及显示的数据的最大最小值
     */
    protected void calcValues() {
        float scaleWidth = chartItemWidth * scaleX;
        if (canvasTranslateX <= scaleWidth / 2) {
            screenLeftIndex = (int) ((-canvasTranslateX) / scaleWidth);
            if (screenLeftIndex < 0) {
                screenLeftIndex = 0;
            }
            screenRightIndex = (int) (screenLeftIndex + (renderWidth / scaleWidth) + 0.5) + 1;
        } else {
            screenLeftIndex = 0;
            screenRightIndex = itemsCount - 1;
        }
        if (screenRightIndex > itemsCount - 1) {
            screenRightIndex = itemsCount - 1;
        }
        volMaxValue = Float.MIN_VALUE;
        double volMinValue = Float.MAX_VALUE;

        indexMACDMaxValue = Float.MIN_VALUE;
        indexKDJMaxValue = Float.MIN_VALUE;
        indexRSIMaxValue = Float.MIN_VALUE;
        indexWRMaxValue = Float.MIN_VALUE;

        indexMACDMinValue = Float.MAX_VALUE;
        indexKDJMinValue = Float.MAX_VALUE;
        indexRSIMinValue = Float.MAX_VALUE;
        indexWRMinValue = Float.MAX_VALUE;

        mainMaxIndex = screenLeftIndex;
        mainMinIndex = screenLeftIndex;
        switch (calcModel) {
            case Status.CALC_NORMAL_WITH_LAST:
            case Status.CALC_CLOSE_WITH_LAST:
                mainMaxValue = getLastPrice();
                mainMinValue = getLastPrice();
                mainHighMaxValue = Float.MIN_VALUE;
                mainLowMinValue = Float.MAX_VALUE;
                break;
            case Status.CALC_CLOSE_WITH_SHOW:
            case Status.CALC_NORMAL_WITH_SHOW:
                mainMaxValue = Float.MIN_VALUE;
                mainMinValue = Float.MAX_VALUE;
                mainHighMaxValue = Float.MIN_VALUE;
                mainLowMinValue = Float.MAX_VALUE;
                break;
        }
        int tempLeft = screenLeftIndex > 0 ? screenLeftIndex + 1 : 0;
        for (int i = tempLeft; i <= screenRightIndex; i++) {
            int tempIndex = indexInterval * i;
            switch (calcModel) {
                case Status.CALC_NORMAL_WITH_LAST:
                    if (i != itemsCount - 1) {
                        calcMainMaxValue(tempIndex);
                        calcMainMinValue(tempIndex);
                    }
                    if (mainHighMaxValue < points[tempIndex + Constants.INDEX_HIGH]) {
                        mainHighMaxValue = points[tempIndex + Constants.INDEX_HIGH];
                        mainMaxIndex = i;
                    }
                    if (mainLowMinValue >= points[tempIndex + Constants.INDEX_LOW]) {
                        mainLowMinValue = points[tempIndex + Constants.INDEX_LOW];
                        mainMinIndex = i;
                    }
                    break;
                case Status.CALC_NORMAL_WITH_SHOW:
                    calcMainMaxValue(tempIndex);
                    calcMainMinValue(tempIndex);
                    if (mainHighMaxValue < points[tempIndex + Constants.INDEX_HIGH]) {
                        mainHighMaxValue = points[tempIndex + Constants.INDEX_HIGH];
                        mainMaxIndex = i;
//                        Log.i("BaseKChartView", "mainMaxIndex-->" + mainMaxIndex);
                    }
                    if (mainLowMinValue >= points[tempIndex + Constants.INDEX_LOW]) {
                        mainLowMinValue = points[tempIndex + Constants.INDEX_LOW];
                        mainMinIndex = i;
//                        Log.i("BaseKChartView", "mainMinIndex-->" + mainMinIndex);
                    }
                    break;
                case Status.CALC_CLOSE_WITH_LAST:
                    float pointClose;
                    if (i != itemsCount - 1) {
                        pointClose = points[tempIndex + Constants.INDEX_CLOSE];
                        if (mainHighMaxValue < pointClose) {
                            mainHighMaxValue = pointClose;
                            mainMaxIndex = i;
                            mainMaxValue = pointClose;
                        }
                        if (mainLowMinValue >= pointClose) {
                            mainLowMinValue = pointClose;
                            mainMinIndex = i;
                            mainMinValue = pointClose;
                        }
                    }
                    break;
                case Status.CALC_CLOSE_WITH_SHOW:
                    pointClose = points[tempIndex + Constants.INDEX_CLOSE];
                    if (mainHighMaxValue < pointClose) {
                        mainHighMaxValue = pointClose;
                        mainMaxValue = pointClose;
                        mainMaxIndex = i;
                    }
                    if (mainLowMinValue >= pointClose) {
                        mainLowMinValue = pointClose;
                        mainMinValue = pointClose;
                        mainMinIndex = i;
                    }
                    break;
            }
            switch (chartShowStatue) {
                case Status.MAIN_VOL_INDEX:
                case Status.MAIN_VOL:
                    float volume = points[tempIndex + Constants.INDEX_VOL];
                    volMaxValue = volumeRender.getMaxValue(
                            (float) volMaxValue,
                            points[tempIndex + Constants.INDEX_VOL],
                            points[tempIndex + Constants.INDEX_VOL_MA_1],
                            points[tempIndex + Constants.INDEX_VOL_MA_2]);
                    if (screenRightIndex != itemsCount - 1) {
                        volMinValue = volumeRender.getMinValue(volMinValue,
                                points[tempIndex + Constants.INDEX_VOL],
                                points[tempIndex + Constants.INDEX_VOL_MA_1],
                                points[tempIndex + Constants.INDEX_VOL_MA_2]);
                    }
                    if (volume < volMinValue) {
                        volMinValue = volume;
                    }
                    if (chartShowStatue == Status.MAIN_VOL) {
                        break;
                    }
                case Status.MAIN_INDEX:
                    if (selectedIndexRenderList != null && selectedIndexRenderList.size() > 0) {
                        for (BaseRender render : selectedIndexRenderList) {
                            if (render instanceof MACDRender) {
                                indexMACDMaxValue = Math.max(indexMACDMaxValue, render.getMaxValue(Arrays.copyOfRange(points, tempIndex, tempIndex + indexInterval)));
                                indexMACDMinValue = Math.min(indexMACDMinValue, render.getMinValue(Arrays.copyOfRange(points, tempIndex, tempIndex + indexInterval)));
                            } else if (render instanceof KDJRender) {
                                indexKDJMaxValue = Math.max(indexKDJMaxValue, render.getMaxValue(Arrays.copyOfRange(points, tempIndex, tempIndex + indexInterval)));
                                indexKDJMinValue = Math.min(indexKDJMinValue, render.getMinValue(Arrays.copyOfRange(points, tempIndex, tempIndex + indexInterval)));
                            } else if (render instanceof RSIRender) {
                                indexRSIMaxValue = Math.max(indexRSIMaxValue, render.getMaxValue(Arrays.copyOfRange(points, tempIndex, tempIndex + indexInterval)));
                                indexRSIMinValue = Math.min(indexRSIMinValue, render.getMinValue(Arrays.copyOfRange(points, tempIndex, tempIndex + indexInterval)));
                            } else if (render instanceof WRRender) {
                                indexWRMaxValue = Math.max(indexWRMaxValue, render.getMaxValue(Arrays.copyOfRange(points, tempIndex, tempIndex + indexInterval)));
                                indexWRMinValue = Math.min(indexWRMinValue
                                        , render.getMinValue(Arrays.copyOfRange(points, tempIndex, tempIndex + indexInterval)));
                            }
                        }
                    }
                    break;
                case Status.MAIN_ONLY:
                    break;
            }
        }
        if (null == maxMinDeal) {
            if (mainMaxValue == mainMinValue) {
                //当最大值和最小值都相等的时候 分别增大最大值和 减小最小值
                mainMaxValue += Math.abs(mainMaxValue * maxMinCoefficient);
                mainMinValue -= Math.abs(mainMinValue * maxMinCoefficient);
            }
            double padding = (mainMaxValue - mainMinValue) * maxMinCoefficient;
            mainMaxValue += padding;
            mainMinValue = padding < mainMinValue ? mainMinValue -= padding : 0;

        } else {
            mainMaxValue = maxMinDeal.dealMainMax(mainMaxValue);
            mainMinValue = maxMinDeal.dealMainMin(mainMinValue);
        }
        switch (chartShowStatue) {
            case Status.MAIN_VOL_INDEX:
            case Status.MAIN_VOL:
                if (volMaxValue < 0.01f) {
                    volMaxValue = 0.01f;
                }
                if (null != maxMinDeal) {
                    volMaxValue = maxMinDeal.dealVolMax(volMaxValue);
                    volMinValue = maxMinDeal.dealVolMin(volMinValue);
                }
                volScaleY = volRect.height() * 1f / (volMaxValue - volMinValue);
                if (chartShowStatue == Status.MAIN_VOL) {
                    break;
                }
            case Status.MAIN_INDEX:
                if (selectedIndexRenderList != null && selectedIndexRenderList.size() > 0) {
                    for (BaseRender render : selectedIndexRenderList) {
                        if (render instanceof MACDRender) {
                            indexMACDMaxValue += Math.abs(indexMACDMaxValue * maxMinCoefficient);
                            indexMACDMinValue -= Math.abs(indexMACDMinValue * maxMinCoefficient);
                            if (indexMACDMaxValue == 0) {
                                indexMACDMaxValue = 1f;
                            }
                            indexMacdScaleY = indexMACDRect.height() * 1f / (indexMACDMaxValue - indexMACDMinValue);
                        } else if (render instanceof KDJRender) {
                            indexKDJMaxValue += Math.abs(indexKDJMaxValue * maxMinCoefficient);

                            indexKDJMinValue -= Math.abs(indexKDJMinValue * maxMinCoefficient);
                            if (indexKDJMaxValue == 0) {
                                indexKDJMaxValue = 1f;
                            }
                            indexKdjScaleY = indexKDJRect.height() * 1f / (indexKDJMaxValue - indexKDJMinValue);
                        } else if (render instanceof RSIRender) {
                            indexRSIMaxValue += Math.abs(indexRSIMaxValue * maxMinCoefficient);

                            indexRSIMinValue -= Math.abs(indexRSIMinValue * maxMinCoefficient);
                            if (indexRSIMaxValue == 0) {
                                indexRSIMaxValue = 1f;
                            }
                            indexRsiScaleY = indexRSIRect.height() * 1f / (indexRSIMaxValue - indexRSIMinValue);

                        } else if (render instanceof WRRender) {
                            indexWRMaxValue += Math.abs(indexWRMaxValue * maxMinCoefficient);

                            indexWRMinValue -= Math.abs(indexWRMinValue * maxMinCoefficient);
                            if (indexWRMaxValue == 0) {
                                indexWRMaxValue = 1f;
                            }
                            indexWrScaleY = indexWRRect.height() * 1f / (indexWRMaxValue - indexWRMinValue);
                        }
                    }
                }


                break;
            case Status.MAIN_ONLY:
                break;
        }
        mainScaleY = mainRect.height() * 1f / (mainMaxValue - mainMinValue);
        if (showAnim.isRunning()) {
            float value = (float) showAnim.getAnimatedValue();
            this.screenRightIndex = screenLeftIndex + Math.round(value * (this.screenRightIndex - screenLeftIndex));
        }
    }

    protected void calcMainMinValue(int tempIndex) {
        switch (mainStatus) {
            case Status.MAIN_MA:
                mainMinValue = mainRender.getMinValue((float) mainMinValue,
                        points[tempIndex + Constants.INDEX_LOW],
                        points[tempIndex + Constants.INDEX_MA_1],
                        points[tempIndex + Constants.INDEX_MA_2],
                        points[tempIndex + Constants.INDEX_MA_3]);
                break;
            case Status.MAIN_EMA:
                mainMinValue = mainRender.getMinValue((float) mainMinValue,
                        points[tempIndex + Constants.INDEX_LOW],
                        points[tempIndex + Constants.EMA_INDEX_1],
                        points[tempIndex + Constants.EMA_INDEX_2],
                        points[tempIndex + Constants.EMA_INDEX_3]);
                break;
            case Status.MAIN_BOLL:
                mainMinValue = mainRender.getMinValue((float) mainMinValue,
                        points[tempIndex + Constants.INDEX_LOW],
                        points[tempIndex + Constants.INDEX_BOLL_DN],
                        points[tempIndex + Constants.INDEX_BOLL_UP],
                        points[tempIndex + Constants.INDEX_BOLL_MB]);
                break;
            default:
                mainMinValue = mainRender.getMinValue((float) mainMinValue,
                        points[tempIndex + Constants.INDEX_LOW]);
                break;
        }
    }

    // 这里计算不同指标时的最大值
    protected void calcMainMaxValue(int tempIndex) {
        switch (mainStatus) {
            case Status.MAIN_MA:
                mainMaxValue = mainRender.getMaxValue((float) mainMaxValue,
                        points[tempIndex + Constants.INDEX_HIGH],
                        points[tempIndex + Constants.INDEX_MA_1],
                        points[tempIndex + Constants.INDEX_MA_2],
                        points[tempIndex + Constants.INDEX_MA_3]);
                break;
            case Status.MAIN_BOLL:
                mainMaxValue = mainRender.getMaxValue((float) mainMaxValue,
                        points[tempIndex + Constants.INDEX_HIGH],
                        points[tempIndex + Constants.INDEX_BOLL_DN],
                        points[tempIndex + Constants.INDEX_BOLL_UP],
                        points[tempIndex + Constants.INDEX_BOLL_MB]);
                break;
            case Status.MAIN_EMA:
                mainMaxValue = mainRender.getMaxValue((float) mainMaxValue,
                        points[tempIndex + Constants.INDEX_HIGH],
                        points[tempIndex + Constants.EMA_INDEX_1],
                        points[tempIndex + Constants.EMA_INDEX_2],
                        points[tempIndex + Constants.EMA_INDEX_3]);
                break;
            default:
                mainMaxValue = mainRender.getMaxValue((float) mainMaxValue,
                        points[tempIndex + Constants.INDEX_HIGH]);
                break;
        }
    }

    /**
     * 通过平移的位置获取X轴上的索引
     *
     * @param translateX mTranslateX
     * @return canvasTranslateX
     */
    public int indexOfTranslateX(float translateX) {
        float dataLength = getDataLength();
        if (dataLength < renderWidth) {
            return (int) ((translateX + canvasTranslateX) / chartItemWidth / scaleX + 0.5);
        } else {
            return (int) (translateX / chartItemWidth / getScaleX());
        }
    }


    /**
     * 在主区域画线
     *
     * @param startX    开始点的横坐标
     * @param stopX     开始点的值
     * @param stopValue 结束点的值
     */
    public void renderMainLine(Canvas canvas, Paint paint, float startX, float startValue, float stopX,
                               float stopValue) {
        canvas.drawLine(startX, getMainY(startValue), stopX, getMainY(stopValue), paint);

    }


    /**
     * 在主区域画分时线
     *
     * @param startX    开始点的横坐标
     * @param stopX     开始点的值
     * @param stopValue 结束点的值
     */
    public void renderLineFill(Canvas canvas, Paint paint, float startX, float startValue, float stopX,
                               float stopValue) {

        float y = displayHeight + chartPaddingTop + chartPaddingBottom;
        LinearGradient linearGradient = new LinearGradient(startX, chartPaddingTop,
                stopX, y, timeLineFillTopColor, timeLineFillBottomColor, Shader.TileMode.CLAMP);
        paint.setShader(linearGradient);
        float mainY = getMainY(stopValue);
        Path path = new Path();
        path.moveTo(startX, y);
        path.lineTo(startX, getMainY(startValue));
        path.lineTo(stopX, mainY);
        path.lineTo(stopX, y);
        path.close();
        canvas.drawPath(path, paint);
    }

    /**
     * 在子区域画线
     *
     * @param startX     开始点的横坐标
     * @param startValue 开始点的Y坐标
     * @param stopX      结束点的横坐标
     * @param stopValue  结束点的Y坐标
     */
    public void renderChildLine(BaseRender render, Canvas canvas, Paint paint, float startX, float startValue, float stopX,
                                float stopValue) {
        canvas.drawLine(startX, getChildY(render, startValue), stopX, getChildY(render, stopValue), paint);
    }

    /**
     * 在子区域画线
     *
     * @param startX     开始点的横坐标
     * @param startValue 开始点的值
     * @param stopX      结束点的横坐标
     * @param stopValue  结束点的值
     */
    public void renderVolLine(Canvas canvas, Paint paint, float startX, float startValue, float stopX,
                              float stopValue) {
        canvas.drawLine(startX, getVolY(startValue), stopX, getVolY(stopValue), paint);
    }


    /**
     * 画板上的坐标点
     *
     * @param position 索引值
     * @return x坐标
     */
    public float getX(int position) {
        float num = position * chartItemWidth * scaleX;
        return num;
    }


    /**
     * 格式化时间,
     *
     * @param date date
     */
    public String formatDateTime(Date date) {
        return dateTimeFormatter.format(date);
    }


    /**
     * 开始动画
     */
    public void startAnimation() {
        if (loadDataWithAnim && null != showAnim) {
            showAnim.start();
        }
    }

    /**
     * view中的x转化为TranslateX
     *
     * @param x x
     * @return translateX
     */
    protected float xToTranslateX(float x) {
        return x - canvasTranslateX;
    }

    /**
     * translateX
     *
     * @return x
     */
    public float getTranslateX() {
        return canvasTranslateX;
    }

    /**
     * 是否显示选中
     */
    public boolean getShowSelected() {
        return showSelected;
    }

    public KikiShowType getShowType() {
        return showType;
    }

    public int getMainMaxIndex() {
        return mainMaxIndex;
    }

    public int getMainMinIndex() {
        return mainMinIndex;
    }

    /**
     * 是否显示选中
     */
    public boolean hideMarketInfo() {
        return hideMarketInfo;
    }

    /**
     * 获取选择索引
     */
    public int getSelectedIndex() {
        return selectedIndex;
    }


    public int getVolRectBottom() {
        return volRect.bottom;
    }

    /**
     * 获取新的价格
     *
     * @return new price
     */
    public float getLastPrice() {
        return lastPrice;
    }

    /**
     * 获取最新的成交量
     *
     * @return new vol value
     */
    public float getLastVol() {
        return lastVol;
    }

    public float getTranslationScreenMid() {
        return getX(screenLeftIndex) + (renderWidth / 2);
    }

    /**
     * 执行一个动画变换
     *
     * @param start    start
     * @param end      end
     * @param listener listener
     * @return ValueAnimator
     */
    @SuppressWarnings("all")
    public void generaterAnimator(Float start, float end, ValueAnimator.AnimatorUpdateListener listener) {
        ValueAnimator animator = ValueAnimator.ofFloat(0 == start ? end - 0.001f : start, end);
        animator.setDuration(duration);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                if (isAnimationLast) {
                    listener.onAnimationUpdate(valueAnimator);
                }
            }
        });
        animator.start();
    }


    /**
     * 15毫秒内不重复刷新页面
     */
    public void animInvalidate() {
        if (System.currentTimeMillis() - time > 15) {
            time = System.currentTimeMillis();
            invalidate();
        }
    }

    /**
     * 设置是否以动画的方式变化最后一根线
     *
     * @return whether show last chart with anim
     */
    public boolean isAnimationLast() {
        return isAnimationLast;
    }

    /**
     * 获取文字画笔
     *
     * @return textPaint
     */
    public Paint getCommonTextPaint() {
        return commonTextPaint;
    }

    /**
     * 获取交易量Render
     *
     * @return {@link VolumeRender}
     */
    public VolumeRender getVolumeRender() {
        return volumeRender;
    }

    @Override
    protected void onDetachedFromWindow() {
        logoBitmap = null;
        super.onDetachedFromWindow();
    }


    /**
     * 获取当前K线显示状态
     *
     * @return {@link Status.KlineStatus}
     */
    public @Status.KlineStatus
    int getKlineStatus() {
        return klineStatus;
    }

    /**
     * 获取当前成交量显示状态
     *
     * @return {@link Status.KlineStatus}
     */
    public @Status.VolChartStatus
    int getVolChartStatus() {
        return volChartStatus;
    }


    /**
     * 获取当前主图显示状态
     *
     * @return {@link Status.MainStatus}
     */
    public @Status.MainStatus
    int getStatus() {
        return this.mainStatus;
    }

    /**
     * 获取某个点的time
     *
     * @param index index
     * @return time string
     */
    public String getTime(int index) {
        return dateTimeFormatter.format(dataAdapter.getDate(index));
    }

    /**
     * 获取上方padding
     */
    public float getChartPaddingTop() {
        return chartPaddingTop;
    }

    /**
     * 获取动画时长
     *
     * @return duration
     */
    public long getDuration() {
        return duration;
    }

    /**
     * 获取当前视图的宽
     *
     * @return float
     */
    public float getViewWidth() {
        return viewWidth;
    }

    /**
     * 获取当前K线显示范围的宽
     *
     * @return float
     */
    public float getChartWidth() {
        if (yLabelModel == Status.LABEL_NONE_GRID) {
            return renderWidth;
        } else {
            return viewWidth;
        }
    }

    @Override
    public boolean changeDrawShape(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if (null != drawShape && drawShapeEnable) {
            if (e2 == null) {
                drawShape.touchDown(xToTranslateX(e1.getX()), e1.getY());
            } else if (e1 == e2) { //点击事件
                drawShape.touchUp(xToTranslateX(e1.getX()), e1.getY());
            } else {
                drawShape.touchMove(xToTranslateX(e2.getX()), e2.getY(), distanceX, distanceY);
            }
            animInvalidate();
            return true;
        }
        return false;
    }

    /**
     * 数据观察者,当数据变化
     */
    protected DataSetObserver dataSetObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
//          1 判断数据个数化,更新最后一个数据还是添加数据
            int currentCount = BaseKChartView.this.itemsCount;
            points = dataAdapter.getPoints();
            //当前没数据默认加载数据
            int tempDataCount = dataAdapter.getCount();
            // 此处为修改的源码，解决设置空集合 crash
            if (points.length == 0) {
//                needRender = true;
                invalidate();
                return;
            }

            if (currentCount == 0) { //原没有数据
                BaseKChartView.this.points = points;
                setItemsCount(tempDataCount);
                int temp = (tempDataCount - 1) * indexInterval;
                lastPrice = points[temp + Constants.INDEX_CLOSE];
                lastVol = points[temp + Constants.INDEX_VOL];
                changeTranslated(getMinTranslate());
                startAnimation();
            } else if (currentCount == tempDataCount) { //更新数据
                lastChange();
                if (dataAdapter.getResetShowPosition()) {
                    changeTranslated(getMinTranslate());
                }
            } else {  //添加数据
                setItemsCount(tempDataCount);
                lastChange();
                if (dataAdapter.getResetShowPosition()) {
                    changeTranslated(getMinTranslate());
                } else if (getX(tempDataCount - 1) < xToTranslateX(renderWidth)) {
                    changeTranslated(getTranslateX() - (tempDataCount - currentCount) * chartItemWidth * getScaleX());
                }
            }
            needRender = true;
            invalidate();
        }

        @Override
        public void onInvalidated() {
            overScroller.forceFinished(true);
            needRender = false;
            //设置当前为0防止切换时出现莫名多一根柱子
            if (dataAdapter.getResetLastAnim()) {
                lastVol = 0;
                lastPrice = 0;
            }
        }
    };

    public float millionToX(long millions) {
        long start = dataAdapter.getDateMillion(0);
        long end = dataAdapter.getDateMillion(itemsCount - 1);
        float diffTime = millions - start;
        float v = getDataLength() * (end - start) / diffTime;

        LogUtil.e("xToMillion  : millions = " + millions + "      x = " + v);
        return v;
    }

    public float xToMillion(float x) {
        long start = dataAdapter.getDateMillion(0);
        long end = dataAdapter.getDateMillion(itemsCount - 1);
        long l = (long) (getDataLength() * (end - start) / x + start);
        LogUtil.e("xToMillion  : x = " + x + "      millions = " + l);
        return l;
    }

    public Bitmap getViewBitmap() {
        Bitmap bitmap = Bitmap.createBitmap((int) getViewWidth(), (int) viewHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.TRANSPARENT);
        canvas.setDrawFilter(new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG
                | Paint.FILTER_BITMAP_FLAG));
        draw(canvas);
        return bitmap;
    }

    public float getChildViewPaddingTop() {
        return childViewPaddingTop;
    }

    public IValueFormatter getBaseValueFormatter() {
        return baseValueFormatter;
    }

    public void setBaseValueFormatter(IValueFormatter baseValueFormatter) {
        this.baseValueFormatter = baseValueFormatter;
    }

    public void setUnitType(String unitType) {

    }
}
