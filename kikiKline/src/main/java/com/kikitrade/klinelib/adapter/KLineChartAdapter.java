package com.kikitrade.klinelib.adapter;

import static com.kikitrade.klinelib.utils.Constants.BOLL_N;
import static com.kikitrade.klinelib.utils.Constants.BOLL_P;

import android.util.Log;

import com.kikitrade.klinelib.model.KKTDModel;
import com.kikitrade.klinelib.model.KLineEntity;
import com.kikitrade.klinelib.utils.Constants;
import com.kikitrade.klinelib.utils.KLineDataTools;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*************************************************************************
 * Description   :
 *
 * @PackageName  : com.icechao.klinelib.adapter
 * @FileName     : KLineChartAdapter.java
 * @Author       : chao
 * @Date         : 2019/4/8
 * @Email        : icechliu@gmail.com
 * @version      : V1
 *************************************************************************/

public class KLineChartAdapter<T extends KLineEntity> extends BaseKLineChartAdapter<T> {

    private static final String TAG = "KLineChartAdapter";
    private int dataCount;

    private boolean resetShowPosition, resetLastAnim;

    public List<T> getDataSource() {
        return dataSource;
    }

    private final List<T> dataSource = new ArrayList<>();
    private float[] points;
    private KLineDataTools dataTools;

    public float[] getPoints() {
        return points;
    }

    public KLineChartAdapter() {
        dataTools= new KLineDataTools();
    }

    @Override
    public int getCount() {
        return dataSource.size();
    }

    @Override
    public Date getDate(int position) {
        if (position > dataSource.size() - 1 || position < 0) {
            return new Date();
        }
        return new Date(dataSource.get(position).getDate());
    }

    @Override
    public long getDateMillion(int position) {
        return dataSource.get(position).getDate();
    }

    /**
     * 重置K线数据
     *
     * @param data              K线数据
     * @param resetShowPosition 重置K线显示位置default true,如不需重置K线传入false
     */
    public void resetData(List<T> data, boolean resetShowPosition) {
        resetData(data, resetShowPosition, false);
    }

    /**
     * 重置K线数据
     *
     * @param data              K线数据
     * @param resetShowPosition 重置K线显示位置default true,如不需重置K线传入false
     * @param resetLastAnim     清楚最后一要柱子的动画,如果切换币需要使用方法传true
     */
    public void resetData(List<T> data, boolean resetShowPosition, boolean resetLastAnim) {
        notifyDataWillChanged();
        Log.i(TAG, "resetData--->"+data.size());
        dataSource.clear();
        dataSource.addAll(data);
        this.dataCount = dataSource.size();
        if (dataCount > 0) {
            points=dataTools.calculate(dataSource);
        } else {
            points = new float[]{};
        }
        setResetShowPosition(resetShowPosition);
        setResetLastAnim(resetLastAnim);
        notifyDataSetChanged();
    }

    /**
     * 重置K线数据
     *
     * @param data K线数据
     */
    public void resetData(List<T> data) {
        resetData(data, true);
    }

    /**
     * 通知K线显示位置发和变化,需要重置时需先设置resetShowPosition为true后调用此方法
     */
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    /**
     * 向尾部添加数据
     */
    public void addLast(T entity) {
        addLast(entity, false);
    }

    /**
     * 向尾部添加数据
     */
    public void addLast(T entity, boolean resetShowPosition) {
        if (null != entity) {
            dataSource.add(entity);
            this.dataCount++;
            points= dataTools.calculate(dataSource);
            setResetShowPosition(resetShowPosition);
            notifyDataSetChanged();
        }
    }

    /**
     * 改变某个点的值
     *
     * @param position 索引值
     */
    public void changeItem(int position, T data) {
        dataSource.set(position, data);
        points= dataTools.calculate(dataSource);
        notifyDataSetChanged();
    }

    public void setResetLastAnim(boolean resetLastAnim) {
        this.resetLastAnim = resetLastAnim;
    }

    public boolean getResetShowPosition() {
        return resetShowPosition;
    }

    public void setResetShowPosition(boolean resetShowPosition) {
        this.resetShowPosition = resetShowPosition;
    }

    @Override
    public boolean getResetLastAnim() {
        return resetLastAnim;
    }


    /**
     * 计算MA BOLL RSI KDJ MACD
     *
     * @param dataList
     */
    public void calculate(List<? extends KLineEntity> dataList) {
        calculate(dataList,
                Constants.getEma1(), Constants.getEma2(), Constants.getEma3());

        calcRsi(points, dataList, Constants.getRsi1(), 1);
        if (-1 != Constants.getRsi2()) {
            calcRsi(points, dataList, Constants.getRsi2(), 2);
        }
        if (-1 != Constants.getRsi3()) {
            calcRsi(points, dataList, Constants.getRsi3(), 3);
        }
    }

    /**
     * 计算
     *
     * @param dataList
     * @return
     */
    private void calculate(List<? extends KLineEntity> dataList,
                           int ema1, int ema2, int ema3) {
        double maSum1 = 0;
        double maSum2 = 0;
        double maSum3 = 0;

        int kdjDay = Constants.getKdjK();

        int wr1 = Constants.getWr1();
        int wr2 = Constants.getWr2();
        int wr3 = 0;


        double maOne = Constants.getVolMa1();
        double maTwo = Constants.getVolMa2();


        double volumeMaOne = 0;
        double volumeMaTwo = 0;


        float preEma12 = 0;
        float preEma26 = 0;

        float preDea = 0;

        int indexInterval = Constants.getCount();
        points = new float[dataList.size() * indexInterval];
        double priceMaOne = Constants.getMaNumber1();
        double priceMaTwo = Constants.getMaNumber2();
        double priceMaThree = Constants.getMaNumber3();

        int s = Constants.getMacdS();
        int l = Constants.getMacdL();
        int m = Constants.getMacdM();

        int size = dataList.size();

        List<KKTDModel> TDs = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            KLineEntity point = dataList.get(i);
            float closePrice = point.getClosePrice();


            points[indexInterval * i + Constants.INDEX_RSI_1] = Float.MIN_VALUE;
//            points[indexInterval * i + Constants.INDEX_DATE] = point.getHighPrice();
            points[indexInterval * i + Constants.INDEX_OPEN] = point.getOpenPrice();
            points[indexInterval * i + Constants.INDEX_CLOSE] = point.getClosePrice();
            points[indexInterval * i + Constants.INDEX_HIGH] = point.getHighPrice();
            points[indexInterval * i + Constants.INDEX_LOW] = point.getLowPrice();
            points[indexInterval * i + Constants.INDEX_VOL] = point.getVolume();


            //ma计算
            maSum1 += closePrice;
            maSum2 += closePrice;
            maSum3 += closePrice;


            if (i == priceMaOne - 1) {
                point.setMaOne((float) (maSum1 / priceMaOne));
            } else if (i >= priceMaOne) {
                maSum1 -= dataList.get((int) (i - priceMaOne)).getClosePrice();
                point.setMaOne((float) (maSum1 / priceMaOne));
            } else {
                point.setMaOne(Float.MIN_VALUE);
            }
            points[indexInterval * i + Constants.INDEX_MA_1] = point.getMaOne();

            if (i == priceMaTwo - 1) {
                point.setMaTwo((float) (maSum2 / priceMaTwo));
            } else if (i >= priceMaTwo) {
                maSum2 -= dataList.get((int) (i - priceMaTwo)).getClosePrice();
                point.setMaTwo((float) (maSum2 / priceMaTwo));
            } else {
                point.setMaTwo(Float.MIN_VALUE);
            }
            points[indexInterval * i + Constants.INDEX_MA_2] = point.getMaTwo();

            if (i == priceMaThree - 1) {
                point.setMaThree((float) (maSum3 / priceMaThree));
            } else if (i >= priceMaThree) {
                maSum3 -= dataList.get((int) (i - priceMaThree)).getClosePrice();
                point.setMaThree((float) (maSum3 / priceMaThree));
            } else {
                point.setMaThree(Float.MIN_VALUE);
            }
            points[indexInterval * i + Constants.INDEX_MA_3] = point.getMaThree();


            if (s > 0 && l > 0 && m > 0) {
                if (size >= m + l - 2) {
                    if (i < l - 1) {
                        point.setDif(0);
                    }

                    if (i >= s - 1) {
                        float ema12 = calculateEma(dataList, s, i, preEma12);
                        preEma12 = ema12;
                        if (i >= l - 1) {
                            float ema26 = calculateEma(dataList, l, i, preEma26);
                            preEma26 = ema26;
                            point.setDif(ema12 - ema26);
                        } else {
                            point.setDif(Float.MIN_VALUE);
                        }
                    } else {
                        point.setDif(Float.MIN_VALUE);
                    }

                    if (i >= m + l - 2) {
                        boolean isFirst = i == m + l - 2;
                        float dea = calculateDea(dataList, l, m, i, preDea, isFirst);
                        preDea = dea;
                        point.setDea(dea);
                    } else {
                        point.setDea(Float.MIN_VALUE);
                    }

                    if (i >= m + l - 2) {
                        point.setMacd(point.getDif() - point.getDea());
                    } else {
                        point.setMacd(0);
                    }

                } else {
                    point.setMacd(0);
                }
            }
            points[indexInterval * i + Constants.INDEX_MACD_DIF] = point.getDif();
            points[indexInterval * i + Constants.INDEX_MACD_MACD] = point.getMacd();
            points[indexInterval * i + Constants.INDEX_MACD_DEA] = point.getDea();

            // ema 计算
            if (ema1 > 0 && i >= ema1 - 1) {
                points[indexInterval * i + Constants.EMA_INDEX_1] = calculateEma(dataList, ema1, i, points[indexInterval * (i - 1) + Constants.EMA_INDEX_1]);
            } else {
                points[indexInterval * i + Constants.EMA_INDEX_1] = 0;
            }
            if (ema2 > 0 && i >= ema2 - 1) {
                points[indexInterval * i + Constants.EMA_INDEX_2] = calculateEma(dataList, ema2, i, points[indexInterval * (i - 1) + Constants.EMA_INDEX_2]);
            } else {
                points[indexInterval * i + Constants.EMA_INDEX_2] = 0;
            }
            if (ema3 > 0 && i >= ema3 - 1) {
                points[indexInterval * i + Constants.EMA_INDEX_3] = calculateEma(dataList, ema3, i, points[indexInterval * (i - 1) + Constants.EMA_INDEX_3]);
            } else {
                points[indexInterval * i + Constants.EMA_INDEX_3] = 0;
            }


            //boll计算
            if (i >= BOLL_N - 1) {
                float boll = calculateBoll(dataList, i, BOLL_N);
                float highBoll = boll + BOLL_P * STD(dataList, i, BOLL_N);
                float lowBoll = boll - BOLL_P * STD(dataList, i, BOLL_N);
                point.setUp(highBoll);
                point.setMb(boll);
                point.setDn(lowBoll);
            } else {
                point.setUp(Float.MIN_VALUE);
                point.setMb(Float.MIN_VALUE);
                point.setDn(Float.MIN_VALUE);
            }


            points[indexInterval * i + Constants.INDEX_BOLL_UP] = point.getUp();
            points[indexInterval * i + Constants.INDEX_BOLL_MB] = point.getMb();
            points[indexInterval * i + Constants.INDEX_BOLL_DN] = point.getDn();

            //vol ma计算
            volumeMaOne += point.getVolume();
            volumeMaTwo += point.getVolume();
            float ma;
            if (i == maOne - 1) {
                ma = (float) (volumeMaOne / maOne);
            } else if (i > maOne - 1) {
                volumeMaOne -= dataList.get((int) (i - maOne)).getVolume();
                ma = (float) (volumeMaOne / maOne);
            } else {
                ma = Float.MIN_VALUE;
            }
            point.setMA5Volume(ma);
            points[indexInterval * i + Constants.INDEX_VOL_MA_1] = point.getMA5Volume();

            if (i == maTwo - 1) {
                ma = (float) (volumeMaTwo / maTwo);
            } else if (i > maTwo - 1) {
                volumeMaTwo -= dataList.get((int) (i - maTwo)).getVolume();
                ma = (float) (volumeMaTwo / maTwo);
            } else {
                ma = Float.MIN_VALUE;
            }
            point.setMA10Volume(ma);
            points[indexInterval * i + Constants.INDEX_VOL_MA_2] = point.getMA10Volume();

            //kdj
            calcKdj(dataList, kdjDay, i, point, closePrice);
            points[indexInterval * i + Constants.INDEX_KDJ_K] = point.getK();
            points[indexInterval * i + Constants.INDEX_KDJ_D] = point.getD();
            points[indexInterval * i + Constants.INDEX_KDJ_J] = point.getJ();

            //计算3个 wr指标
            point.setWrOne(getWrValue(dataList, wr1, i));
            point.setWrTwo(getWrValue(dataList, wr2, i));
            point.setWrThree(getWrValue(dataList, wr3, i));
            points[indexInterval * i + Constants.INDEX_WR_1] = (float) point.getWrOne();
            points[indexInterval * i + Constants.INDEX_WR_2] = (float) point.getWrTwo();

            // 计算 TD 值
            calcTd(dataList, i, TDs, indexInterval, points);
        }
    }


    private void calcTd(List<? extends KLineEntity> models, int index, List<KKTDModel> TDs, int indexInterval, float[] points) {
        KKTDModel td = new KKTDModel();
        if (index >= 5) {
            td.setSellCountdownIndex(TDs.get(index - 1).getSellCountdownIndex());
            td.setBuyCountdownIndex(TDs.get(index - 1).getBuyCountdownIndex());
            td.setSellSetup(TDs.get(index - 1).isSellSetup());
            td.setBuySetup(TDs.get(index - 1).isBuySetup());
            td.setTDSTBuy(TDs.get(index - 1).getTDSTBuy());
            td.setTDSTSell(TDs.get(index - 1).getTDSTSell());
            td.setSellSetupPerfection(TDs.get(index - 1).isSellSetupPerfection());
            td.setBuySetupPerfection(TDs.get(index - 1).isBuySetupPerfection());

            boolean closeLessThanCloseOf4BarsEarlier = models.get(index).getClosePrice() < models.get(index - 4).getClosePrice();
            boolean closeGreaterThanCloseOf4BarsEarlier = models.get(index).getClosePrice() > models.get(index - 4).getClosePrice();
            td.setBearishFlip(models.get(index - 1).getClosePrice() > models.get(index - 5).getClosePrice() && closeLessThanCloseOf4BarsEarlier);
            td.setBullishFlip(models.get(index - 1).getClosePrice() < models.get(index - 5).getClosePrice() && closeGreaterThanCloseOf4BarsEarlier);
            // TD buySetup
            if (td.isBearishFlip() || (TDs.get(index - 1).getBuySetupIndex() > 0 && closeLessThanCloseOf4BarsEarlier)) {// 表示买,红色
                td.setBuySetupIndex((TDs.get(index - 1).getBuySetupIndex() + 1 - 1) % 13 + 1);
                points[indexInterval * index + Constants.TD_INDEX_BUY] = (TDs.get(index - 1).getBuySetupIndex() + 1 - 1) % 13 + 1;
                td.setTDSTBuy(Math.max(models.get(index).getHighPrice(), TDs.get(index - 1).getTDSTBuy()));
            } else if (td.isBullishFlip() || (TDs.get(index - 1).getSellSetupIndex() > 0 && closeGreaterThanCloseOf4BarsEarlier)) {// 表示卖，绿色
                td.setSellSetupIndex((TDs.get(index - 1).getSellSetupIndex() + 1 - 1) % 13 + 1);
                points[indexInterval * index + Constants.TD_INDEX_SELL] = (TDs.get(index - 1).getSellSetupIndex() + 1 - 1) % 13 + 1;
                td.setTDSTSell(Math.max(models.get(index).getLowPrice(), TDs.get(index - 1).getTDSTSell()));
            }

            // Did buy setup happen?
            if (td.getBuySetupIndex() == 13) {
                td.setBuySetup(true);
                td.setSellSetup(false);
                td.setSellSetupPerfection(false);
                td.setBuySetupPerfection((models.get(index - 1).getLowPrice() < models.get(index - 3).getLowPrice() && models.get(index - 1).getLowPrice() < models.get(index - 2).getLowPrice()) ||
                        // bar 9 low < 6 and 7
                        (models.get(index).getLowPrice() < models.get(index - 3).getLowPrice() && models.get(index).getLowPrice() < models.get(index - 2).getLowPrice()));
            }

            // Did sell setup happen?
            if (td.getSellSetupIndex() == 13) {
                td.setSellSetup(true);
                td.setBuySetup(false);
                td.setBuySetupPerfection(false);
                td.setSellSetupPerfection((models.get(index - 1).getHighPrice() > models.get(index - 3).getHighPrice() && models.get(index - 1).getHighPrice() > models.get(index - 2).getHighPrice()) ||
                        // bar 9 high > 6 and 7
                        (models.get(index).getHighPrice() > models.get(index - 3).getHighPrice() && models.get(index).getHighPrice() > models.get(index - 2).getHighPrice()));
            }
            calculateTDBuyCountdown(TDs, td, models, models.get(index), index, indexInterval, points);
            calculateTDSellCountdown(TDs, td, models, models.get(index), index, indexInterval, points);
        }
        TDs.add(td);
    }

    private void calculateTDSellCountdown(List<KKTDModel> result, KKTDModel resultObj, List<? extends KLineEntity> ohlc, KLineEntity item, int i, int interval, float[] points) {

        // TD Sell countdown
        boolean isTDExist = item.getTd() != null;
        if (
            // Sell setup appears
                (result.get(i - 1).isSellSetup() && resultObj.isBuySetup()) ||
                        // Close above TDSTBuy
                        (item.getClosePrice() < (isTDExist ? item.getTd().getTDSTSell() : 0))
        ) {
            resultObj.setSellCountdownIndex(0);
            resultObj.setCountdownResetForTDST(true);
        } else if (resultObj.isSellSetup()) {
            if (
                    item.getClosePrice() > ohlc.get(i - 2).getHighPrice()
                // && item.close > ohlc[i - 1].high
            ) {
                resultObj.setSellCountdownIndex((result.get(i - 1).getSellCountdownIndex() + 1 - 1) % 13 + 1);
                resultObj.setCountdownIndexIsEqualToPreviousElement(false);
            }
        }

        //If this item and the preivous one were both 13, we set it to zero
        if (resultObj.getSellCountdownIndex() == 13 && result.get(i - 1).getSellCountdownIndex() == 13) {
            resultObj.setSellCountdownIndex(0);
        }

        //A.S: If countdown  hit 13, and we were counting another  setup, we reset that  setup
        if (resultObj.getSellCountdownIndex() == 13 && resultObj.getSellSetupIndex() > 0) {
            resultObj.setSellSetupIndex(1);
            points[interval * i + Constants.TD_INDEX_SELL] = 1;
        }

        // If we just reset the countdown
        if (resultObj.getSellCountdownIndex() != 13 && result.get(i - 1).getSellCountdownIndex() == 13) {
            resultObj.setSellSetup(false);
            resultObj.setSellSetupPerfection(false);
            resultObj.setSellCountdownIndex(0);
        }

    }

    private void calculateTDBuyCountdown(List<KKTDModel> result, KKTDModel resultObj, List<? extends KLineEntity> ohlc, KLineEntity item, int i, int interval, float[] points) {

        //First we do cancellations:
        //If we were doing buy countdown, and now sell setup happens
        boolean isTDExist = item.getTd() != null;
        if (
            // Sell setup appears
                (result.get(i - 1).isBuySetup() && resultObj.isSellSetup()) ||
                        // Close above TDSTBuy
                        (item.getClosePrice() > (isTDExist ? item.getTd().getTDSTBuy() : 0))
        ) {
            resultObj.setBuyCountdownIndex(0);
            resultObj.setCountdownResetForTDST(true);
        } else if (resultObj.isBuySetup()) {
            if (
                    item.getClosePrice() < ohlc.get(i - 2).getLowPrice()
                // && item.close > ohlc[i - 1].low
            ) {
                resultObj.setBuyCountdownIndex((result.get(i - 1).getBuyCountdownIndex() + 1 - 1) % 13 + 1);
                resultObj.setCountdownIndexIsEqualToPreviousElement(false);
            }
        }

        //If this item and the previous one were both 13, we set it to zero
        if (resultObj.getBuyCountdownIndex() == 13 && result.get(i - 1).getBuyCountdownIndex() == 13) {
            resultObj.setBuyCountdownIndex(0);
        }

        //A.S: If countdown  hit 13, and we were counting another buy setup, we reset that buy setup
        if (resultObj.getBuyCountdownIndex() == 13 && resultObj.getBuySetupIndex() > 0) {
            resultObj.setBuySetupIndex(1);
            points[interval * i + Constants.TD_INDEX_BUY] = 1;
        }

        // If we just reset the countdown
        if (resultObj.getBuyCountdownIndex() != 13 && result.get(i - 1).getBuyCountdownIndex() == 13) {
            resultObj.setBuySetup(false);
            resultObj.setBuySetupPerfection(false);
            resultObj.setBuyCountdownIndex(0);
        }

    }


    public void calcRsi(float[] points, List<? extends KLineEntity> klineInfos,
                        int n, int index) {
        if (klineInfos.size() > n) {
            double firstValue;
            switch (index) {
                default:
                case 1:
                    firstValue = klineInfos.get(n - 1).getRsiOne();
                    break;
                case 2:
                    firstValue = klineInfos.get(n - 1).getRsiTwo();
                    break;
                case 3:
                    firstValue = klineInfos.get(n - 1).getRsiThree();
                    break;
            }
            if (firstValue != 0 && firstValue != Float.MIN_VALUE) {
                calcRsiChange(points, klineInfos, n, findStart(klineInfos, index),
                        klineInfos.size(), index);
            } else {
                calcRsiChange(points, klineInfos, n, 0, klineInfos.size(), index);
            }
        }

    }


    private void calcRsiChange(float[] points, List<? extends KLineEntity> klineInfos,
                               int n, int start, int end, int index) {
        double upPriceRma = 0;
        double downPriceRma = 0;

        for (int i = start; i < end; i++) {
            double rsi = Float.MIN_VALUE;
            if (i == n) {
                double upPrice = 0;
                double downPrice = 0;
                for (int k = 1; k <= n; k++) {
                    KLineEntity kLineEntity = klineInfos.get(k);
                    double close = kLineEntity.getClosePrice();
                    double lastClose = klineInfos.get(k - 1).getClosePrice();
                    upPrice += Math.max(close - lastClose, 0);
                    downPrice += Math.max(lastClose - close, 0);
                }
                upPriceRma = upPrice / n;
                downPriceRma = downPrice / n;
                rsi = calcRsi(upPriceRma, downPriceRma);
            } else if (i > n) {
                double close = klineInfos.get(i).getClosePrice();
                double lastClose = klineInfos.get(i - 1).getClosePrice();

                double upPrice = Math.max(close - lastClose, 0);
                double downPrice = Math.max(lastClose - close, 0);

                upPriceRma = (upPrice + (n - 1) * upPriceRma) / n;
                downPriceRma = (downPrice + (n - 1) * downPriceRma) / n;
                rsi = calcRsi(upPriceRma, downPriceRma);
            }
            switch (index) {
                case 1:
                    klineInfos.get(i).setrOne((float) rsi);
                    points[Constants.getCount() * i + Constants.INDEX_RSI_1] = (float) rsi;
                    break;
                case 2:
                    klineInfos.get(i).setrTwo((float) rsi);
                    points[Constants.getCount() * i + Constants.INDEX_RSI_2] = (float) rsi;
                    break;
                case 3:
                    klineInfos.get(i).setrThree((float) rsi);
                    points[Constants.getCount() * i + Constants.INDEX_RSI_3] = (float) rsi;
                    break;
            }
        }
    }

    private double calcRsi(double upPriceRma, double downPriceRma) {
        if (downPriceRma == 0) {
            return 100;
        } else if (upPriceRma == 0) {
            return 0;
        } else {
            return 100 - (100 / (1 + upPriceRma / downPriceRma));
        }

    }

    private int findStart(List<? extends KLineEntity> klineInfos, int index) {
        for (int i = klineInfos.size() - 1; i > 0; i--) {
            //double  float 互转有精度问题无法精确判断,使用-1000判断 Rsi没有这个值
            switch (index) {
                default:
                case 1:
                    if (klineInfos.get(i).getRsiOne() < -1000) {
                        return i + 1;
                    }
                    break;
                case 2:
                    if (klineInfos.get(i).getRsiTwo() < -1000) {
                        return i + 1;
                    }
                    break;
                case 3:
                    if (klineInfos.get(i).getRsiThree() < -1000) {
                        return i + 1;
                    }
                    break;
            }

        }
        return 0;
    }


    private float getWrValue(List<? extends KLineEntity> dataList, int wr1, int i) {
        if (wr1 != 0 && i >= wr1 - 1) {
            return calcWr(dataList, i, wr1);
        } else {
            return Float.MIN_VALUE;
        }
    }

    private void calcKdj(List<? extends KLineEntity> dataList, int kdjDay, int i, KLineEntity point, float closePrice) {
        float k, d;
        if (i < kdjDay - 1 || 0 == i) {
            point.setK(Float.MIN_VALUE);
            point.setD(Float.MIN_VALUE);
            point.setJ(Float.MIN_VALUE);
        } else {
            int startIndex = i - kdjDay + 1;
            float maxRsi = Float.MIN_VALUE;
            float minRsi = Float.MAX_VALUE;
            for (int index = startIndex; index <= i; index++) {
                maxRsi = Math.max(maxRsi, dataList.get(index).getHighPrice());
                minRsi = Math.min(minRsi, dataList.get(index).getLowPrice());
            }
            float rsv = 100f * (closePrice - minRsi) / (maxRsi - minRsi);
            if (Float.isNaN(rsv) || Float.isInfinite(rsv)) {
                rsv = 0f;
            }
            KLineEntity kLineEntity = dataList.get(i - 1);
            float k1 = kLineEntity.getK();
            if (Float.isNaN(k1)) {
                k1 = 50;
            }
            k = 2f / 3f * (k1 == Float.MIN_VALUE ? 50 : k1) + 1f / 3f * rsv;
            float d1 = kLineEntity.getD();
            d = 2f / 3f * (d1 == Float.MIN_VALUE ? 50 : d1) + 1f / 3f * k;
            point.setK(k);
            point.setD(d);
            point.setJ(3f * k - 2 * d);
        }
    }

    private float calculateEma(List<? extends KLineEntity> list, int n, int index, float preEma) {
        try {
            float y = 0;
            if (index + 1 < n) {
                return y;
            } else if (index + 1 == n) {
                for (int i = 0; i < n; i++) {
                    y += list.get(i).getClosePrice();
                }
                return y / n;
            } else {
                return (preEma * (n - 1) + list.get(index).getClosePrice() * 2) / (n + 1);
            }
        } catch (Exception e) {
            Log.e(TAG, "kline--calculateEma--error-->" + e.getMessage());
            return 0;
        }
    }


    private float calculateDea(List<? extends KLineEntity> list, int l, int m, int index,
                               float preDea,
                               boolean isFirst) {

        try {
            float y = 0;
            if (isFirst) {
                for (int i = l - 1; i <= m + l - 2; i++) {
                    y += list.get(i).getDif();
                }
                return y / m;
            } else {
                return ((preDea * (m - 1) + list.get(index).getDif() * 2) / (m + 1));
            }
        } catch (Exception e) {
            Log.e(TAG, "Kline--calculateDea--error->" + e.getMessage());
            return 0;
        }
    }


    public float calcWr(List<? extends KLineEntity> dataDiction, int nIndex, int n) {

        float lowInNLowsValue = getMin(dataDiction, nIndex, n);   //N日内最低价的最低值
        float highInHighsValue = getMax(dataDiction, nIndex, n);   //N日内最低价的最低值
        float valueSpan = highInHighsValue - lowInNLowsValue;
        if (valueSpan > 0) {
            KLineEntity kLineData = dataDiction.get(nIndex);
            return 100 * (highInHighsValue - kLineData.getClosePrice()) / valueSpan;
        } else
            return 0;

    }


    public float getMin(List<? extends KLineEntity> valuesArray, int fromIndex, int nCount) {
        float result = Float.MAX_VALUE;
        int endIndex = fromIndex - (nCount - 1);
        if (fromIndex >= endIndex) {
            for (int itemIndex = fromIndex + 1; itemIndex > endIndex; itemIndex--) {
                KLineEntity klineData = valuesArray.get(itemIndex - 1);
                float lowPrice = klineData.getLowPrice();
                result = result <= lowPrice ? result : lowPrice;
            }
        }
        return result;
    }


    public float getMax(List<? extends KLineEntity> valuesArray,
                        int fromIndex, int nCount) {
        float result = Float.MIN_VALUE;
        int endIndex = fromIndex - (nCount - 1);
        if (fromIndex >= endIndex) {
            for (int itemIndex = fromIndex + 1; itemIndex > endIndex; itemIndex--) {
                KLineEntity klineData = valuesArray.get(itemIndex - 1);
                float highPrice = klineData.getHighPrice();
                result = result >= highPrice ? result : highPrice;
            }
        }
        return result;
    }

    private float calculateBoll(List<? extends KLineEntity> payloads, int position, int maN) {
        float sum = 0;
        for (int i = position; i >= position - maN + 1; i--) {
            sum = (sum + payloads.get(i).getClosePrice());
        }
        return sum / maN;

    }

    private float STD(List<? extends KLineEntity> payloads, int positon, int maN) {
        float sum = 0f, std = 0f;
        for (int i = positon; i >= positon - maN + 1; i--) {
            sum += payloads.get(i).getClosePrice();
        }
        float avg = sum / maN;
        for (int i = positon; i >= positon - maN + 1; i--) {
            std += (payloads.get(i).getClosePrice() - avg) * (payloads.get(i).getClosePrice() - avg);
        }
        return (float) Math.sqrt(std / maN);
    }
}
