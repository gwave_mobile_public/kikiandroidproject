package com.kikitrade.klinelib.model;

import java.io.Serializable;

public class KKTDModel implements Serializable {
    private int buySetupIndex = 0;
    private int sellSetupIndex = 0;
    private int buyCountdownIndex = 0;
    private int sellCountdownIndex = 0;
    private boolean countdownIndexIsEqualToPreviousElement;
    private boolean sellSetup;
    private boolean buySetup;
    private boolean sellSetupPerfection;
    private boolean buySetupPerfection;
    private boolean bearishFlip;
    private boolean bullishFlip;
    private double TDSTBuy = 0;
    private double TDSTSell = 0;
    private boolean countdownResetForTDST;

    public int getBuySetupIndex() {
        return buySetupIndex;
    }

    public void setBuySetupIndex(int buySetupIndex) {
        this.buySetupIndex = buySetupIndex;
    }

    public int getSellSetupIndex() {
        return sellSetupIndex;
    }

    public void setSellSetupIndex(int sellSetupIndex) {
        this.sellSetupIndex = sellSetupIndex;
    }

    public int getBuyCountdownIndex() {
        return buyCountdownIndex;
    }

    public void setBuyCountdownIndex(int buyCountdownIndex) {
        this.buyCountdownIndex = buyCountdownIndex;
    }

    public int getSellCountdownIndex() {
        return sellCountdownIndex;
    }

    public void setSellCountdownIndex(int sellCountdownIndex) {
        this.sellCountdownIndex = sellCountdownIndex;
    }

    public boolean isCountdownIndexIsEqualToPreviousElement() {
        return countdownIndexIsEqualToPreviousElement;
    }

    public void setCountdownIndexIsEqualToPreviousElement(boolean countdownIndexIsEqualToPreviousElement) {
        this.countdownIndexIsEqualToPreviousElement = countdownIndexIsEqualToPreviousElement;
    }

    public boolean isSellSetup() {
        return sellSetup;
    }

    public void setSellSetup(boolean sellSetup) {
        this.sellSetup = sellSetup;
    }

    public boolean isBuySetup() {
        return buySetup;
    }

    public void setBuySetup(boolean buySetup) {
        this.buySetup = buySetup;
    }

    public boolean isSellSetupPerfection() {
        return sellSetupPerfection;
    }

    public void setSellSetupPerfection(boolean sellSetupPerfection) {
        this.sellSetupPerfection = sellSetupPerfection;
    }

    public boolean isBuySetupPerfection() {
        return buySetupPerfection;
    }

    public void setBuySetupPerfection(boolean buySetupPerfection) {
        this.buySetupPerfection = buySetupPerfection;
    }

    public boolean isBearishFlip() {
        return bearishFlip;
    }

    public void setBearishFlip(boolean bearishFlip) {
        this.bearishFlip = bearishFlip;
    }

    public boolean isBullishFlip() {
        return bullishFlip;
    }

    public void setBullishFlip(boolean bullishFlip) {
        this.bullishFlip = bullishFlip;
    }

    public double getTDSTBuy() {
        return TDSTBuy;
    }

    public void setTDSTBuy(double TDSTBuy) {
        this.TDSTBuy = TDSTBuy;
    }

    public double getTDSTSell() {
        return TDSTSell;
    }

    public void setTDSTSell(double TDSTSell) {
        this.TDSTSell = TDSTSell;
    }

    public boolean isCountdownResetForTDST() {
        return countdownResetForTDST;
    }

    public void setCountdownResetForTDST(boolean countdownResetForTDST) {
        this.countdownResetForTDST = countdownResetForTDST;
    }
}
