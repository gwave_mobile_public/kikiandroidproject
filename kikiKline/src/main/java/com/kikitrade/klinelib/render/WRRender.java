package com.kikitrade.klinelib.render;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;

import androidx.annotation.NonNull;

import com.kikitrade.kikikline.R;
import com.kikitrade.klinelib.base.BaseKChartView;
import com.kikitrade.klinelib.base.BaseRender;
import com.kikitrade.klinelib.utils.Constants;

/*************************************************************************
 * Description   :
 *
 * @PackageName  : com.icechao.klinelib.render
 * @FileName     : WRRender.java
 * @Author       : chao
 * @Date         : 2019/4/8
 * @Email        : icechliu@gmail.com
 * @version      : V1
 *************************************************************************/
public class WRRender extends BaseRender {

    private Paint r1Paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint r2Paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint r3Paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final int indexInterval;
    private final String wr1Text;
    private final String wr2Text;

    public WRRender(Context context) {
        indexInterval = Constants.getCount();
        wr1Text = String.format(context.getString(R.string.k_index_wr), Constants.WR_1);
        wr2Text = String.format(context.getString(R.string.k_index_wr), Constants.WR_2);

    }

    public void setCustomTypeface(Typeface typeface) {
        r1Paint.setTypeface(typeface);
        r2Paint.setTypeface(typeface);
    }

    @Override
    public void render(Canvas canvas, float lastX, float curX, @NonNull BaseKChartView view, int position, float... values) {
        if (Float.MIN_VALUE != values[Constants.INDEX_WR_1]) {
            view.renderChildLine(this,canvas, r1Paint, lastX,
                    values[Constants.INDEX_WR_1],
                    curX,
                    values[Constants.INDEX_WR_1 + indexInterval]);
        }
        if (Float.MIN_VALUE != values[Constants.INDEX_WR_2]) {
            view.renderChildLine(this,canvas, r2Paint, lastX,
                    values[Constants.INDEX_WR_2],
                    curX,
                    values[Constants.INDEX_WR_2 + indexInterval]);
        }
    }

    @Override
    public void renderText(@NonNull Canvas canvas, @NonNull BaseKChartView view, float x, float y, int position, float... values) {
//        IWR point = (IWR) view.getItem(position);
        if (Float.MIN_VALUE != values[Constants.INDEX_WR_1]) {
            canvas.drawText(wr1Text, x, y, r1Paint);
            x += r1Paint.measureText(wr1Text+" ");
            String temp = view.getBaseValueFormatter().format(values[Constants.INDEX_WR_1]) + "  ";
            canvas.drawText(temp, x, y, r1Paint);
            x+= r1Paint.measureText(temp);
        }
        if (Constants.getWr2() > 0 && Float.MIN_VALUE != values[Constants.INDEX_WR_2]){
            canvas.drawText(wr2Text, x, y,r2Paint);
            x+= r2Paint.measureText(wr2Text+" ");
            String r2String = view.getBaseValueFormatter().format(values[Constants.INDEX_WR_2]) + "  ";
            canvas.drawText(r2String,x,y,r2Paint);
        }
    }

    @Override
    public void setItemCount(int mItemCount) {

    }

    @Override
    public void startAnim(BaseKChartView view, float... values) {

    }

    @Override
    public float getMaxValue(float... values) {
        return Math.max(values[Constants.INDEX_WR_1], values[Constants.INDEX_WR_2]);
    }

    @Override
    public float getMinValue(float... values) {
        return Math.min(values[Constants.INDEX_WR_1], values[Constants.INDEX_WR_2]);
    }

    @Override
    public void resetValues() {

    }

    /**
     * 设置%R颜色
     */
    public void setR1Color(int color) {
        r1Paint.setColor(color);
    }

    /**
     * 设置%R颜色
     */
    public void setR2Color(int color) {
        r2Paint.setColor(color);
    }

    /**
     * 设置%R颜色
     */
    public void setR3Color(int color) {
        r3Paint.setColor(color);
    }

    /**
     * 设置曲线宽度
     */
    public void setLineWidth(float width) {
        r1Paint.setStrokeWidth(width);
        r2Paint.setStrokeWidth(width);
        r3Paint.setStrokeWidth(width);
    }

    /**
     * 设置文字大小
     */
    public void setTextSize(float textSize) {
        r1Paint.setTextSize(textSize);
        r2Paint.setTextSize(textSize);
        r3Paint.setTextSize(textSize);
    }
}
