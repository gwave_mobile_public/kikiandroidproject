package com.kikitrade.klinelib.render;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.kikitrade.kikikline.config.KikiShowType;
import com.kikitrade.kikikline.model.KChartNewBean;
import com.kikitrade.kikikline.utils.Utils;
import com.kikitrade.klinelib.base.BaseKChartView;
import com.kikitrade.klinelib.utils.Constants;
import com.kikitrade.klinelib.utils.Status;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/11/1 at 1:56 下午
 * @Description:
 */
public class DataMainRender extends MainRender {

    private String[] strings = new String[8];


    public DataMainRender(Context context) {
        super(context);
    }


    @Override
    public void render(Canvas canvas, float lastX, float curX, @NonNull BaseKChartView view, int position, float... values) {
        super.render(canvas, lastX, curX, view, position, values);
    }


    @Override
    protected void drawSelector(BaseKChartView view, Canvas canvas, float[] values) {
        int index = view.getSelectedIndex();

        KChartNewBean chart = list.get(index);
        strings[0] = view.getTime(index);
        double open = chart.getOpen().doubleValue();
        double close = 0;

        if (chart.getClose() != null) {
            close = chart.getClose().doubleValue();
        }
        double high = chart.getHigh().doubleValue();
        double low = chart.getLow().doubleValue();

        double tempDiffPrice = close - open;

        if (needUnitFormat) {
            strings[1] = Utils.formatNum(chart.getOpen(), 2);
            strings[2] = Utils.formatNum(chart.getClose(), 2);
            strings[3] = Utils.formatNum(chart.getHigh(), 2);
            strings[4] = Utils.formatNum(chart.getLow(), 2);
            if (tempDiffPrice > 0) {
                strings[5] = "+" + Utils.formatNum(new BigDecimal(tempDiffPrice), 2);
            } else if (tempDiffPrice < 0) {
                strings[5] = "-" + Utils.formatNum(BigDecimal.valueOf(Math.abs(tempDiffPrice)), 2);
            } else {
                strings[5] = "0.00";
            }
        } else {
            strings[1] = getValueFormatter().format(open);
            strings[2] = getValueFormatter().format(close);
            strings[3] = getValueFormatter().format(high);
            strings[4] = getValueFormatter().format(low);
            if (tempDiffPrice > 0) {
                strings[5] = "+" + getValueFormatter().format(tempDiffPrice);
            } else if (tempDiffPrice < 0) {
                strings[5] = getValueFormatter().format(tempDiffPrice);
            } else {
                strings[5] = getValueFormatter().format(0);
            }
        }

        if (open == 0) {
            strings[6] = "0.00" + "%";
        } else {
            if ((tempDiffPrice * 100) / open > 0) {
                strings[6] = "+" + String.format("%.2f", (tempDiffPrice * 100) / open) + "%";
            } else if ((tempDiffPrice * 100) / open < 0) {
                strings[6] = String.format("%.2f", (tempDiffPrice * 100) / open) + "%";
            } else {
                strings[6] = "0.00" + "%";
            }
        }
        if (open == 0) {
            strings[7] = "0";
        } else {
            strings[7] = String.format("%.2f", (high - low) / open * 100) + "%";
        }

        float width = 0, left, top = margin;
        //上下多加两个padding值的间隙
        int length = strings.length;
        float height = selectedInfoVerticalMargin * ((length - 1) + 2) + selectedTextHeight * length;
        for (int i = 0; i < length; i++) {
            String tempString = marketInfoText[i] + strings[i];
            width = Math.max(width, selectorTextPaint.measureText(tempString));
        }
        width += selectedInfoHorizontalPadding * 2;

        float x = view.getX(index) + view.getTranslateX();
        if (x > view.getChartWidth() / 2) {
            left = margin / 2;
        } else {
            left = view.getChartWidth() - width - margin * 2;
        }

        float right = left + width + selectedInfoHorizontalPadding + selectedInfoBoxTextHorizontalMargin;

        RectF r = new RectF(left, top, right, top + height);
        canvas.drawRoundRect(r, padding / 2, padding / 2, selectorBackgroundPaint);
        canvas.drawRoundRect(r, padding / 2, padding / 2, selectorBorderPaint);
        float y = top + selectedInfoVerticalMargin + selectedTextBaseLine;
        float tempX = right - selectedInfoBoxTextHorizontalMargin;
        for (int i = 0; i < length; i++) {
            String s = strings[i];
            // 标题 key
            canvas.drawText(marketInfoText[i], left + selectedInfoBoxTextHorizontalMargin, y, selectorTextPaint);
            // value 值
            if (i == 5 || i == 6) {
                Paint paint;
                if (tempDiffPrice > 0) {
                    paint = upPaint;
                } else if (tempDiffPrice < 0) {
                    paint = downPaint;
                } else {
                    paint = greyPaint;
                }
                canvas.drawText(s, tempX - selectorTextPaint.measureText(s), y, paint);
            } else {
                canvas.drawText(s, tempX - selectorTextPaint.measureText(s), y, selectorTextPaint);
            }
            y += selectedTextHeight + selectedInfoVerticalMargin;
        }

    }

    @Override
    public void renderMaxMinValue(Canvas canvas, BaseKChartView view, float maxX, float mainHighMaxValue, float minX, float mainLowMinValue) {
        if ((view.getKlineStatus() != Status.KLINE_SHOW_TIME_LINE)) {
            //绘制最大值和最小值
            float y = view.getMainY(mainLowMinValue);
            //计算显示位置
            y = fixTextYBaseBottom(y);
            String LowString;
            float stringWidth, screenMid = view.getTranslationScreenMid();

            String low = getValueFormatter().format(list.get(view.getMainMinIndex()).getLow().doubleValue());
            String high = getValueFormatter().format(list.get(view.getMainMaxIndex()).getHigh().doubleValue());
//            if (needUnitFormat) {
//                low = Utils.formatNum(list.get(view.getMainMinIndex()).getLow(), 2);
//                high = Utils.formatNum(list.get(view.getMainMaxIndex()).getHigh(), 2);
//            }
            if (minX < screenMid) {
                LowString = "── " + low;
            } else {
                LowString = low + " ──";
                stringWidth = maxMinPaint.measureText(LowString);
                minX -= stringWidth;
            }
            canvas.drawText(LowString, minX, y, maxMinPaint);

            y = view.getMainY(mainHighMaxValue);
            String highString;
            y = fixTextYBaseBottom(y);
            if (maxX < screenMid) {
                highString = "── " + high;
            } else {
                highString = high + " ──";
                stringWidth = maxMinPaint.measureText(highString);
                maxX -= stringWidth;
            }

            canvas.drawText(highString, maxX, y, maxMinPaint);
        }
    }
}
