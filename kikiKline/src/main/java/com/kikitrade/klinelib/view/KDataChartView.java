package com.kikitrade.klinelib.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;

import com.kikitrade.kikikline.model.KChartNewBean;
import com.kikitrade.klinelib.render.DataMainRender;
import com.kikitrade.klinelib.render.VolumeRender;

import java.util.List;

/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/11/10 at 5:57 下午
 * @Description:
 */
public class KDataChartView extends KChartView {

    private onReceiveBaseCacheError onReceiveBaseCacheError;

    public KDataChartView(Context context) {
        super(context);
    }

    public KDataChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KDataChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void initView(Context context) {
        mainRender = new DataMainRender(context);
        volumeRender = new VolumeRender(context);
    }

    public void setPointsData(List<KChartNewBean> values) {
        if (mainRender == null) {
            return;
        }
        list = values;
        mainRender.setOriginDatas(values);
    }

    public void setNeedUnitFormat() {
        needUnitFormat = true;
        if (mainRender == null) {
            return;
        }
        mainRender.setNeedUnitFormat();
    }

    @Override
    protected void onReceiveError() {
        super.onReceiveError();
        if (onReceiveBaseCacheError == null) {
            return;
        }
        onReceiveBaseCacheError.onReceiveError();
    }


    public void setListener(onReceiveBaseCacheError listener) {
        this.onReceiveBaseCacheError = listener;
    }

    public interface onReceiveBaseCacheError {
        void onReceiveError();
    }


}
