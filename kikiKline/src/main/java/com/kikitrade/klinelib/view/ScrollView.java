package com.kikitrade.klinelib.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

/*************************************************************************
 * Description   : 不拦截横向滑动的ScrollView
 *
 * @PackageName  : com.icechao.klinelib.view
 * @FileName     : ScrollView.java
 * @Author       : chao
 * @Date         : 2019/4/10
 * @Email        : icechliu@gmail.com
 * @version      : V1
 *************************************************************************/
public class ScrollView extends NestedScrollView {

    private boolean isOldEvent;
    // 分别记录上次滑动的坐标
    private int mLastX = 0;
    private int mLastY = 0;


    public ScrollView(@NonNull Context context) {
        super(context);
    }

    public ScrollView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (isOldEvent) {
            return false;
        }
        int x = (int) ev.getX();
        int y = (int) ev.getY();
        boolean isScale = ev.getPointerCount() >= 2;

        if (isScale){
            return false;
        }

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_MOVE:

                int deltaX = x - mLastX;
                int deltaY = y - mLastY;
                if (Math.abs(deltaX) > Math.abs(deltaY)) {
                    isOldEvent = true;
                    return false;
                }
                break;

        }
        mLastX = x;
        mLastY = y;
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_UP:
                isOldEvent = false;
                break;
        }

        return super.dispatchTouchEvent(ev);
    }

}

