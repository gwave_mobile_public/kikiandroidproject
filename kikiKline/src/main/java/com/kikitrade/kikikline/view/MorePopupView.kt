package com.kikitrade.kikikline.view

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.databinding.DataBindingUtil
import com.kikitrade.kikikline.config.KikiTabBarType
import com.kikitrade.kikikline.R
import com.kikitrade.kikikline.databinding.ViewMoreSelectorBinding
import com.lxj.xpopup.XPopup
import com.lxj.xpopup.enums.PopupPosition
import com.lxj.xpopup.impl.PartShadowPopupView


class MorePopupView(private val builder: Builder) :
    PartShadowPopupView(builder.context),
    RadioGroup.OnCheckedChangeListener {

    lateinit var binding: ViewMoreSelectorBinding

    override fun addInnerContent() {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.view_more_selector,
            attachPopupContainer,
            false
        )
        binding.apply {
            onCheckedChanged = this@MorePopupView
            attachPopupContainer.addView(this.root)
        }
    }

    override fun onCreate() {
        super.onCreate()

        builder.moreTimeTypes?.let {
            if (builder.moreTitles == null) {
                return
            }

            builder.moreTitles?.forEachIndexed { index, title ->
                binding.moreRadioGroup.getChildAt(index).apply {
                    if (this is RadioButton) {
                        text = title
                        this.tag = it[index]
                        typeface = builder.mTypeface
                        if (builder.moreInitialTimeType == it[index]) {
                            isChecked = true
                        }
                    }
                }
            }
        }
    }

    /**
     *  外部调用 show  方法
     */
    fun showDialog(view: View) {
        XPopup.Builder(builder.context)
            .atView(view)
            .popupPosition(PopupPosition.Left)
            .isClickThrough(true)
            .hasShadowBg(false)
            .isViewMode(true)
            .isClickThrough(true)
            .asCustom(this)
            .show()
    }


    class Builder(val context: Context) {

        var onGetMoreDetailClickListener: OnGetMoreDetailClickListener? = null
            private set

        var moreTimeTypes: Array<KikiTabBarType>? = null
            private set

        var moreTitles: Array<String>? = null
            private set

        var moreInitialTimeType: KikiTabBarType? = null
            private set

        var mTypeface: Typeface? = null
            private set

        fun setMoreDetailClickListener(listener: OnGetMoreDetailClickListener) = apply {
            this.onGetMoreDetailClickListener = listener
        }

        fun setMoreOriginData(
            timeTypes: Array<KikiTabBarType>?,
            titles: Array<String>?, initialTimeType: KikiTabBarType?
        ) = apply {
            this.moreTimeTypes = timeTypes
            this.moreTitles = titles
            this.moreInitialTimeType = initialTimeType
        }

        fun setCustomTypeface(typeface: Typeface) {
            this.mTypeface = typeface
        }

        fun build(): MorePopupView {
            return MorePopupView(this)
        }

    }

    override fun onDismiss() {
        super.onDismiss()
        builder.onGetMoreDetailClickListener?.onMoreDismiss()
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        val button = group?.findViewById<RadioButton>(checkedId)
        if (button?.text == null) {
            return
        }
        if (!button.isChecked) {
            return
        }
        builder.onGetMoreDetailClickListener?.getMoreTime(button)
        dismiss()
    }

    interface OnGetMoreDetailClickListener {
        fun getMoreTime(button: RadioButton)
        fun onMoreDismiss()
    }

}
