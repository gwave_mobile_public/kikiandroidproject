package com.kikitrade.kikikline.view

import android.content.*
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import com.kikitrade.kikikline.R
import com.kikitrade.kikikline.config.*
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_CLOSE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_HIGH
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_LOW
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_OPEN
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_RISE_AND_FALL
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_RISE_AND_FALL_RATE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_TIME
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_FIFTEEN_MINUTE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_FIVE_MINUTE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_FOUR_HOUR
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_DAY
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_HOUR
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_MINUTE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_MONTH
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_WEEK
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_YEAR
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_THIRTY_MINUTE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHAT_DATA_DES_RISE_AND_FALL
import com.kikitrade.kikikline.databinding.ChartContainerDataViewBinding
import com.kikitrade.kikikline.model.KChartNewBean
import com.kikitrade.kikikline.model.KChatViewBaseBean
import com.kikitrade.kikikline.network.KKNetworkCallback
import com.kikitrade.kikikline.utils.I18NUtil
import com.kikitrade.kikikline.utils.StorageUtil
import com.kikitrade.klinelib.adapter.KLineChartAdapter
import com.kikitrade.klinelib.callback.SlidListener
import com.kikitrade.klinelib.formatter.DateFormatter
import com.kikitrade.klinelib.formatter.ValueFormatter
import com.kikitrade.klinelib.utils.DpUtil
import com.kikitrade.klinelib.utils.LogUtil
import com.kikitrade.klinelib.utils.Status.*
import com.kikitrade.klinelib.view.KDataChartView
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class ChartViewDataContainerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {

    companion object {
        const val TAG: String = "ChartView_Container"

        const val MINUTE: String = "mm"

        //24小时进制
        const val HOUR: String = "HH"

    }

    private var requestJod: Job? = null
    private var request = false

    var globalConfig: CryptoChartGlobalConfig? = null
    var binding: ChartContainerDataViewBinding
    private lateinit var adapter: KLineChartAdapter<KChartNewBean>
    private var onChartViewLoadingInterface: OnChartViewLoadingInterface? =
        null
    private var locale: String? = null
    private var timeTypes: MutableList<String>? = null
    private var selectedTab = 0
    private val children = mutableListOf<TextView>()
    private var repeatRequestTimes = 0


    init {
        val layoutInflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.chart_container_data_view,
            this,
            true
        )
    }


    fun config(config: CryptoChartGlobalConfig?) {
        if (config == null) {
            return
        }
        repeatRequestTimes = 0
        globalConfig = config
        if (config.environment?.isEmpty() == true || config.environment == null) {
            config.environment = "beta"
        }
        if (config.coinCode?.isEmpty() == true || config.coinCode == null) {
            config.coinCode = "BTC_USDT"
        }

        val selectedType = StorageUtil.getLocalStorageString(context, globalConfig?.coinCode ?: "")
        globalConfig?.timeType = when (selectedType.isNullOrEmpty()) {
            true -> config.timeType
            else -> selectedType
        }

        ChartManager.INSTANCE.timeType = ChartManager.INSTANCE.getTimeType(config.timeType)
//        Log.i(TAG, "initView: ${ChartManager.INSTANCE.timeType}")
        // origin config
        adapter = KLineChartAdapter<KChartNewBean>()
        ChartManager.INSTANCE.config = this.globalConfig
        initView()
        binding.chartView.setAdapter(adapter)
        setupData()
        requestChartVieData()
    }

    // for RN project
    fun setSPFileName(name: String) {
        StorageUtil.setSharedPreferencesFileName(name)
    }

    fun setCustomTypeface(typeface: Typeface){
        binding.chartView.setCustomTypeface(typeface)
    }

    private fun setupData() {
        locale = if (globalConfig != null && globalConfig?.getLocal() != null) {
            ChartManager.INSTANCE.getLocale(globalConfig?.getLocal())
        } else {
            ChartManager.INSTANCE.defaultLocale
        }
        val descriptions = arrayOf(
            I18NUtil.getString(CHART_DESCRIPTION_TIME, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_OPEN, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_CLOSE, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_HIGH, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_LOW, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_RISE_AND_FALL, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_RISE_AND_FALL_RATE, locale, context),
            I18NUtil.getString(CHAT_DATA_DES_RISE_AND_FALL, locale, context),
        )
        binding.chartView.setSelectedInfoLabels(descriptions)
        // 处理需要格式化的数据
        if (globalConfig?.needUnitFormat == true) {
            binding.chartView.setNeedUnitFormat()
        }
    }

    private fun initView() {
        binding.chartView.apply {
            setTimeLineEndRadius(10f)
                .setShowType(KikiShowType.DataKline)
                .setLoadingView(ProgressBar(context))
                .setAnimLoadData(false)
                .setGridNumbers(3)
                .setShowPriceLine(false)
                .setMacdHollow(NONE_HOLLOW)
                .setPriceLabelInLineClickable(true)
                .setLabelSpace(DpUtil.Dp2Px(context, 55f).toFloat())
                .setLogoAlpha(100)
                .setKlineSupportStatus(KLINE_SUPPORT_DATA)
                .setKlineState(K_LINE_SHOW_CANDLE_LINE)
                .setVolShowState(false)
                .changeMainDrawType(MAIN_NONE)
                .setYLabelState(LABEL_WITH_GRID)//set right can over range
                .setOverScrollRange(
                    (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.width.div(
                        4
                    ).toFloat()
                ) //show loading View
                .setCandleHollow(NONE_HOLLOW)
                .setBetterX(true) //set slid listener
                .setSlidListener(object : SlidListener {
                    override fun onSlidLeft() {
                        LogUtil.e("onSlidLeft")
                    }

                    override fun onSlidRight() {
                        LogUtil.e("onSlidRight")
                    }
                })
                //set main  formater
                .setMainValueFormatter(object : ValueFormatter() {

                    override fun format(value: Double): String {
                        return String.format(
                            Locale.CHINA,
                            "%,.0${globalConfig?.coinPrecision ?: 2}f",
                            value
                        )
                    }
                }) //set vol value  formater
                .dateTimeFormatter = object : DateFormatter() {
                override fun format(date: Date): String {
                    return ChartManager.INSTANCE.getTimeFormat(date, ChartManager.INSTANCE.timeType)
                }
            }
            scaleXMax = 3f
            scaleXMin = 0.1f
            setListener {
                repeatRequestTimes++
                if (repeatRequestTimes > 3) {
                    return@setListener
                }
                requestChartVieData()
            }
        }

    }

    override fun requestLayout() {
        super.requestLayout()
        post(measureAndLayout)
    }

    private val measureAndLayout = Runnable {
        measure(
            View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY),
            View.MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
        )
        layout(left, top, right, bottom)
    }


    private fun requestChartVieData() {
        binding.chartView.hideSelectData()
        if (repeatRequestTimes == 0) {
            onChartViewLoadingInterface?.showLoading(
                true,
                ChartManager.INSTANCE.timeType?.getTime() ?: ""
            )
        }
        if (globalConfig == null) {
            return
        }
        binding.chartView.setScale(1.0f)
        request = false
        // 取消上次网络请求，防止两次请求造成页面闪烁
        requestJod?.cancel()
        if (context is LifecycleOwner) {
            requestJod = (context as LifecycleOwner).lifecycleScope.launch {
                val data = requestData()
                onChartViewLoadingInterface?.showLoading(
                    false,
                    ChartManager.INSTANCE.timeType?.getTime() ?: ""
                )
                data?.let {
                    if (timeTypes?.isNotEmpty() == true) {
                        return@launch
                    }
                    initTabBar(it)
                    timeTypes = it
                }
            }
        }
    }

    private fun initTabBar(it: MutableList<String>) {
        if (it.isEmpty()) {
            return
        }
        children.clear()
        it.forEachIndexed { index, time ->
            val tab = getTabBarName(time)
            val view = LayoutInflater.from(context).inflate(R.layout.chat_title_item, null)
            val text = view.findViewById<TextView>(R.id.tab_name);
            text.text = tab?.name
            val params = LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                1.0f
            )
            if (globalConfig?.timeType == time) {
                selectedTab = index
            }
            view.findViewById<LinearLayout>(R.id.tab_background).apply {
                layoutParams = params
                setOnClickListener {
                    repeatRequestTimes = 0
                    setOtherClickStatus(index)
                    tab?.let {
                        globalConfig?.timeType = tab.tab.getTime()
                        ChartManager.INSTANCE.apply {
                            config = globalConfig
                            timeType = tab.tab
                        }
                        StorageUtil.saveLocalStorageString(
                            context,
                            globalConfig?.coinCode ?: "",
                            tab.tab.getTime()
                        )
                        requestChartVieData()
                    }
                }
            }
            children.add(text)
            binding.container.addView(view)
        }

        if (globalConfig?.timeType.isNullOrEmpty()) {
            selectedTab = 0
        }
//        Log.i(TAG, "initTabBar: ${globalConfig?.timeType}  -->${children.size} --->${selectedTab}")
        setOtherClickStatus(selectedTab)
    }

    private fun setOtherClickStatus(current: Int) {
        children.forEachIndexed { index, view ->
            if (current != index) {
                view.background = ContextCompat.getDrawable(context, R.drawable.tab_bar_data_normal)
                view.setTextColor(ContextCompat.getColor(context, R.color.color_FF1F2126))
            } else {
                view.background =
                    ContextCompat.getDrawable(context, R.drawable.tab_bar_data_button_selected)
                view.setTextColor(ContextCompat.getColor(context, R.color.color_white))
            }
        }
    }

    private fun getTabBarName(time: String): DataKlineTab? {

        return when (time) {
            KikiTabBarType.OneMinute.getTime() -> {
                DataKlineTab(
                    I18NUtil.getString(CHART_TIME_ON_ONE_MINUTE, locale, context),
                    KikiTabBarType.OneMinute
                )
            }
            KikiTabBarType.FiveMinutes.getTime() -> {
                DataKlineTab(
                    I18NUtil.getString(CHART_TIME_ON_FIVE_MINUTE, locale, context),
                    KikiTabBarType.FifteenMinutes
                )
            }
            KikiTabBarType.FifteenMinutes.getTime() -> {
                DataKlineTab(
                    I18NUtil.getString(CHART_TIME_ON_FIFTEEN_MINUTE, locale, context),
                    KikiTabBarType.FifteenMinutes
                )
            }
            KikiTabBarType.ThirtyMinutes.getTime() -> {

                DataKlineTab(
                    I18NUtil.getString(CHART_TIME_ON_THIRTY_MINUTE, locale, context),
                    KikiTabBarType.ThirtyMinutes
                )
            }
            KikiTabBarType.OneHour.getTime() -> {
                DataKlineTab(
                    I18NUtil.getString(CHART_TIME_ON_ONE_HOUR, locale, context),
                    KikiTabBarType.OneHour
                )
            }
            KikiTabBarType.FourHours.getTime() -> {
                DataKlineTab(
                    I18NUtil.getString(CHART_TIME_ON_FOUR_HOUR, locale, context),
                    KikiTabBarType.FourHours
                )
            }
            KikiTabBarType.OneDay.getTime() -> {
                DataKlineTab(
                    I18NUtil.getString(CHART_TIME_ON_ONE_DAY, locale, context),
                    KikiTabBarType.OneDay
                )
            }
            KikiTabBarType.OneWeek.getTime() -> {

                DataKlineTab(
                    I18NUtil.getString(CHART_TIME_ON_ONE_WEEK, locale, context),
                    KikiTabBarType.OneWeek
                )
            }
            KikiTabBarType.OneMonth.getTime() -> {

                DataKlineTab(
                    I18NUtil.getString(CHART_TIME_ON_ONE_MONTH, locale, context),
                    KikiTabBarType.OneMonth
                )
            }
            KikiTabBarType.OneYear.getTime() -> {

                DataKlineTab(
                    I18NUtil.getString(CHART_TIME_ON_ONE_YEAR, locale, context),
                    KikiTabBarType.OneYear
                )
            }
            else -> {
                null
            }

        }
    }


    private suspend fun requestData(): MutableList<String>? =
        withContext(Dispatchers.IO) {
//            adapter.resetData(ArrayList<KChartNewBean>())
            suspendCoroutine { continuation ->
                ChartManager.INSTANCE
                    .requestDataKline(object :
                        KKNetworkCallback {
                        override fun onFinish(chartModel: KChatViewBaseBean) {
                            // run on Thread
                            chartModel.klines?.let {
//                                Log.i(TAG, "onFinish--->${it.size}")
                                adapter.resetData(it)
                                binding.chartView.setPointsData(it)
                            }
                            continuation.resume(chartModel.timeTypes)
                        }

                        override fun onError(e: Exception?) {
                            e?.printStackTrace()
                            continuation.resume(null)
                        }
                    })
            }
        }

    // for RN project
    fun setChartViewInterface(listener: OnChartViewLoadingInterface) {
        this.onChartViewLoadingInterface = listener
    }


    interface OnChartViewLoadingInterface {
        fun showLoading(loading: Boolean, resolution: String)
    }

}