package com.kikitrade.kikikline.view

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.kikitrade.kikikline.R
import com.kikitrade.kikikline.config.*
import com.kikitrade.kikikline.databinding.ViewChartTabBarBinding
import com.kikitrade.kikikline.utils.StorageUtil

/**
 * the tabBar of chartView
 * show time and other filter condition
 */
class KKCryptoChartTabBar(context: Context, attributeSet: AttributeSet) :
    LinearLayout(context, attributeSet), View.OnClickListener,
    RadioGroup.OnCheckedChangeListener {

    private val TAG: String = "KKCryptoChartTabBar"
    var binding: ViewChartTabBarBinding
    private var isInitialed = false
    private var currentButton: RadioButton? = null
    private var originMoreTitle: String? = null
    private var originIndicatorTitle: String? = null
    private lateinit var triangleSign: String
    private var moreTimeTypes: Array<KikiTabBarType>? = null
    private var moreTitles: Array<String>? = null
    private var moreInitialTimeType: KikiTabBarType? = null
    private var indicatorTitles: Array<String>? = null
    private var onTabBarClickListener: OnTabBarClickListener? = null

    // Temporary storage Indicator selected data
    private var tempLocalMainIndicator: KikiMainIndicator? = null

    private var mainIndicators: Array<KikiMainIndicator>? = null
    private var otherIndicators: Array<KikiOtherIndicator>? = null
    private var mTypeface:Typeface?=null

    init {
        val layoutInflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding =
            DataBindingUtil.inflate(
                layoutInflater, R.layout.view_chart_tab_bar, this, true
            )
        binding.apply {
            onClick = this@KKCryptoChartTabBar
            onChecked = this@KKCryptoChartTabBar
        }
    }

    fun  setCustomTypeface(typeface: Typeface) {
        this.mTypeface= typeface
    }
    // setting tab origin data
    fun setOriginData(
        types: Array<KikiTabBarType?>,
        titles: Array<String?>,
        initialTimeType: String?,
        locale: String?, triangle: String
    ) {
        triangleSign = triangle
        if (types.size != titles.size || isInitialed) {
            return
        }

        titles.forEachIndexed { index, title ->
            when (index) {
                titles.size - 1 -> {
                    originIndicatorTitle = title
                    binding.radioButtonIndicator.apply {
                        text = setupTriangleButtonTitle(title, true)
                        typeface=mTypeface
                    }
                }
                titles.size - 2 -> {
                    originMoreTitle = title
                    binding.radioButtonMore.apply {
                        text = setupTriangleButtonTitle(title, true)
                        typeface=mTypeface
                    }
                }
                else -> {
                    binding.radioGroup.getChildAt(index).apply {
                        this as Button
                        text = title
                        tag = types[index]
                        typeface = mTypeface
                    }
                }
            }

            types[index]?.let {
                if (it.getTime().equals(initialTimeType, ignoreCase = true)) {
                    currentButton = binding.radioGroup.getChildAt(index) as RadioButton?
                    currentButton?.isChecked = true
                }
            }
        }
        // set more show default data when last time selected more data
        if (currentButton == null) {
            val timeType = ChartManager.INSTANCE.getTimeType(initialTimeType)
            val title =
                ChartManager.INSTANCE.getTimeTitle(timeType, locale, context)
            updateMoreTabItem(title)
        }

        isInitialed = true
    }


    fun setMoreButtonOriginData(
        timeTypes: Array<KikiTabBarType>,
        titles: Array<String>, initialTimeType: KikiTabBarType?
    ) {
        moreTimeTypes = timeTypes
        moreTitles = titles
        moreInitialTimeType = initialTimeType
    }

    fun setIndicatorButtonOriginData(
        main: Array<KikiMainIndicator>,
        other: Array<KikiOtherIndicator>,
        titles: Array<String>
    ) {
        mainIndicators = main
        otherIndicators = other
        indicatorTitles = titles
        tempLocalMainIndicator = ChartManager.INSTANCE.mainIndicator
    }

    override fun requestLayout() {
        super.requestLayout()
        post(measureAndLayout)
    }

    private val measureAndLayout = Runnable {
        measure(
            MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
            MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
        )
        layout(left, top, right, bottom)
    }

    private fun changeIndicatorStyle(isSelected: Boolean) {
        binding.radioButtonIndicator.apply {
            text = setupTriangleButtonTitle(originIndicatorTitle, isSelected)
        }
    }

    private fun changeMoreStyle(isSelected: Boolean) {
        binding.radioButtonMore.apply {
            text = setupTriangleButtonTitle(originMoreTitle, isSelected)
        }

    }


    private fun setupTriangleButtonTitle(title: String?, isNormal: Boolean): SpannableString? {
        if (title == null || title.isEmpty() || title.length < 3) {
            return null
        }
        val secondColor = if (isNormal) {
            ContextCompat.getColor(context, R.color.chart_tab_bar_normal_title_color)
        } else {
            ContextCompat.getColor(context,R.color.color_FF1F2126)
        }
        return setupTriangleButtonTitle(
            title, ContextCompat.getColor(context, R.color.chart_tab_bar_normal_title_color),
            secondColor
        )
    }

    private fun setupTriangleButtonTitle(
        title: String?,
        firstColor: Int,
        secondColor: Int
    ): SpannableString? {
        if (title == null || title.isEmpty() || title.length < 2) {
            return null
        }
        // More / Indicator
        val spannableString = SpannableString(title)
        val firstSizeSpan = AbsoluteSizeSpan(14, true)
        val firstColorSpan = ForegroundColorSpan(firstColor)
        spannableString.setSpan(
            firstSizeSpan,
            0,
            title.length - 1,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        spannableString.setSpan(
            firstColorSpan,
            0,
            title.length - 1,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        // ◢
        val secondColorSpan = ForegroundColorSpan(secondColor)
        val secondSizeSpan = AbsoluteSizeSpan(7, true)
        spannableString.setSpan(
            secondColorSpan, title.length - 1,
            title.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        spannableString.setSpan(
            secondSizeSpan, title.length - 1, title.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return spannableString
    }

    private fun updateMoreTabItem(title: String) {
        val drawable = ContextCompat.getDrawable(context, R.drawable.tab_bar_data_button_selected)
        binding.radioButtonMore.apply {
            background = drawable
            setTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.chart_tab_bar_text_pressed
                )
            )
            text = setupTriangleButtonTitle(
                title + triangleSign,
                ContextCompat.getColor(context, R.color.chart_tab_bar_text_pressed),
                ContextCompat.getColor(context, R.color.chart_tab_bar_text_pressed)
            )
        }
        binding.radioGroup.clearCheck()
        return
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.radioButtonMore -> {
                if (binding.radioButtonMore.text.toString() == originMoreTitle) {
                    changeMoreStyle(false)
                }
                showMoreView(view)
            }
            binding.radioButtonIndicator -> {
                changeIndicatorStyle(false)
                showIndicatorView(view)
            }
        }
    }

    private fun showMoreView(view: View) {
        MorePopupView.Builder(context)
            .setMoreDetailClickListener(object : MorePopupView.OnGetMoreDetailClickListener {
                override fun getMoreTime(button: RadioButton) {
//                    Log.d(TAG, "  get more tag --->${button.tag}")
                    updateMoreTabItem(button.text.toString())
                    button.tag.apply {
                        this as KikiTabBarType
                        moreInitialTimeType = this
                        onTabBarClickListener?.onSelectTime(this)
                        StorageUtil.saveTimeType(context, this.getTime())
                    }
                }

                override fun onMoreDismiss() {
                    if (binding.radioButtonMore.text.toString() == originMoreTitle) {
                        changeMoreStyle(true)
                    }
                }

            })
            .setMoreOriginData(moreTimeTypes, moreTitles, moreInitialTimeType)
            .build()
            .showDialog(view)
    }

    private fun showIndicatorView(view: View) {
        IndicatorPopupView.Builder(context)
            .setOriginData(mainIndicators, otherIndicators, indicatorTitles)
            .setMultiSelect(true)
            .setClickListener(object : IndicatorPopupView.OnIndicatorSelectedListener {
                override fun onGetMainIndicator(mainIndicator: KikiMainIndicator?) {
                    onTabBarClickListener?.setMainIndicator(mainIndicator)
                }

                override fun onGetOtherIndicator(otherIndicators: MutableList<KikiOtherIndicator>?) {
                    onTabBarClickListener?.setOtherIndicator(otherIndicators)
                }
                override fun onIndicatorDismiss() {
                    changeIndicatorStyle(true)
                }
            })
            .build()
            .showDialog(view)
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        val button = group?.findViewById<RadioButton>(checkedId)
        if (button?.text == null || !button.isChecked) {
            return
        }
        changeMoreStyle(true)
        binding.radioButtonMore.background = null

        button.tag?.apply {
            this as KikiTabBarType
            onTabBarClickListener?.onSelectTime(this)
            moreInitialTimeType = null
            StorageUtil.saveTimeType(context, this.getTime())
        }
    }

    fun setOnTabBarClickListener(clickListener: OnTabBarClickListener) {
        this.onTabBarClickListener = clickListener
    }

    interface OnTabBarClickListener {

        fun onSelectTime(kikiTabBarType: KikiTabBarType)
        fun setMainIndicator(main: KikiMainIndicator?)
        fun setOtherIndicator(other: List<KikiOtherIndicator>?)
    }
}