package com.kikitrade.kikikline.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.kikitrade.kikikline.R;

/**
 * 作为 k 线的横屏 展示页面
 */
public class FullScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);
    }
}