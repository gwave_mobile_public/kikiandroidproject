package com.kikitrade.kikikline.view

import android.content.*
import android.graphics.Typeface
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.IBinder
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.WindowManager
import android.view.WindowMetrics
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.kikitrade.kikikline.R
import com.kikitrade.kikikline.config.*
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_CLOSE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_HIGH
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_LOW
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_ONEVOL
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_OPEN
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_RISE_AND_FALL
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_RISE_AND_FALL_RATE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_DESCRIPTION_TIME
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_MAIN
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_SUB
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TAB_BAR_INDICATOR
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TAB_BAR_MORE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TAB_BAR_TRIANGLE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_FIFTEEN_MINUTE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_FIVE_MINUTE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_FOUR_HOUR
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_DAY
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_HOUR
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_MINUTE
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_MONTH
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_WEEK
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_ONE_YEAR
import com.kikitrade.kikikline.config.ChartManager.Companion.CHART_TIME_ON_THIRTY_MINUTE
import com.kikitrade.kikikline.databinding.ViewContainerKkcryptochartBinding
import com.kikitrade.kikikline.model.KChartNewBean
import com.kikitrade.kikikline.model.KChatViewBaseBean
import com.kikitrade.kikikline.network.KKNetworkCallback
import com.kikitrade.kikikline.utils.ConvertTimeData.Companion.getDayTimeData
import com.kikitrade.kikikline.utils.ConvertTimeData.Companion.getHourTimeData
import com.kikitrade.kikikline.utils.ConvertTimeData.Companion.getMinuteTimeData
import com.kikitrade.kikikline.utils.ConvertTimeData.Companion.getMonthTimeData
import com.kikitrade.kikikline.utils.ConvertTimeData.Companion.getWeekTimeData
import com.kikitrade.kikikline.utils.ConvertTimeData.Companion.getYearTimeData
import com.kikitrade.kikikline.utils.DateUtil.Companion.dateToString
import com.kikitrade.kikikline.utils.I18NUtil
import com.kikitrade.kikikline.utils.StorageUtil
import com.kikitrade.kikikline.websocket.JWebSocketClientService
import com.kikitrade.kikikline.websocket.KiKiWebSocket
import com.kikitrade.kikikline.websocket.WSConfig
import com.kikitrade.klinelib.adapter.KLineChartAdapter
import com.kikitrade.klinelib.callback.SlidListener
import com.kikitrade.klinelib.formatter.DateFormatter
import com.kikitrade.klinelib.formatter.ValueFormatter
import com.kikitrade.klinelib.utils.DateUtil
import com.kikitrade.klinelib.utils.DpUtil
import com.kikitrade.klinelib.utils.LogUtil
import com.kikitrade.klinelib.utils.Status.*
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class ChartViewContainerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : LinearLayout(context, attrs), JWebSocketClientService.OnGetServiceKlineInterface {

    companion object {
        const val TAG: String = "ChartView_Container"

        const val MINUTE: String = "mm"

        //24小时进制
        const val HOUR: String = "HH"

    }

    private var requestJod: Job? = null
    private var countDownJod: Job? = null
    private var request = false
    private val moreTimeTypes by lazy {
        arrayOf(
            KikiTabBarType.OneMinute,
            KikiTabBarType.FiveMinutes,
            KikiTabBarType.ThirtyMinutes,
            KikiTabBarType.OneWeek,
            KikiTabBarType.OneMonth,
            KikiTabBarType.OneYear
        )
    }
    private val mainIndicators by lazy {
        arrayOf(
            KikiMainIndicator.MA,
            KikiMainIndicator.EMA,
            KikiMainIndicator.BOLL,
            KikiMainIndicator.TD
        )
    }

    private val otherIndicators by lazy {
        arrayOf(
            KikiOtherIndicator.MACD,
            KikiOtherIndicator.KDJ,
            KikiOtherIndicator.RSI,
            KikiOtherIndicator.WR
        )
    }

    var globalConfig: CryptoChartGlobalConfig? = null
    var binding: ViewContainerKkcryptochartBinding
    private lateinit var adapter: KLineChartAdapter<KChartNewBean>
    var service: JWebSocketClientService? = null
    private var onChartViewLoadingInterface: OnChartViewLoadingInterface? = null
    private var initState: Boolean = false

    private val serviceConnection by lazy {
        object : ServiceConnection {
            override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
                val binder = service as JWebSocketClientService.JWebSocketClientBinder?
                binder?.service.apply {
                    this@ChartViewContainerView.service = this
                    this?.setOnGetServiceKlineInterface(this@ChartViewContainerView)
                    this?.closeConnect()
                }
            }

            override fun onServiceDisconnected(name: ComponentName?) {
            }

        }
    }

    private val indicatorSelectorTexts: Array<String>
        get() {
            val locale = if (globalConfig != null && globalConfig?.getLocal() != null) {
                ChartManager.INSTANCE.getLocale(globalConfig?.getLocal())
            } else {
                ChartManager.INSTANCE.defaultLocale
            }
            return arrayOf(
                I18NUtil.getString(CHART_MAIN, locale, context),
                I18NUtil.getString(CHART_SUB, locale, context)
            )
        }

    init {
        val layoutInflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.view_container_kkcryptochart,
            this,
            true
        )
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        //  init chartView and set origin data
        initView()
        initService()
        Log.d(TAG, "--onAttachedToWindow-->${ChartManager.INSTANCE.config?.coinCode}")
    }


    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        // stop service and socket
        service?.closeConnect()
        context.unbindService(serviceConnection)
        countDownJod?.cancel()
        Log.d(TAG, " onDetachedFromWindow  执行了")
    }

    fun config(config: CryptoChartGlobalConfig?) {
        if (config == null) {
            return
        }
        if (config.environment?.isEmpty() == true || config.environment == null) {
            config.environment = "beta"
        }
        if (config.coinCode?.isEmpty() == true || config.coinCode == null) {
            config.coinCode = "BTC_USDT"
        }
        adapter = KLineChartAdapter<KChartNewBean>()
        val timeTypeString = StorageUtil.getTimeType(context)
        val timeType = if (timeTypeString == null) {
            KikiTabBarType.FifteenMinutes
        } else {
            ChartManager.INSTANCE.getTimeType(timeTypeString)
        }

        // 设置默认展示 ma 指标，用户手动设置就按设置的展示
        val localMainIndicator = StorageUtil.getLocalIndicatorMainType(context)
        val isClearMaStatus = StorageUtil.getClearMaStatus(context);
        if (localMainIndicator.isNullOrEmpty() && !isClearMaStatus) {
            ChartManager.INSTANCE.mainIndicator = KikiMainIndicator.MA
        } else {
            ChartManager.INSTANCE.mainIndicator =
                ChartManager.INSTANCE.getMainIndicator(localMainIndicator)
        }

        ChartManager.INSTANCE.otherIndicator =
            when (StorageUtil.getLocalIndicatorOthersType(context).isNullOrEmpty()) {
                true -> mutableListOf()
                else -> {
                    try {
                        Gson().fromJson(
                            StorageUtil.getLocalIndicatorOthersType(context),
                            object : TypeToken<List<KikiOtherIndicator?>?>() {}.type
                        )
                    } catch (exception: Exception) {
                        mutableListOf()
                    }
                }
            }
        globalConfig = config
        globalConfig?.timeType = timeType.getTime()
        ChartManager.INSTANCE.config = this.globalConfig
        ChartManager.INSTANCE.timeType = timeType
        binding.chartView.setAdapter(adapter)
        setupData()
        requestChartVieData()
    }

    // for RN project
    fun setSPFileName(name: String) {
        StorageUtil.setSharedPreferencesFileName(name)
    }

    private fun setupData() {
        val locale = if (globalConfig != null && globalConfig?.getLocal() != null) {
            ChartManager.INSTANCE.getLocale(globalConfig?.getLocal())
        } else {
            ChartManager.INSTANCE.defaultLocale
        }
        initSelectorData(locale)

        initTabBarData(locale)

        initSelectedDescription(locale)

    }

    private fun initSelectedDescription(locale: String) {

        val descriptions = arrayOf(
            I18NUtil.getString(CHART_DESCRIPTION_TIME, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_OPEN, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_CLOSE, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_HIGH, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_LOW, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_ONEVOL, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_RISE_AND_FALL, locale, context),
            I18NUtil.getString(CHART_DESCRIPTION_RISE_AND_FALL_RATE, locale, context)
        )
        binding.chartView.setSelectedInfoLabels(descriptions)
    }


    private fun initService() {
        val bindIntent = Intent(context, JWebSocketClientService::class.java)
        context.bindService(bindIntent, serviceConnection, AppCompatActivity.BIND_AUTO_CREATE)
    }

    private fun initView() {
        // origin config
        binding.apply {
            chartView.setTimeLineEndRadius(10f)
                .setLoadingView(ProgressBar(context))
                .setAnimLoadData(false)
                .setGridNumbers(3)
                .setMacdHollow(NONE_HOLLOW)
                .setPriceLabelInLineClickable(true)
                .setLabelSpace(DpUtil.Dp2Px(context, 55f).toFloat())
                .setLogoAlpha(100)
                .setKlineState(K_LINE_SHOW_CANDLE_LINE)
                .setYLabelState(LABEL_WITH_GRID)//set right can over range
                .setOverScrollRange(
                    (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.width.div(
                        4
                    ).toFloat()
                ) //show loading View
                .setCandleHollow(NONE_HOLLOW)
                .setBetterX(true) //set slid listener
                .setSlidListener(object : SlidListener {
                    override fun onSlidLeft() {
                        LogUtil.e("onSlidLeft")
                    }

                    override fun onSlidRight() {
                        LogUtil.e("onSlidRight")
                    }
                })
                .setVolumeFormatter(object : ValueFormatter() {

                    override fun format(value: Double): String {
                        return String.format(
                            Locale.CHINA,
                            "%,.0${globalConfig?.tradeVolumePrecision ?: 2}f",
                            value
                        )
                    }
                })
                //set main  formater
                .setMainValueFormatter(object : ValueFormatter() {

                    override fun format(value: Double): String {
                        return String.format(
                            Locale.CHINA,
                            "%,.0${globalConfig?.coinPrecision ?: 2}f",
                            value
                        )
                    }
                }) //set vol value  formater
                .setVolFormatter(object : ValueFormatter() {
                    override fun format(value: Double): String {
                        return String.format(
                            Locale.CHINA,
                            "%,.0${globalConfig?.tradeVolumePrecision ?: 2}f",
                            value
                        )
                    }
                }).dateTimeFormatter = object : DateFormatter() {
                override fun format(date: Date): String {
                    return ChartManager.INSTANCE.getTimeFormat(date, ChartManager.INSTANCE.timeType)
                }
            }

            chartView.scaleXMax = 3f
            chartView.scaleXMin = 0.1f
            setMainIndicatorType(ChartManager.INSTANCE.mainIndicator)
            setOtherIndicatorType(ChartManager.INSTANCE.otherIndicator)
        }


        binding.chartTabBar.setOnTabBarClickListener(object :
            KKCryptoChartTabBar.OnTabBarClickListener {

            override fun onSelectTime(kikiTabBarType: KikiTabBarType) {
                if (globalConfig == null) {
                    return
                }
                ChartManager.INSTANCE.apply {
                    globalConfig?.timeType = kikiTabBarType.getTime()
                    config = globalConfig
                    timeType = kikiTabBarType
                }
                binding.chartView.hideSelectData()
                requestChartVieData()
            }

            override fun setMainIndicator(main: KikiMainIndicator?) {
                setMainIndicatorType(main)
            }

            override fun setOtherIndicator(other: List<KikiOtherIndicator>?) {
                setOtherIndicatorType(other)
            }
        })
    }

    override fun requestLayout() {
        super.requestLayout()
        post(measureAndLayout)
    }

    private val measureAndLayout = Runnable {
        measure(
            MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
            MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
        )
        layout(left, top, right, bottom)
    }


    private fun updateKline(socketData: String) {
        try {
            adapter.apply {
                if (this.count <= 1) {
                    return
                }
                // more important
                if (resetShowPosition) resetShowPosition = false

                val data = Gson().fromJson(socketData, KChartNewBean::class.java)
//                val last = adapter.dataSource.get(adapter.count - 1)
//                Log.d(
//                    TAG, " 最后一条数据时间---->${dateToString(Date(last.closeTime))}  --当前时间-->${
//                        dateToString(
//                            Date(data.closeTime)
//                        )
//                    } ---SocketData---->$socketData  "
//                )
                val timeData = isAddOrUpdate(ChartManager.INSTANCE.timeType, data.closeTime)
                timeData?.let { time ->
                    if (data.timeFrame.isNullOrEmpty() || !data.timeFrame.equals(ChartManager.INSTANCE.timeType?.getResolution())) {
                        return
                    }
                    if (time.add) {
                        data.closeTime = time.time
                        addLast(data)
                        return
                    }
                    changeItem(
                        this.count - 1,
                        KChartNewBean(
                            time.time,
                            data.open,
                            data.close,
                            data.high,
                            data.low,
                            data.volume,
                            ""
                        )
                    )
                }


            }
        } catch (e: Exception) {
            Log.e(TAG, " 更新KLine捕获异常: ${e.message}")
        }
    }

    /**
     * 根据当前barType 和  时间戳 判断是更新数据还是增加柱子
     *
     * true-> addList();
     * false-> updateItem()
     */
    private fun isAddOrUpdate(barType: KikiTabBarType?, timeStamp: Long): TimeData? {
        return when (barType) {
            KikiTabBarType.OneMinute, KikiTabBarType.FiveMinutes,
            KikiTabBarType.FifteenMinutes, KikiTabBarType.ThirtyMinutes -> {
                getMinuteTimeData(barType, timeStamp, adapter.getDate(adapter.count - 1).time)
            }

            KikiTabBarType.OneHour, KikiTabBarType.FourHours -> {
                getHourTimeData(barType, timeStamp, adapter.getDate(adapter.count - 1).time)
            }

            KikiTabBarType.OneDay -> {
                getDayTimeData(timeStamp)
            }

            KikiTabBarType.OneWeek -> {
                getWeekTimeData(timeStamp)
            }
            KikiTabBarType.OneMonth -> {
                getMonthTimeData(timeStamp)
            }
            else -> {
                getYearTimeData(timeStamp)
            }
        }

    }

    // 设置附图指标
    private fun setOtherIndicatorType(others: List<KikiOtherIndicator>?) {
        binding.chartView.hideSelectData()
        val otherKeyList = mutableListOf<Int>()
        if (others == null || others.isEmpty()) {
            binding.chartView.setIndexDraw(otherKeyList)
            return
        }
        others.forEach {
            when (it) {
                KikiOtherIndicator.MACD -> {
                    otherKeyList.add(INDEX_MACD)
                }
                KikiOtherIndicator.KDJ -> {
                    otherKeyList.add(INDEX_KDJ)
                }
                KikiOtherIndicator.RSI -> {
                    otherKeyList.add(INDEX_RSI)
                }
                KikiOtherIndicator.WR -> {
                    otherKeyList.add(INDEX_WR)
                }
            }
        }
//        Log.d(TAG,"--设置附图指标--->${Gson().toJson(otherKeyList)}")
        binding.chartView.setIndexDraw(otherKeyList)
    }

    // 设置主图指标
    private fun setMainIndicatorType(main: KikiMainIndicator?) {

        binding.chartView.hideSelectData()

        when (main) {
            null -> {
                binding.chartView.changeMainDrawType(MAIN_NONE)
            }
            KikiMainIndicator.MA -> {
                binding.chartView.changeMainDrawType(MAIN_MA)
            }
            KikiMainIndicator.BOLL -> {
                binding.chartView.changeMainDrawType(MAIN_BOLL)
            }
            KikiMainIndicator.EMA -> {
                binding.chartView.changeMainDrawType(MAIN_EMA)
            }
            KikiMainIndicator.TD -> {
                binding.chartView.changeMainDrawType(MAIN_TD)
            }
        }
    }

    private fun requestChartVieData() {
        onChartViewLoadingInterface?.showLoading(
            true,
            ChartManager.INSTANCE.timeType?.getTime() ?: ""
        )
        if (globalConfig == null) {
            return
        }
        binding.chartView.setScale(1.0f)
        request = false
        // 取消上次网络请求，防止两次请求造成页面闪烁
        requestJod?.cancel()
        service?.closeConnect()
        if (context is LifecycleOwner) {
            requestJod = (context as LifecycleOwner).lifecycleScope.launch {
                val data = requestData()
                onChartViewLoadingInterface?.showLoading(
                    false,
                    ChartManager.INSTANCE.timeType?.getTime() ?: ""
                )
                if (data?.isNotEmpty() == true) {
                    return@launch
                }
                initState = true
                service?.reconnectWsAndReset()
            }
        }
    }


    private suspend fun requestData(): String? = withContext(Dispatchers.IO) {
        adapter.resetData(ArrayList<KChartNewBean>())
        suspendCoroutine { continuation ->
            ChartManager.INSTANCE
                .fetchCloudData(object :
                    KKNetworkCallback {
                    override fun onFinish(chartModels: KChatViewBaseBean) {
                        // run on Thread
                        chartModels.klines?.let {
                            adapter.resetData(it)
                        }
                        continuation.resume("")
                    }

                    override fun onError(e: Exception?) {
                        e?.printStackTrace()
                        continuation.resume(e?.message)
                    }
                })
        }
    }

    private fun initSelectorData(locale: String) {
        val moreSelectorTitles = arrayOf(
            I18NUtil.getString(CHART_TIME_ON_ONE_MINUTE, locale, context),
            I18NUtil.getString(CHART_TIME_ON_FIVE_MINUTE, locale, context),
            I18NUtil.getString(CHART_TIME_ON_THIRTY_MINUTE, locale, context),
            I18NUtil.getString(CHART_TIME_ON_ONE_WEEK, locale, context),
            I18NUtil.getString(CHART_TIME_ON_ONE_MONTH, locale, context),
            I18NUtil.getString(CHART_TIME_ON_ONE_YEAR, locale, context)
        )
        binding.chartTabBar.setMoreButtonOriginData(
            moreTimeTypes,
            moreSelectorTitles,
            ChartManager.INSTANCE.timeType
        )
        // set indicator data
        binding.chartTabBar.setIndicatorButtonOriginData(
            mainIndicators,
            otherIndicators,
            indicatorSelectorTexts
        )

    }

    private fun initTabBarData(locale: String) {

        val tabBarTypes = arrayOf(
            KikiTabBarType.FifteenMinutes,
            KikiTabBarType.OneHour,
            KikiTabBarType.FourHours,
            KikiTabBarType.OneDay,
            null, null
        )
        val triangle = I18NUtil.getString(CHART_TAB_BAR_TRIANGLE, locale, context)
        val titles = arrayOf(
            I18NUtil.getString(CHART_TIME_ON_FIFTEEN_MINUTE, locale, context),
            I18NUtil.getString(CHART_TIME_ON_ONE_HOUR, locale, context),
            I18NUtil.getString(CHART_TIME_ON_FOUR_HOUR, locale, context),
            I18NUtil.getString(CHART_TIME_ON_ONE_DAY, locale, context),
            I18NUtil.getString(CHART_TAB_BAR_MORE, locale, context) + triangle,
            I18NUtil.getString(CHART_TAB_BAR_INDICATOR, locale, context) + triangle
        )
        binding.chartTabBar.setOriginData(
            tabBarTypes,
            titles,
            globalConfig?.timeType,
            locale,
            triangle
        )
    }

    override fun getSocketKline(socketData: String) {
        updateKline(socketData)
    }

    // for RN project
    fun setChartViewInterface(listener: OnChartViewLoadingInterface) {
        this.onChartViewLoadingInterface = listener
    }

    fun setCustomTypeface(typeface: Typeface) {
        binding.apply {
            chartView.setCustomTypeface(typeface)
            chartTabBar.setCustomTypeface(typeface)
        }
    }

    //  感知当前 view 在前后台状态
    override fun onWindowFocusChanged(hasWindowFocus: Boolean) {
        super.onWindowFocusChanged(hasWindowFocus)
        Log.d(TAG, "hasWindowFocus-->$hasWindowFocus")
        when (hasWindowFocus) {
            true -> {
                // 可以优化为退到后台 大于 1min,重新请求数据
                countDownJod?.cancel()
                if (request) {
                    requestChartVieData()
                    return
                }
                // 回到前台，重新连接 socket
                if (adapter.count > 0) {
                    service?.reconnectWsAndReset()
                }
            }
            else -> {
                // 好多机型会在切到后台自动断掉socket,这里强制断掉
                // 退到后台开始计时器
                service?.closeConnect()
                countDownJod?.cancel()
                if (context is LifecycleOwner) {
                    countDownJod = (context as LifecycleOwner).lifecycleScope.launch {
                        delay(60 * 1000L)
                        request = true
                    }
                }
            }
        }
    }

    interface OnChartViewLoadingInterface {
        fun showLoading(loading: Boolean, resolution: String)
    }
}

@Keep
class TimeData(var add: Boolean = false, var time: Long)