package com.kikitrade.kikikline.view

import android.content.Context
import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import com.kikitrade.kikikline.R
import com.kikitrade.kikikline.config.ChartManager
import com.kikitrade.kikikline.config.KikiMainIndicator
import com.kikitrade.kikikline.config.KikiOtherIndicator
import com.kikitrade.kikikline.databinding.ViewIndicatorPopupBinding
import com.kikitrade.kikikline.utils.StorageUtil
import com.litesuits.common.assist.Check
import com.lxj.xpopup.XPopup
import com.lxj.xpopup.enums.PopupPosition
import com.lxj.xpopup.impl.PartShadowPopupView
import org.jetbrains.annotations.NotNull

class IndicatorPopupView @JvmOverloads constructor(private val builder: Builder) :
    PartShadowPopupView(builder.context),
    View.OnClickListener {


    lateinit var binding: ViewIndicatorPopupBinding
    var mainCheckedList: Array<CheckBox>? = null
    var otherCheckedList: Array<CheckBox>? = null

    override fun addInnerContent() {

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.view_indicator_popup, attachPopupContainer, false
        )
        binding.apply {
            onClick = this@IndicatorPopupView
            attachPopupContainer.addView(binding.root)

            mainCheckedList = arrayOf(
                indicatorMa,
                indicatorEma,
                indicatorBoll,
                indicatorTd
            )
            otherCheckedList = arrayOf(
                indicatorMacd,
                indicatorKdj,
                indicatorRsi,
                indicatorWr
            )

        }

    }

    override fun onCreate() {
        super.onCreate()
        builder.mainIndicators?.forEachIndexed { index, mainIndicator ->
            binding.apply {
                mainGroup.getChildAt(index)?.apply {
                    tag = mainIndicator
                    this as CheckBox
                    typeface = builder.mTypeface
                }
                // set selected status
                if (mainGroup.getChildAt(index)?.tag == builder.selectMainIndicator) {
                    mainGroup.getChildAt(index)?.apply {
                        if (this is CheckBox) {
                            isChecked = true
                            mainResetImg.isSelected = true
                        }
                    }
                }
            }
        }

        builder.otherIndicators?.forEachIndexed { index, otherIndicator ->

            binding.apply {
                otherGroup.getChildAt(index).apply {
                    this as CheckBox
                    tag = otherIndicator
                    typeface = builder.mTypeface
                }
                // set selected status
                builder.selectOtherIndicators?.forEach { selected ->
                    if (otherGroup.getChildAt(index)?.tag == selected) {
                        otherGroup.getChildAt(index)?.apply {
                            if (this is CheckBox) {
                                isChecked = true
                                otherResetImg.isSelected = true
                            }
                        }
                    }
                }
            }
        }

        builder.indicatorTitles?.apply {
            if (this.size < 2) {
                return
            }
            binding.mainTitle.text = this[0]
            binding.otherTitle.text = this[1]
        }
    }

    fun showDialog(view: View) {
        XPopup.Builder(builder.context)
            .atView(view)
            .popupPosition(PopupPosition.Bottom)
            .hasShadowBg(false)
            .isViewMode(true)
            .isClickThrough(true)
            .asCustom(this)
            .show()
    }

    class Builder(val context: Context) {

        var onIndicatorSelectedListener: OnIndicatorSelectedListener? = null


        var indicatorTitles: Array<String>? = null
            private set

        var selectMainIndicator: KikiMainIndicator? = null

        var selectOtherIndicators: MutableList<KikiOtherIndicator>? = null

        var multiSelect: Boolean = false

        var mainIndicators: Array<KikiMainIndicator>? = null
        var otherIndicators: Array<KikiOtherIndicator>? = null
        var mTypeface: Typeface? = null


        fun setClickListener(listener: OnIndicatorSelectedListener) = apply {
            this.onIndicatorSelectedListener = listener
        }

        fun setOriginData(
            mainIndicators: Array<KikiMainIndicator>?,
            otherIndicators: Array<KikiOtherIndicator>?,
            titles: Array<String>?
        ) = apply {
            this.mainIndicators = mainIndicators
            this.otherIndicators = otherIndicators
            this.indicatorTitles = titles
            this.selectMainIndicator = ChartManager.INSTANCE.mainIndicator
            this.selectOtherIndicators = ChartManager.INSTANCE.otherIndicator
        }

        fun setMultiSelect(multiSelect: Boolean) = apply {
            this.multiSelect = multiSelect
        }

        fun setCustomTypeface(typeface: Typeface) {
            this.mTypeface = typeface
        }

        fun build(): IndicatorPopupView = IndicatorPopupView(this)

    }


    /**
     * interface for IndicatorSetting
     */
    interface OnIndicatorSelectedListener {
        fun onGetMainIndicator(mainIndicator: KikiMainIndicator?)
        fun onGetOtherIndicator(otherIndicators: MutableList<KikiOtherIndicator>?)

        fun onIndicatorDismiss()
    }

    override fun onClick(view: View?) {
        binding.apply {
            when (view) {
                mainHideLayout -> {
                    reSetMainGroupCheckedStatus(null)
                    builder.selectMainIndicator = null
                    builder.onIndicatorSelectedListener?.onGetMainIndicator(null)
                    saveLocalMain()
                    dismiss()
                }
                otherHideLayout -> {
                    reSetOtherGroupCheckedStatus(null)
                    builder.onIndicatorSelectedListener?.onGetOtherIndicator(null)
                    selectedOtherIndicators(null)
                    dismiss()
                }

                indicatorMa, indicatorEma, indicatorTd, indicatorBoll -> {
                    view as CheckBox
                    builder.selectMainIndicator = if (view.isChecked) {
                        reSetMainGroupCheckedStatus(view)
                        view.tag as KikiMainIndicator?
                    } else {
                        null
                    }
                    builder.onIndicatorSelectedListener?.onGetMainIndicator(builder.selectMainIndicator)
                    mainResetImg.isSelected = true
                    saveLocalMain()
                }

                indicatorMacd, indicatorKdj, indicatorRsi, indicatorWr -> {
                    view as CheckBox
                    if (!builder.multiSelect){
                        reSetOtherGroupCheckedStatus(view)
                    }
                    selectedOtherIndicators(view)
                    otherResetImg.isSelected = true
                }
            }
        }
    }

    private fun saveLocalMain() {
        // 只有当主视图指标有修改，记录本地
        StorageUtil.saveClearMa(context, true)
        StorageUtil.saveMainIndicator(
            context,
            builder.selectMainIndicator?.getMainIndicatorDes() ?: ""
        )
        ChartManager.INSTANCE.mainIndicator = builder.selectMainIndicator
    }

    private fun selectedOtherIndicators(view: CheckBox?) {
        if (ChartManager.INSTANCE.otherIndicator.isNullOrEmpty()){
            ChartManager.INSTANCE.otherIndicator= mutableListOf()
        }
        if (view == null) {
            ChartManager.INSTANCE.otherIndicator?.clear()
        }else{
            val indicator = view.tag as KikiOtherIndicator
            if (view.isChecked && ChartManager.INSTANCE.otherIndicator?.contains(indicator) == false) {

                if (!builder.multiSelect){
                    ChartManager.INSTANCE.otherIndicator?.clear()
                }
                ChartManager.INSTANCE.otherIndicator?.add(indicator)
            } else {
                if (ChartManager.INSTANCE.otherIndicator?.contains(indicator) == true) {
                    ChartManager.INSTANCE.otherIndicator?.remove(indicator)
                }
            }
        }
        StorageUtil.saveOtherIndicators(
            context,
            Gson().toJson(ChartManager.INSTANCE.otherIndicator) ?: ""
        )
//        Log.d("Indicator", " 附图指标------>${Gson().toJson(ChartManager.INSTANCE.otherIndicator)}")
        builder.onIndicatorSelectedListener?.onGetOtherIndicator(ChartManager.INSTANCE.otherIndicator)
    }

    override fun onDismiss() {
        super.onDismiss()
        builder.onIndicatorSelectedListener?.onIndicatorDismiss()
    }


    private fun reSetMainGroupCheckedStatus(buttonView: CompoundButton?) {
        mainCheckedList?.filterNot {
            it == buttonView
        }?.forEach {
            it.isChecked = false
        }
    }

    private fun reSetOtherGroupCheckedStatus(buttonView: CompoundButton?) {
        otherCheckedList?.filterNot {
            it == buttonView
        }?.forEach {
            it.isChecked = false
        }
    }

}

