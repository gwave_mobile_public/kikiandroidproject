package com.kikitrade.kikikline.network;


import com.kikitrade.kikikline.config.KKCryptoChartConstants;
import com.kikitrade.kikikline.config.KKCryptoChartEnvironment;

public class KKCryptoChartBaseApi {
    private String baseUrl;

    private KKCryptoChartEnvironment environment;

    public KKCryptoChartEnvironment getEnvironment() {
        return environment;
    }

    public void setEnvironment(KKCryptoChartEnvironment environment) {
        this.environment = environment;
    }

    public String getBaseURL() {
        switch (environment) {
            case Beta:
                baseUrl = KKCryptoChartConstants.KK_BASE_BETA_URL;
                break;
            case Product:
            case ProdGreen:
                baseUrl = KKCryptoChartConstants.KK_BASE_PROD_URL;
                break;
        }
        return baseUrl;
    }

    /**
     * 获取 data kline
     *
     * @return
     */
    public String getTrendsBaseUrl() {
        switch (environment) {
            case Dev:
                baseUrl = KKCryptoChartConstants.KK_BASE_DATA_KLINE_DEV_URL;
                break;
            case Product:
            case ProdGreen:
                baseUrl = KKCryptoChartConstants.KK_BASE_DATA_KLINE_PROD_URL;
                break;
            case Beta:
                baseUrl = KKCryptoChartConstants.KK_BASE_DATA_KLINE_BETA_URL;
                break;
        }
        return baseUrl;
    }

    public String getCustomURL() {
        return "";
    }

    public String getCompletedURL() {
        return "";
    }
}
