package com.kikitrade.kikikline.network;


import com.kikitrade.kikikline.model.KChartNewBean;
import com.kikitrade.kikikline.model.KChatViewBaseBean;

import java.util.ArrayList;

public interface KKNetworkCallback {
    /**
     * 成功回调
     *
     * @param
     */
    void onFinish(KChatViewBaseBean baseBean);

    /**
     * 失败回调
     *
     * @param e 异常
     */
    void onError(Exception e);
}
