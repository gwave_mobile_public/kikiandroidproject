package com.kikitrade.kikikline.network;

import android.util.Log;

public class KKCryptoChartInitApi extends KKCryptoChartBaseApi {

    private static final String TAG = "KKCryptoChartInitApi";
    // 盘口名
    private String symbol;

    // 时间维度
    private String resolution;

    // 起始时间戳
    private String from;

    // 结束时间戳
    private String to;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String getBaseURL() {
        return super.getBaseURL();
    }

    @Override
    public String getCompletedURL() {
        StringBuilder sb = new StringBuilder(getBaseURL());
        sb.append(getCustomURL());
        if (!symbol.isEmpty()) {
            sb.append("?symbol=").append(symbol);
        }
        if (!resolution.isEmpty()) {
            sb.append("&resolution=").append(resolution);
        }
        if (!from.isEmpty()) {
            sb.append("&from=").append(from);
        }
        if (!to.isEmpty()) {
            sb.append("&to=").append(to);
        }
        return sb.toString();
    }


    public String getTrendsCompletedUrl(){
        StringBuilder sb = new StringBuilder(getTrendsBaseUrl());
        if (!symbol.isEmpty()) {
            sb.append(symbol);
        }
        if (!resolution.isEmpty()) {
            sb.append("?resolution=").append(resolution);
        }
        if (!from.isEmpty()) {
            sb.append("&from=").append(from);
        }
        if (!to.isEmpty()) {
            sb.append("&to=").append(to);
        }
        return sb.toString();
    }

}
