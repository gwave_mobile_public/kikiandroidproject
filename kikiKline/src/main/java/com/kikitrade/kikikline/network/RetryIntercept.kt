package com.kikitrade.kikikline.network

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import kotlin.Throws
import kotlin.math.log

/**
 * @Author: peter.Qin@kikitrade
 * @CreateDate: 2022/2/8 10:09 上午
 * @descriptiopn: 重试机制
 */
class RetryIntercept(  //最大重试次数
    var maxRetry: Int
) : Interceptor {
    private var retryNum = 0 //假如设置为3次重试的话，则最大可能请求4次（默认1次+3次重试）

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
//        Log.d("RetryIntercept", "retryNum=$retryNum")
        var response: Response = chain.proceed(request)
        while (!response.isSuccessful && retryNum < maxRetry) {
            retryNum++
            response.close()
            response = chain.proceed(request)
        }
        return response
    }
}