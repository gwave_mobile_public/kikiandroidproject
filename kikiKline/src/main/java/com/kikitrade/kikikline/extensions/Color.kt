package com.kikitrade.kikikline.extensions

import android.graphics.Color
import android.os.Build
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi

@RequiresApi(Build.VERSION_CODES.O)
@ColorInt
fun Int.setColorTransparency(alpha: Float): Int {
  val targetColor = Color.valueOf(this)
  return Color.argb(alpha, targetColor.red(), targetColor.green(), targetColor.blue())
}

private fun lerp(interpolation: Float, val1: Float, val2: Float): Float {
  return val1 * (1 - interpolation) + val2 * interpolation
}

/**
 * 颜色变换
 * @param this 启始颜色
 * @param interpolation 颜色进度 0-1
 * @param targetColor 目标颜色
 */
@ColorInt
public fun Int.lerpTo(interpolation: Float, targetColor: Int): Int {
  val r = lerp(
    interpolation,
    (this shr 16 and 0xff).toFloat(),
    (targetColor shr 16 and 0xff).toFloat()
  ).toInt()
  val g =
    lerp(
      interpolation,
      (this shr 8 and 0xff).toFloat(),
      (targetColor shr 8 and 0xff).toFloat()
    ).toInt()
  val b = lerp(interpolation, (this and 0xff).toFloat(), (targetColor and 0xff).toFloat()).toInt()
  return Color.rgb(r, g, b)
}
