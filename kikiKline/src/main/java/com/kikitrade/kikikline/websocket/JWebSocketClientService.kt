package com.kikitrade.kikikline.websocket

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.kikitrade.kikikline.config.ChartManager
import com.kikitrade.kikikline.view.ChartViewContainerView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.java_websocket.WebSocket
import org.java_websocket.enums.ReadyState
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.timerTask

class JWebSocketClientService : Service(),
    KiKiWebSocket.OnReceiveKlineDataInterface {
    var client: KiKiWebSocket? = null
    private val mBinder: JWebSocketClientBinder = JWebSocketClientBinder()
    var reconnectTime: Int = 0
    var timer: Timer? = null
    private var onGetServiceKlineInterface: OnGetServiceKlineInterface? = null
    private var job: Job? = null

    companion object {
        const val TAG = ChartViewContainerView.TAG

        //每隔10秒进行一次对长连接的心跳检测
        const val HEART_BEAT_RATE = (15 * 1000).toLong()

        const val SOCKET_TIME_OUT = (5 * 1000).toLong()
    }


    //用于Activity和service通讯
    inner class JWebSocketClientBinder : Binder() {
        val service: JWebSocketClientService
            get() = this@JWebSocketClientService
    }


    /**
     * 开启重连
     */
    private fun reconnectWs() {
        if (job != null) {
            job?.cancel()
            job = null
        }
        job = GlobalScope.launch {

            if (client?.readyState == ReadyState.NOT_YET_CONNECTED) {
                try {
                    val result = client?.connectBlocking()
                    if (result == true) {
                        WSConfig.configRoom(RoomType.KLINE_ROOM)?.let {
//                        Log.d(TAG, "--sendRoom--->${Gson().toJson(it)}")
                            if (client?.isOpen == true)
                                client?.doSend(it)
                        }
                    }
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                }
            } else if (client?.readyState == ReadyState.CLOSING || client?.readyState == ReadyState.CLOSED) {
                try {
                    val result = client?.reconnectBlocking()
                    if (result == true) {
                        WSConfig.configRoom(RoomType.KLINE_ROOM)?.let {
//                        Log.d(TAG, "--sendRoom--->${Gson().toJson(it)}")
                            if (client?.isOpen == true)
                                client?.doSend(it)
                        }
                    }
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                }
            }
        }


    }

    override fun onBind(intent: Intent): IBinder {
        return mBinder
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    /**
     * 初始化websocket连接
     */
    private fun initSocketClient() {
        client = KiKiWebSocket(WSConfig())
        client?.setListener(this@JWebSocketClientService)
        client?.connectionLostTimeout = SOCKET_TIME_OUT.toInt()
        if (job != null) {
            job?.cancel()
            job = null
        }
        job = GlobalScope.launch {
            try {
                val result = client?.connectBlocking(SOCKET_TIME_OUT, TimeUnit.MILLISECONDS)
                if (result == true) {
                    WSConfig.configRoom(RoomType.KLINE_ROOM)?.let {
                        if (client?.isOpen == true)
                            client?.doSend(it)
                    }
                }
                sendBeatData()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * 断开连接
     */
    fun closeConnect() {
        if (job != null) {
            job?.cancel()
            job = null
        }
        job = GlobalScope.launch {
            try {
                client?.closeBlocking()
                timer?.cancel()
                timer = null
                client = null
            } catch (e: Exception) {
                e.printStackTrace()
//                Log.e(TAG, " closeConnect--->${e.message}")
            }
        }
    }

    /**
     *  发送心跳包
     */
    private fun sendBeatData() {
        if (timer != null) {
            timer?.cancel()
            timer = null
        }
        timer = Timer()
        timer?.schedule(timerTask {
            reconnectTime++
            if (client != null) {
                if (client?.isClosed == true && reconnectTime < 10) {
                    reconnectWs()
                }
            } else {
                //如果client已为空，重新初始化websocket
                initSocketClient()
            }
            //定时对长连接进行心跳检测
            if (client?.isOpen == true) {
                val beatMsg = ChartManager.INSTANCE.timeType?.getResolution()
//                Log.d(TAG, "sendBeatData-->$beatMsg")
                client?.send(beatMsg)
            }
        }, HEART_BEAT_RATE, HEART_BEAT_RATE)
    }


    /**
     * 开启重连
     */
    fun reconnectWsAndReset() {
        reconnectTime = 0
        timer?.cancel()
        timer = null
        if (client == null) {
            initSocketClient()
            return
        }
        reconnectWs()
    }


    override fun onReceiveKline(socketData: String) {
        onGetServiceKlineInterface?.getSocketKline(socketData)
    }


    fun setOnGetServiceKlineInterface(listener: OnGetServiceKlineInterface) {
        this.onGetServiceKlineInterface = listener
    }

    interface OnGetServiceKlineInterface {
        fun getSocketKline(socketData: String)
    }

    override fun onDestroy() {
        super.onDestroy()
        job?.cancel()
        timer?.cancel()
        Log.i(TAG, "------onDestroy--->")
    }
}
