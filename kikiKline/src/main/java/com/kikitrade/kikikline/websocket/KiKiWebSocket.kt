package com.kikitrade.kikikline.websocket

import android.util.Log
import androidx.annotation.Keep
import androidx.annotation.StringDef
import com.google.gson.Gson
import com.kikitrade.kikikline.config.KKCryptoChartConstants
import com.kikitrade.kikikline.config.ChartManager
import com.kikitrade.kikikline.config.KKCryptoChartEnvironment
import com.kikitrade.kikikline.websocket.RoomType.Companion.KLINE_ROOM
import com.kikitrade.kikikline.websocket.RoomType.Companion.ORDER_BOOK
import com.kikitrade.kikikline.websocket.WSConfig.EnvironmentType.Companion.MainNet
import com.kikitrade.kikikline.websocket.WSConfig.EnvironmentType.Companion.TestNet
import com.kikitrade.kikikline.utils.Utils
import com.kikitrade.kikikline.view.ChartViewContainerView
import com.kikitrade.klinelib.utils.LogUtil
import com.litesuits.common.utils.RandomUtil
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import org.jetbrains.annotations.NotNull
import org.json.JSONObject
import java.lang.StringBuilder
import java.net.URI
import kotlin.Exception

/**
 * @Author:         xiaopeng@kikitrade
 * @CreateDate:     2021/12/15 5:49 下午
 */
class KiKiWebSocket(val config: WSConfig) : WebSocketClient(config.getUri()) {

    private var onReceiveKlineDataInterface: OnReceiveKlineDataInterface? = null

    companion object {

        const val TAG =ChartViewContainerView.TAG

        const val PATH = "/api/room"

        const val METHOD = "POST"
    }


    override fun onOpen(handshakedata: ServerHandshake?) {
        // register first and send
        Log.d(TAG, "Socket onOpen")
        send("RG#${RandomUtil.getRandom(Int.MAX_VALUE)}")
    }

    override fun onMessage(message: String?) {
        if (message.isNullOrEmpty()) {
            return
        }
//        Log.i(TAG, "onMessage---->${message}")

        if (message.startsWith("NF#")) {
            val originData = message.substring(3, message.length)
            val jsonObject = JSONObject(originData)
            if (jsonObject.getString("data").isNotBlank() && jsonObject.getString("type") == KLINE_ROOM){
                onReceiveKlineDataInterface?.onReceiveKline(jsonObject.getString("data"))
            }
//            Log.e(TAG, "Receive Kline onMessage--->${jsonObject.getString("data")}")
        }
    }

    override fun onClose(code: Int, reason: String?, remote: Boolean) {
        Log.e(TAG, "----onClose---> reason-->$reason  remote--->$remote")
    }

    override fun onError(ex: Exception?) {
        Log.e(TAG, "----onError--->  message-->${ex?.message}")
    }


    fun doSend(room: Room) {
        val data = formDataString(room)

        try {
            if (config.authType == "appCode" || config.authType == "none") {
                val msg = createMsg(data, config)
//                Log.d("doSend", " send message --->${Gson().toJson(msg)}")
                send(Gson().toJson(msg))
            } else {
                Throwable("Parameter type is wrong")
            }
        } catch (exception: Exception) {
            Log.e(TAG,"  KiKiWebSocket  doSend--->${exception.message}")
            exception.printStackTrace()
        }
    }


    private fun createMsg(body: String, config: WSConfig): CreateMsg {
        val map = mutableMapOf<String, String>()
        map["content-type"] = config.content_type
        map["accept"] = config.accept_type
        map["x-ca-deviceid"] = ChartManager.INSTANCE.deviceId.toString()
        map["Authorization"] = "APPCODE ${config.appCode}"
        val md5 = Utils.getMd5Base64(body)
        map["content-md5"] = md5

//        Log.d(TAG, " deviceId --->${ChartManager.INSTANCE.deviceId}")
        return CreateMsg(body, map, config.getHost(), 0, METHOD, PATH)
    }


    /**
     * return  for example:"room=kline&symbol=btc&fromSymbol=eth&resolution=P1m"
     */
    private fun formDataString(room: Room): String {
        val testString = "{\n" +
                "\"room\":\"kline\",\n" +
                "\"symbol\":\"btc\",\n" +
                "\"fromSymbol\":\"eth\",\n" +
                "\"resolution\":\"P1m\"\n" +
                "}"
        val jsonObject = JSONObject(Gson().toJson(room))
        val builder = StringBuilder()

        val map = mutableMapOf<String, String>()

        for (keys in jsonObject.keys().withIndex()) {
            val keyValue = keys.value
            map[keyValue] = jsonObject.get(keyValue) as String
        }
        val filterNotNullMap = map.filter {
            it.value.isNotEmpty()
        }
        filterNotNullMap.keys.forEachIndexed { index, key ->
            builder.append("$key=${filterNotNullMap.get(key)}")
            if (index != filterNotNullMap.keys.size - 1) {
                builder.append("&")
            }
        }
        LogUtil.e("$TAG formDataString---->$builder")
        return builder.toString()
    }

    fun setListener(listener: OnReceiveKlineDataInterface) {
        this.onReceiveKlineDataInterface = listener
    }

    interface OnReceiveKlineDataInterface {
        fun onReceiveKline(socketData: String)
    }


}

@Keep
class Room(
    @NotNull var room: String,
    @NotNull var symbol: String,
    var fromSymbol: String?,
    var resolution: String = "P1m"
)

@StringDef(KLINE_ROOM, ORDER_BOOK)
annotation class RoomType {
    companion object {
        const val KLINE_ROOM = "kline"
        const val ORDER_BOOK = "order_book"
    }
}


@Keep
class WSConfig(
    var registerPath: String = "/api/register",
    var unregisterPath: String = "/api/unregister",
    var authType: String = "appCode",
    var appCode: String? = "c022929d0e1f4b0d8eace9cb9934bf0d",
    var content_type: String = "application/x-www-form-urlencoded; charset=utf-8",
    var accept_type: String = "application/json; charset=utf-8"
) {


    fun getUri(): URI {
        return URI.create(getWSUrl())
    }

    fun getHost(): String {
        return "${URI.create(getWSUrl()).host}:${URI.create(getWSUrl()).port}"
    }


    @StringDef(MainNet, TestNet)
    annotation class EnvironmentType {
        companion object {
            const val MainNet: String = KKCryptoChartConstants.WB_PROD_URL
            const val TestNet: String = KKCryptoChartConstants.WB_BETA_URL
        }
    }

    fun getWSUrl(): String {
        return when (ChartManager.INSTANCE.config?.getEnv() == KKCryptoChartEnvironment.Beta) {
            true -> {
                KKCryptoChartConstants.WB_BETA_URL
            }
            else -> {
                KKCryptoChartConstants.WB_PROD_URL
            }
        }
    }


    companion object {
        fun configRoom(@NotNull @RoomType roomType: String): Room? {
            return when (roomType) {
                KLINE_ROOM -> {
                    Room(
                        "kline",
                        ChartManager.INSTANCE.config?.coinCode ?: "",
                        ChartManager.INSTANCE.config?.fromSymbol,
                        ChartManager.INSTANCE.timeType?.getResolution() ?: ""
                    )
                }
                else -> {
                    null
                }
            }
        }
    }


}


@Keep
class CreateMsg(
    var body: String,
    var headers: Map<String, String>,
    var host: String,
    var isBase64: Int = 0,
    var method: String,
    var path: String
)
