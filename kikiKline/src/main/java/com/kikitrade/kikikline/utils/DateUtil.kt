package com.kikitrade.kikikline.utils

import android.annotation.SuppressLint
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * @Author:         xiaopeng@kikitrade
 * @CreateDate:     2021/12/24 11:45 上午
 */
class DateUtil {

    companion object {


        @JvmStatic
        fun getCurrentTime(time: Long, format: String): Int {
            val date = Date(time)
            val formatTime = SimpleDateFormat(format).format(date)
            return formatTime.toInt()
        }

        @SuppressLint("SimpleDateFormat")
        @JvmStatic
        fun timeStringLong(time: String): Long {
            return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(time).time
        }

        @SuppressLint("SimpleDateFormat")
        @JvmStatic
        fun getTimeString(time: Long): String {
            val date = Date(time)
            return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date).toString()
        }

        @SuppressLint("SimpleDateFormat")
        @JvmStatic
        fun dayForWeek(pTime: String): Int {
            if (pTime.isNullOrEmpty() && pTime.length < 13) {
                return 7
            }
            return try {
                val c = Calendar.getInstance()

                c.time = Date(pTime.toLong())
                return if (c[Calendar.DAY_OF_WEEK] == 1) {
                    7
                } else {
                    c[Calendar.DAY_OF_WEEK] - 1
                }
            } catch (exception: Exception) {
                1
            }
        }

        /**
         * 获取当前时间当前所在周的开始时间 或者结束时间
         *
         * start:true 获取开始时间; false:获取结束时间
         */
        @JvmStatic
        fun getThisWeekStartOrEnd(start: Boolean): String {

            val cal = Calendar.getInstance()
            //开始
            cal.set(
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONDAY),
                cal.get(Calendar.DAY_OF_MONTH),
                0,
                0,
                0
            )
            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
            if (start) {
                return cal.toString()
            }
            //结束
            cal.add(Calendar.DAY_OF_WEEK, 6)
            return cal.toString()

        }


        @JvmStatic
        fun dateToString(date: Date?): String {
            return SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date)
        }

        /**
         *
         * 根据当前日期获得所在周的日期区间（周一和周日日期）
         * 任意一时间的当前周的时间段
         */
        @JvmStatic
        fun getTimeInterval(date: Date?): String {
            val cal = Calendar.getInstance()
            cal.time = date
            // 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
            val dayWeek = cal[Calendar.DAY_OF_WEEK] // 获得当前日期是一个星期的第几天
            if (1 == dayWeek) {
                cal.add(Calendar.DAY_OF_MONTH, -1)
            }
            // 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
            cal.firstDayOfWeek = Calendar.MONDAY
            // 获得当前日期是一个星期的第几天
            val day = cal[Calendar.DAY_OF_WEEK]
            // 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
            cal.add(Calendar.DATE, cal.firstDayOfWeek - day)
            val sdf = SimpleDateFormat("yyyyMMdd")
            val imptimeBegin = sdf.format(cal.time)
            // System.out.println("所在周星期一的日期：" + imptimeBegin);
            cal.add(Calendar.DATE, 6)
            val imptimeEnd = sdf.format(cal.time)
            // System.out.println("所在周星期日的日期：" + imptimeEnd);
            return "$imptimeBegin,$imptimeEnd"
        }

        /**
         * 获取今天开始时间
         */
        @JvmStatic
        fun getTodayStartTime(time: String = ""): Date {
            val todayStart = Calendar.getInstance()
            //注：这里不能用Calendar.HOUR，因为他是12小时制，而Calendar.HOUR_OF_DAY才是24小时制
            if (time.isNotEmpty() && time.length == 13) {
                todayStart.timeInMillis = time.toLong()
            }
            todayStart[Calendar.HOUR_OF_DAY] = 0
            todayStart[Calendar.MINUTE] = 0
            todayStart[Calendar.SECOND] = 0
            todayStart[Calendar.MILLISECOND] = 0
            return todayStart.time
        }

        /**
         * 获取今天结束时间
         */
        @JvmStatic
        fun getTodayEndTime(): Date {
            val todayEnd = Calendar.getInstance()
            todayEnd[Calendar.HOUR_OF_DAY] = 23
            todayEnd[Calendar.MINUTE] = 59
            todayEnd[Calendar.SECOND] = 59
            todayEnd[Calendar.MILLISECOND] = 999
            return todayEnd.time
        }

        // 获得本周一0点时间
        @JvmStatic
        fun getCurrentWeekStartTimes(time: String = ""): Date {
            val cal = Calendar.getInstance()
            if (time.isNotEmpty() && time.length == 13) {
                cal.time = Date(time.toLong())
            }
            cal[cal[Calendar.YEAR], cal[Calendar.MONDAY], cal[Calendar.DAY_OF_MONTH], 0, 0] = 0
            cal[Calendar.DAY_OF_WEEK] = Calendar.MONDAY
            return cal.time
        }

        // 获得本周日24点时间
        @JvmStatic
        fun getCurrentWeekEndTimes(): Date {
            val cal = Calendar.getInstance()
            cal.time = getCurrentWeekStartTimes()
            cal.add(Calendar.DAY_OF_WEEK, 6)
            cal[Calendar.HOUR_OF_DAY] = 23
            cal[Calendar.MINUTE] = 59
            cal[Calendar.SECOND] = 59
            cal[Calendar.MILLISECOND] = 999
            return cal.time
        }

        /**
         * 获取当月开始时间戳
         *
         * @return
         */
        @JvmStatic
        fun getCurrentMonthStartTimes(time: String = ""): Date {
            val calendar = Calendar.getInstance() // 获取当前日期
            if (time.isNotEmpty() && time.length == 13) {
                calendar.timeInMillis = time.toLong()
            }
            calendar.timeInMillis = Date().time
            calendar.add(Calendar.YEAR, 0)
            calendar.add(Calendar.MONTH, 0)
            calendar.add(Calendar.DATE, 0)
            calendar[Calendar.DAY_OF_MONTH] = 1 // 设置为1号,当前日期既为本月第一天
            calendar[Calendar.HOUR_OF_DAY] = 0
            calendar[Calendar.MINUTE] = 0
            calendar[Calendar.SECOND] = 0
            calendar[Calendar.MILLISECOND] = 0
            return calendar.time
        }

        /**
         * 获取当月的结束时间戳
         *
         * @return
         */
        @JvmStatic
        fun getCurrentMonthEndTimes(): Date {
            val calendar = Calendar.getInstance() // 获取当前日期
            calendar.timeInMillis = Date().time
            calendar.add(Calendar.YEAR, 0)
            calendar.add(Calendar.MONTH, 0)
            calendar[Calendar.DAY_OF_MONTH] =
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH) // 获取当前月最后一天
            calendar[Calendar.HOUR_OF_DAY] = 23
            calendar[Calendar.MINUTE] = 59
            calendar[Calendar.SECOND] = 59
            calendar[Calendar.MILLISECOND] = 999
            return calendar.time
        }

        /**
         * 季度
         */
        @SuppressLint("SimpleDateFormat")
        @JvmStatic
        fun getCurrentQuarterStartTime(): Date? {
            val c = Calendar.getInstance()
            val currentMonth = c[Calendar.MONTH] + 1
            val longSdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val shortSdf = SimpleDateFormat("yyyy-MM-dd")
            var now: Date? = null
            try {
                when (currentMonth) {
                    in 1..3 -> c[Calendar.MONTH] =
                        0
                    in 4..6 -> c[Calendar.MONTH] =
                        3
                    in 7..9 -> c[Calendar.MONTH] =
                        6
                    in 10..12 -> c[Calendar.MONTH] =
                        9
                }
                c[Calendar.DATE] = 1
                now = longSdf.parse(shortSdf.format(c.time).toString() + " 00:00:00")
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            return now
        }

        /**
         * 当前季度的结束时间，即2012-03-31 23:59:59
         */
        @JvmStatic
        fun getCurrentQuarterEndTime(): Date {
            val cal = Calendar.getInstance()
            cal.time = getCurrentQuarterStartTime()
            cal.add(Calendar.MONTH, 2)
            cal[Calendar.DAY_OF_MONTH] = cal.getActualMaximum(Calendar.DAY_OF_MONTH) // 获取当前月最后一天
            cal[Calendar.HOUR_OF_DAY] = 23
            cal[Calendar.MINUTE] = 59
            cal[Calendar.SECOND] = 59
            cal[Calendar.MILLISECOND] = 999
            return cal.time
        }

        /**
         * 获取当年的开始时间戳
         *
         * @return
         */
        @JvmStatic
        fun getCurrentYearStartTime(time: String = ""): Date {
            val calendar = Calendar.getInstance() // 获取当前日期
            if (time.isNotEmpty() && time.length == 13) {
                calendar.time = Date(time.toLong())
            } else {
                calendar.timeInMillis = Date().time
            }
            calendar.add(Calendar.YEAR, 0)
            calendar.add(Calendar.MONTH, 0)
            calendar[Calendar.DAY_OF_YEAR] = 1
            calendar[Calendar.HOUR_OF_DAY] = 0
            calendar[Calendar.MINUTE] = 0
            calendar[Calendar.SECOND] = 0
            calendar[Calendar.MILLISECOND] = 0
            return calendar.time
        }

        /**
         * 获取当年的最后时间戳
         *
         * @return
         */
        @JvmStatic
        fun getCurrentYearEndTime(): Date? {
            val calendar = Calendar.getInstance() // 获取当前日期
            calendar.timeInMillis = Date().time
            val year = calendar[Calendar.YEAR]
            calendar.clear()
            calendar[Calendar.YEAR] = year
            calendar[Calendar.HOUR_OF_DAY] = 23
            calendar[Calendar.MINUTE] = 59
            calendar[Calendar.SECOND] = 59
            calendar[Calendar.MILLISECOND] = 999
            calendar.roll(Calendar.DAY_OF_YEAR, -1)
            return calendar.time
        }
    }


}