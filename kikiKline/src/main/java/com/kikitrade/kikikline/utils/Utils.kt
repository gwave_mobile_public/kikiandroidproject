package com.kikitrade.kikikline.utils

import android.util.Base64
import com.litesuits.common.utils.MD5Util
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * @Author:         xiaopeng@kikitrade
 * @CreateDate:     2021/12/15 5:38 下午
 */
class Utils {

    companion object {

        @JvmStatic
        fun getMd5Base64(string: String?): String {
            if (string.isNullOrEmpty()) {
                return ""
            }
            return Base64.encodeToString(MD5Util.md5(string), Base64.DEFAULT)
        }

        @JvmStatic
        fun formatNum(num: BigDecimal, scale: Int = 2): String {
            if (num < BigDecimal("1000")) {
                return num.setScale(scale, RoundingMode.DOWN).toPlainString()
            }
            //1000≤数值<1,000,000，千以K代替
            if (num.compareTo(BigDecimal("1000")) > -1 && num < BigDecimal("1000000")) {
                return num.divide(BigDecimal("1000"), scale, RoundingMode.DOWN).toPlainString() + "K"
            }
            //1,000,000≤数值< 1,000,000,000 百万以M代替
            if (num.compareTo(BigDecimal("1000000")) > -1 && num < BigDecimal("1000000000")) {
                return num.divide(BigDecimal("1000000"), scale, RoundingMode.DOWN).toPlainString() + "M"
            }
            //1,000,000,000≤数值    超过十亿以B代替，
            return if (num.compareTo(BigDecimal("1000000000")) > -1) {
                num.divide(BigDecimal("1000000000"), scale, RoundingMode.DOWN).toPlainString() + "B"
            } else num.toPlainString()
        }
    }
}