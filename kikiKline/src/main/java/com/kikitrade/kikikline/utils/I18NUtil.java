package com.kikitrade.kikikline.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.kikitrade.kikikline.config.CryptoChartGlobalConfig;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class I18NUtil {
    private static final String PROPERTIES_FILE_NAME = "kkcryptochartstrings";

    public static String getString(String key, String locale, Context context) {
        try {
            Properties properties = new Properties();;
            AssetManager assetManager = context.getAssets();
            InputStream inputStream = assetManager.open(getFileName(locale));
            properties.load(inputStream);
            return properties.getProperty(key);
        } catch (IOException e) {
            Log.e("I18NUtil", "getString exception: " + e.getMessage());
        }
        return "";
    }

    private static String getFileName(String locale) {
        if (locale.isEmpty()) {
            return PROPERTIES_FILE_NAME + ".properties";
        } else {
            return PROPERTIES_FILE_NAME + "_" + locale + ".properties";
        }
    }
}
