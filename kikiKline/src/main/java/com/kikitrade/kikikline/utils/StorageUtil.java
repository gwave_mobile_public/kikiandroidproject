package com.kikitrade.kikikline.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class StorageUtil {
    private static final String LOCAL_DATA_TIME_TYPE = "time_type";
    private static final String LOCAL_INDICATOR_MAIN_TYPE = "main_indicator";
    private static final String LOCAL_INDICATOR_OTHER_TYPE = "other_indicator";
    private static final String LOCAL_INDICATOR_MAIN_CLEAR_MA="main_indicator_clear_ma";
    // 附图多选指标的本地存储key
    private static final String LOCAL_INDICATOR_OTHERS_TYPE="other_indicators";


    private static String SP_NAME;

    public static void setSharedPreferencesFileName(String sharedPreferencesFileName) {
        SP_NAME = sharedPreferencesFileName;
    }


    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
    }

    public static void saveTimeType(Context context, String value) {
        saveLocalStorageString(context, LOCAL_DATA_TIME_TYPE, value);
    }

    public static String getTimeType(Context context) {
        return getLocalStorageString(context, LOCAL_DATA_TIME_TYPE);
    }

    public static void saveMainIndicator(Context context,String value){
        saveLocalStorageString(context,LOCAL_INDICATOR_MAIN_TYPE,value);
    }

    public static void saveOtherIndicator (Context context,String value) {
        saveLocalStorageString(context,LOCAL_INDICATOR_OTHER_TYPE,value);
    }

    // 本地存储附图多选指标
    public static void saveOtherIndicators (Context context,String values){
        saveLocalStorageString(context,LOCAL_INDICATOR_OTHERS_TYPE,values);
    }

    public static String getLocalIndicatorMainType (Context context){
        return getLocalStorageString(context,LOCAL_INDICATOR_MAIN_TYPE);
    }

    public static String getLocalIndicatorOthersType(Context context){
        return getLocalStorageString(context,LOCAL_INDICATOR_OTHERS_TYPE);
    }

    public static String getLocalIndicatorOtherType (Context context) {
        return getLocalStorageString(context,LOCAL_INDICATOR_OTHER_TYPE);
    }
    public static void saveLocalStorageString(Context context, String key, String value) {
        Editor editor = getSharedPreferences(context).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void saveClearMa(Context context, boolean isMAClear){
        getSharedPreferences(context).edit().putBoolean(LOCAL_INDICATOR_MAIN_CLEAR_MA,isMAClear).apply();
    }

    public static boolean getClearMaStatus(Context context){
        return getSharedPreferences(context).getBoolean(LOCAL_INDICATOR_MAIN_CLEAR_MA,false);
    }

    public static String getLocalStorageString(Context context, String key) {
        return getSharedPreferences(context).getString(key, null);
    }

}
