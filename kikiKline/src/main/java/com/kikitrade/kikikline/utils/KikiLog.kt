package com.kikitrade.kikikline.utils

import android.text.TextUtils
import android.util.Log

/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/7/23 at 2:34 下午
 * @Description:
 */
object KikiLog {
    private const val NOTHING = 6
    private const val VERBOSE = 1
    private const val DEBUG = 2;
    private const val INFO = 3
    private const val WARN = 4
    private const val ERROR = 5
    private const val SEPARATOR = ", "

    private const val LEVEL = VERBOSE

    fun v(message: String) {
        if (LEVEL <= VERBOSE) {
            val stackTraceElement = Thread.currentThread().stackTrace[4]
            val tag = getDefaultTag(stackTraceElement)
            Log.v(tag, getLogInfo(stackTraceElement) + message)
        }
    }

    fun v(tag: String?, message: String) {
        var tag = tag
        if (LEVEL <= VERBOSE) {
            val stackTraceElement = Thread.currentThread().stackTrace[4]
            if (TextUtils.isEmpty(tag)) {
                tag = getDefaultTag(stackTraceElement)
            }
            Log.v(tag, getLogInfo(stackTraceElement) + message)
        }
    }

    fun d(message: String) {
        if (LEVEL <= DEBUG) {
            val stackTraceElement = Thread.currentThread().stackTrace[4]
            val tag = getDefaultTag(stackTraceElement)
            Log.d(tag, getLogInfo(stackTraceElement) + message)
        }
    }

    fun d(tag: String?, message: String) {
        var tag = tag
        if (LEVEL <= DEBUG) {
            val stackTraceElement = Thread.currentThread().stackTrace[4]
            if (TextUtils.isEmpty(tag)) {
                tag = getDefaultTag(stackTraceElement)
            }
            Log.d(tag, getLogInfo(stackTraceElement) + message)
        }
    }

    fun i(message: String) {
        if (LEVEL <= INFO) {
            val stackTraceElement = Thread.currentThread().stackTrace[4]
            val tag = getDefaultTag(stackTraceElement)
            Log.i(tag, getLogInfo(stackTraceElement) + message)
        }
    }

    fun i(tag: String?, message: String) {
        var tag = tag
        if (LEVEL <= INFO) {
            val stackTraceElement = Thread.currentThread().stackTrace[4]
            if (TextUtils.isEmpty(tag)) {
                tag = getDefaultTag(stackTraceElement)
            }
            Log.i(tag, getLogInfo(stackTraceElement) + message)
        }
    }

    fun w(message: String) {
        if (LEVEL <= WARN) {
            val stackTraceElement = Thread.currentThread().stackTrace[4]
            val tag = getDefaultTag(stackTraceElement)
            Log.w(tag, getLogInfo(stackTraceElement) + message)
        }
    }

    fun w(tag: String?, message: String) {
        var tag = tag
        if (LEVEL <= WARN) {
            val stackTraceElement = Thread.currentThread().stackTrace[4]
            if (TextUtils.isEmpty(tag)) {
                tag = getDefaultTag(stackTraceElement)
            }
            Log.w(tag, getLogInfo(stackTraceElement) + message)
        }
    }

    fun e(message: String) {
        if (LEVEL <= ERROR) {
            val stackTraceElement = Thread.currentThread().stackTrace[4]
            val tag = getDefaultTag(stackTraceElement)
            Log.e(tag, getLogInfo(stackTraceElement) + message)
        }
    }

    fun e(tag: String?, message: String) {
        var tag = tag
        if (LEVEL <= ERROR) {
            val stackTraceElement = Thread.currentThread().stackTrace[4]
            if (TextUtils.isEmpty(tag)) {
                tag = getDefaultTag(stackTraceElement)
            }
            Log.e(tag, getLogInfo(stackTraceElement) + message)
        }
    }

    /**
     * 获取默认的TAG名称. 比如在MainActivity.java中调用了日志输出. 则TAG为MainActivity
     */
    fun getDefaultTag(stackTraceElement: StackTraceElement): String? {
        val fileName = stackTraceElement.fileName
        val stringArray = fileName.split("\\.").toTypedArray()
        return stringArray[0]
    }

    /**
     * 输出日志所包含的信息
     */
    fun getLogInfo(stackTraceElement: StackTraceElement): String {
        val logInfoStringBuilder = StringBuilder()
        // 获取线程名
        val threadName = Thread.currentThread().name
        // 获取线程ID
        val threadID = Thread.currentThread().id
        // 获取文件名.即xxx.java
        val fileName = stackTraceElement.fileName
        // 获取类名.即包名+类名
        val className = stackTraceElement.className
        // 获取方法名称
        val methodName = stackTraceElement.methodName
        // 获取生日输出行数
        val lineNumber = stackTraceElement.lineNumber
        logInfoStringBuilder.append("[ ")
        logInfoStringBuilder.append("threadID=$threadID").append(SEPARATOR)
        logInfoStringBuilder.append("threadName=$threadName").append(SEPARATOR)
        logInfoStringBuilder.append("fileName=$fileName").append(SEPARATOR)
        //		logInfoStringBuilder.append("className=" + className).append(SEPARATOR);
        logInfoStringBuilder.append("methodName=$methodName").append(SEPARATOR)
        logInfoStringBuilder.append("lineNumber=$lineNumber")
        logInfoStringBuilder.append(" ] ")
        return logInfoStringBuilder.toString()
//		return "";
    }

}