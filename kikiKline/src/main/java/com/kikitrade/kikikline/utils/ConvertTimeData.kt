package com.kikitrade.kikikline.utils

import com.kikitrade.kikikline.config.KikiTabBarType
import com.kikitrade.kikikline.utils.DateUtil.Companion.getCurrentTime
import com.kikitrade.kikikline.utils.DateUtil.Companion.timeStringLong
import com.kikitrade.kikikline.view.ChartViewContainerView.Companion.HOUR
import com.kikitrade.kikikline.view.ChartViewContainerView.Companion.MINUTE
import com.kikitrade.kikikline.view.TimeData
import java.util.*

/**
 * @Author:         peter.Qin@kikitrade
 * @CreateDate:     2022/1/8 2:32 下午
 */
class ConvertTimeData {

    companion object {

        @JvmStatic
        fun getYearTimeData(timeStamp: Long): TimeData {

            val timeShow = DateUtil.getCurrentYearStartTime(timeStamp.toString()).time
            return TimeData(timeShow == timeStamp, timeShow)
        }


        @JvmStatic
        fun getMonthTimeData(timeStamp: Long): TimeData {
            val timeShow = DateUtil.getCurrentMonthStartTimes(timeStamp.toString()).time
            return TimeData(timeStamp == timeShow, timeShow)
        }

        /**
         *  我们定每周的周一为第一天
         *
         */
        @JvmStatic
        fun getWeekTimeData(timeStamp: Long): TimeData {

            val time = DateUtil.getCurrentWeekStartTimes(timeStamp.toString()).time
            val isAdd = timeStamp == time

            return TimeData(isAdd, time)
        }


        @JvmStatic
        fun getDayTimeData(timeStamp: Long): TimeData {
//            val lastDay = getCurrentTime(lastTime, DAY)
//            val currentDay = getCurrentTime(timeStamp, DAY)
//            // 根据是否是0点 && 并且判断当前天数是否大于上个数据的天数  是否是添加
//            val currentHour = getCurrentTime(timeStamp, HOUR)
//            val isAdd =
//                BigDecimal(currentHour).compareTo(BigDecimal("0")) == 0 && currentDay > lastDay
            val timeShow = DateUtil.getTodayStartTime(timeStamp.toString()).time
            return TimeData(timeStamp == timeShow, timeStamp)
        }

        @JvmStatic
        fun getHourTimeData(barType: KikiTabBarType, timeStamp: Long, lastTime: Long): TimeData? {
            val currentHour = getCurrentTime(timeStamp, HOUR)
            val lastHour = getCurrentTime(lastTime, HOUR)
            // BigDecimal 取 余数
            val remainder = currentHour % barType.getTimeRepeat()
            // 先格式化成小时
            val isAdd =
                remainder == 0 && timeStamp - lastTime >= barType.getTimeRepeat() * 60 * 60 * 1000L
            return when (timeStamp > lastTime) {
                true -> {
                    val time = timeStamp - remainder * 60 * 60 * 1000
                    TimeData(isAdd, time)
                }
                else -> {
                    null
                }
            }
        }

        @JvmStatic
        fun getMinuteTimeData(
            barType: KikiTabBarType,
            currentTime: Long,
            lastTime: Long
        ): TimeData? {
            val lastMinute = getCurrentTime(lastTime, MINUTE)
            val currentMinute = getCurrentTime(currentTime, MINUTE)
            val remainder = currentMinute % barType.getTimeRepeat()
            val isAdd =
                remainder == 0 && currentTime - lastTime >= barType.getTimeRepeat() * 60 * 1000L
            return when (currentTime >= lastTime) {
                true -> {
                    // 当前时间- 差值 = 显示时间
                    val time = currentTime - remainder * 60 * 1000
                    TimeData(isAdd, time)
                }
                else -> {
                    null
                }
            }
        }


        @JvmStatic
        fun main(args: Array<String>) {
            val time = 1641628500000

            val day = DateUtil.getTodayStartTime(time.toString()).time

            print(
                " day 格式化--->${DateUtil.dateToString(Date(time))} 起始时间时间戳-->$day - 格式挂-->${
                    DateUtil.dateToString(Date(day))
                }"
            )

            val week = DateUtil.getCurrentWeekStartTimes(time = time.toString()).time

            print(" week 起始--》${DateUtil.dateToString(Date(week))}")

            val month = DateUtil.getCurrentMonthStartTimes(time.toString()).time

            print(" month 起始--》${DateUtil.dateToString(Date(month))}")

            val year = DateUtil.getCurrentYearStartTime(time.toString()).time

            print(" year 起始--》${DateUtil.dateToString(Date(year))}")

            val currentTime = "2022-03-01 23:00:00"
            val lastTime = "2022-03-02 00:00:00"
            val currentTimeNum = getCurrentTime(timeStringLong(currentTime), HOUR)
            val lastTimeNum = getCurrentTime(timeStringLong(lastTime), HOUR)

            println("currentTimeNum--->$currentTimeNum  -余数-->${currentTimeNum % 15}")
            println("lastTimeNum--->$lastTimeNum  -余数-->${lastTimeNum % 15}")
            println(" 相减 --->${timeStringLong(currentTime) - timeStringLong(lastTime)}")
        }

    }
}