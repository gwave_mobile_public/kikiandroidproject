package com.kikitrade.kikikline.utils

import android.util.Log
import com.kikitrade.kikikline.network.RetryIntercept
import com.kikitrade.kikikline.view.ChartViewContainerView
import okhttp3.Callback
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.Request
import java.util.concurrent.TimeUnit

object NetworkRequestUtil {


    private const val READ_TIMEOUT = 100L
    private const val CONNECT_TIMEOUT = 60L
    private const val WRITE_TIMEOUT = 60L

    @JvmStatic
    fun fetchCloudData(urlString: String, callback: Callback) {
        Log.d(ChartViewContainerView.TAG, "fetchCloudData urlString:$urlString")
        OkHttpClient.Builder().apply {
            //读取超时
            readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            //连接超时
            connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            //写入超时
            writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            // 加入重试机制 重试1+2 = 3次
            addInterceptor(RetryIntercept(2))
            //自定义连接池最大空闲连接数和等待时间大小，否则默认最大5个空闲连接
            connectionPool(ConnectionPool(32, 5, TimeUnit.MINUTES))
            val request = Request.Builder().url(urlString).build()
            build().newCall(request).enqueue(callback)
        }
    }

}

