package com.kikitrade.kikikline.model

/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/11/22 at 4:45 下午
 * @Description:
 */
data class KChatViewBaseBean(
    val klines:MutableList<KChartNewBean>?,
    val timeTypes:MutableList<String>?
)
