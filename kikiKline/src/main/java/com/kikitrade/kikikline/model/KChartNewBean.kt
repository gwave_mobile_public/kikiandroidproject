package com.kikitrade.kikikline.model

import android.util.Log
import androidx.annotation.Keep
import com.kikitrade.klinelib.model.KLineEntity
import java.math.BigDecimal

/**
 *
 * @Author:         peter Qin
 * @CreateDate:     2021/12/7 10:55 上午
 */
@Keep
data class KChartNewBean(
    var closeTime: Long,
    var open: BigDecimal,
    var close: BigDecimal?,
    var high: BigDecimal,
    var low: BigDecimal,
    var volume: BigDecimal?,
    var timeFrame: String?
) : KLineEntity() {

    override fun getDate(): Long = closeTime

    override fun getOpenPrice(): Float = open.toFloat()

    override fun getHighPrice(): Float = high.toFloat()

    override fun getLowPrice(): Float = low.toFloat()

    override fun getClosePrice(): Float {
        return close?.toFloat()?:0.0f
    }

    override fun getVolume(): Float = volume?.toFloat() ?: 0.0f
}