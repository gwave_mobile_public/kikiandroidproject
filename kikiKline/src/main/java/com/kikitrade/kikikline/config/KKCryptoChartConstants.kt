package com.kikitrade.kikikline.config

object KKCryptoChartConstants {
    const val KK_BASE_PROD_URL = "https://api.kikitrade.com/v1/market/klines/init"
    const val KK_BASE_BETA_URL = "https://api.beta.dipbit.xyz/v1/market/klines/init"
    const val WB_BETA_URL = "wss://wstest.kikitrade.com:8081"
    const val WB_PROD_URL = "ws://ws.kikitrade.com:8080"

    const val KK_BASE_DATA_KLINE_DEV_URL="http://api.dev.dipbit.xyz/v1/market/trends/"
    const val KK_BASE_DATA_KLINE_BETA_URL="http://api.beta.dipbit.xyz/v1/market/trends/"
    const val KK_BASE_DATA_KLINE_PROD_URL="https://api.kikitrade.com/v1/market/trends/"

}