package com.kikitrade.kikikline.config

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.kikitrade.kikikline.extensions.getScale

import kotlin.Throws
import org.json.JSONArray
import com.kikitrade.kikikline.network.KKNetworkCallback
import com.kikitrade.kikikline.model.*
import com.kikitrade.kikikline.network.KKCryptoChartInitApi
import com.kikitrade.kikikline.utils.I18NUtil
import com.kikitrade.kikikline.utils.NetworkRequestUtil
import com.kikitrade.kikikline.view.ChartViewContainerView
import org.json.JSONException
import org.json.JSONObject
import com.litesuits.common.utils.RandomUtil
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException
import java.lang.NullPointerException
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Exception

class ChartManager private constructor() {


    companion object {
        val INSTANCE: ChartManager by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            ChartManager()
        }


        const val CHART_TIME_ON_ONE_MINUTE = "chart_time_one_minute"

        const val CHART_TIME_ON_FIVE_MINUTE = "chart_time_five_minutes"

        const val CHART_TIME_ON_FIFTEEN_MINUTE = "chart_time_fifteen_minutes"

        const val CHART_TIME_ON_THIRTY_MINUTE = "chart_time_thirty_minutes"

        const val CHART_TIME_ON_ONE_HOUR = "chart_time_one_hour"

        const val CHART_TIME_ON_FOUR_HOUR = "chart_time_four_hours"

        const val CHART_TIME_ON_ONE_DAY = "chart_time_one_day"

        const val CHART_TIME_ON_ONE_WEEK = "chart_time_one_week"

        const val CHART_TIME_ON_ONE_MONTH = "chart_time_one_month"

        const val CHART_TIME_ON_ONE_YEAR = "chart_time_one_year"

        const val CHART_TAB_BAR_MORE = "chart_tab_bar_more"

        const val CHART_TAB_BAR_INDICATOR = "chart_tab_bar_indicator"

        const val CHART_MAIN = "chart_main"

        const val CHART_SUB = "chart_sub"

        const val CHART_DESCRIPTION_TIME = "chart_description_time"

        const val CHART_DESCRIPTION_OPEN = "chart_description_open"

        const val CHART_DESCRIPTION_CLOSE = "chart_description_close"

        const val CHART_DESCRIPTION_HIGH = "chart_description_high"

        const val CHART_DESCRIPTION_LOW = "chart_description_low"

        const val CHART_DESCRIPTION_ONEVOL = "chart_description_oneVol"

        const val CHART_DESCRIPTION_RISE_AND_FALL = "chart_description_riseAndFall"

        const val CHART_DESCRIPTION_RISE_AND_FALL_RATE = "chart_description_riseAndFallRate"

        const val CHART_TAB_BAR_TRIANGLE = "chart_tab_bar_triangle"

        const val CHAT_DATA_DES_RISE_AND_FALL = "chart_data_description_riseAndFall"


    }

    var config: CryptoChartGlobalConfig? = null

    var timeType: KikiTabBarType? = null

    var mainIndicator: KikiMainIndicator? = null

    var otherIndicator: MutableList<KikiOtherIndicator>? = null

    val deviceId by lazy {
        RandomUtil.getRandom(Int.MAX_VALUE)
    }


    fun fetchCloudData(callback: KKNetworkCallback) {
        config?.let {

            try {
                NetworkRequestUtil.fetchCloudData(
                    setUpRequestApi(it.coinCode ?: "", it.timeType, it.getEnv()),
                    object : Callback {
                        override fun onFailure(call: Call, e: IOException) {
                            callback.onError(e)
                        }

                        @Throws(IOException::class)
                        override fun onResponse(call: Call, response: Response) {
                            if (response.isSuccessful) {
                                val body = response.body
                                if (body != null) {
                                    val responseString = body.string()
                                    try {
                                        val jsonArray = parseResponse(responseString)
                                        if (jsonArray != null) {
                                            callback.onFinish(
                                                handleChartsData(jsonArray, JSONArray(), 6)
                                            )
                                            return
                                        }
                                    } catch (e: Exception) {
                                        callback.onError(e)
                                    }
                                    return
                                }
                                val e = NullPointerException()
                                callback.onError(e)
                                return
                            }
                            val e = Exception()
                            callback.onError(e)
                        }
                    })
            } catch (exception: Exception) {
                exception.printStackTrace()
                Log.e(ChartViewContainerView.TAG, " --fetchCloudData--->${exception.message}")
            }

        }

    }

    fun requestDataKline(callback: KKNetworkCallback) {

        config?.let {
            NetworkRequestUtil.fetchCloudData(
                setTrendsRequestApi(it.coinCode ?: "", it.timeType, it.getEnv()),
                object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        callback.onError(e)
                    }

                    override fun onResponse(call: Call, response: Response) {
                        if (response.isSuccessful) {
                            val body = response.body
                            if (body != null) {
                                val responseString = body.string()
                                try {
                                    val jsonArray = parseResponse(responseString)
                                    val timeTypeArray = parseTimeTypes(responseString)
                                    if (jsonArray != null && timeTypeArray != null) {
                                        callback.onFinish(
                                            handleChartsData(jsonArray, timeTypeArray, 5)
                                        )
                                    }
                                } catch (e: Exception) {
                                    callback.onError(e)
                                }
                            }
                        }
                    }

                }
            )
        }


    }

    fun getTimeType(time: String?): KikiTabBarType {
        if (time == null || time.isEmpty()) {
            return KikiTabBarType.OneMinute
        }
        for (type in KikiTabBarType.values()) {
            if (type.getTime() == time) {
                return type
            }
        }
        return KikiTabBarType.OneMinute
    }

    fun getMainIndicator(des: String?): KikiMainIndicator? {
        if (des.isNullOrEmpty()) {
            return null
        }
        KikiMainIndicator.values().forEach { main ->
            if (des == main.getMainIndicatorDes()) {
                return main
            }
        }
        return null
    }

    fun getOtherIndicator(des: String?): KikiOtherIndicator? {
        if (des.isNullOrEmpty()) {
            return null
        }
        KikiOtherIndicator.values().forEach { other ->
            if (des == other.getOtherIndicatorDes()) {
                return other
            }
        }
        return null
    }

    fun getDeviceId(): String {
        return RandomUtil.getRandom(Int.MAX_VALUE).toString()
    }


    val defaultLocale: String
        get() = "hk"

    fun getLocale(locale: KKCryptoChartLocale?): String {
        return when (locale) {
            KKCryptoChartLocale.English -> "en"
            KKCryptoChartLocale.SimplifiedChinese -> "zh"
            KKCryptoChartLocale.TraditionalChinese -> "hk"
            else -> "hk"
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getTimeFormat(date: Date, timeType: KikiTabBarType?): String {
        if (timeType == null) {
            return SimpleDateFormat("MM/dd HH:mm").format(date)
        }
        return when (timeType) {
            KikiTabBarType.OneMinute, KikiTabBarType.FiveMinutes, KikiTabBarType.FifteenMinutes, KikiTabBarType.ThirtyMinutes, KikiTabBarType.OneHour, KikiTabBarType.FourHours -> {
                SimpleDateFormat("MM/dd HH:mm").format(date)
            }
            KikiTabBarType.OneDay, KikiTabBarType.OneMonth, KikiTabBarType.OneWeek -> {
                SimpleDateFormat("yyyy/MM/dd").format(date)
            }
            else -> {
                SimpleDateFormat("yyyy").format(date)
            }
        }
    }


    fun getTimeTitle(timeType: KikiTabBarType?, locale: String?, context: Context?): String {

        val title = when (timeType) {
            KikiTabBarType.OneMinute -> I18NUtil.getString(
                CHART_TIME_ON_ONE_MINUTE,
                locale,
                context
            )
            KikiTabBarType.FiveMinutes -> I18NUtil.getString(
                CHART_TIME_ON_FIVE_MINUTE,
                locale,
                context
            )
            KikiTabBarType.FifteenMinutes -> I18NUtil.getString(
                CHART_TIME_ON_FIFTEEN_MINUTE,
                locale,
                context
            )
            KikiTabBarType.ThirtyMinutes -> I18NUtil.getString(
                CHART_TIME_ON_THIRTY_MINUTE,
                locale,
                context
            )
            KikiTabBarType.OneHour -> I18NUtil.getString(
                CHART_TIME_ON_ONE_HOUR,
                locale,
                context
            )
            KikiTabBarType.FourHours -> I18NUtil.getString(
                CHART_TIME_ON_FOUR_HOUR,
                locale,
                context
            )
            KikiTabBarType.OneDay -> I18NUtil.getString(
                CHART_TIME_ON_ONE_DAY,
                locale,
                context
            )
            KikiTabBarType.OneWeek -> I18NUtil.getString(
                CHART_TIME_ON_ONE_WEEK,
                locale,
                context
            )
            KikiTabBarType.OneMonth -> I18NUtil.getString(
                CHART_TIME_ON_ONE_MONTH,
                locale,
                context
            )
            KikiTabBarType.OneYear -> I18NUtil.getString(
                CHART_TIME_ON_ONE_YEAR,
                locale,
                context
            )
            else -> I18NUtil.getString(CHART_TIME_ON_FIVE_MINUTE, locale, context)
        }
        return title
    }

    private fun setUpRequestApi(
        coinType: String, timeType: String,
        environmentType: KKCryptoChartEnvironment
    ): String {
        val api = KKCryptoChartInitApi()
        api.symbol = coinType
        api.resolution = timeType
        api.environment = environmentType
        api.from = setUpRequestApiTime(getTimeType(timeType))[0]
        api.to = setUpRequestApiTime(getTimeType(timeType))[1]
        return api.completedURL
    }

    private fun setTrendsRequestApi(
        coinType: String, timeType: String,
        environmentType: KKCryptoChartEnvironment
    ): String {
        val api = KKCryptoChartInitApi()
        api.symbol = coinType
        api.resolution = timeType
        api.environment = environmentType
        api.from = setUpRequestApiTime(getTimeType(timeType))[0]
        api.to = setUpRequestApiTime(getTimeType(timeType))[1]
        return api.trendsCompletedUrl
    }

    private fun setUpRequestApiTime(timeType: KikiTabBarType): Array<String> {
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        val end = calendar.time
        val endTimestamp = end.time.toString()
        when (timeType) {
            KikiTabBarType.OneMinute -> calendar.add(Calendar.HOUR, -16)
            KikiTabBarType.FiveMinutes -> calendar.add(Calendar.HOUR, -80)
            KikiTabBarType.FifteenMinutes -> calendar.add(Calendar.HOUR, -240)
            KikiTabBarType.ThirtyMinutes -> calendar.add(Calendar.HOUR, -480)
            KikiTabBarType.OneHour -> calendar.add(Calendar.DATE, -40)
            KikiTabBarType.FourHours -> calendar.add(Calendar.DATE, -160)
            KikiTabBarType.OneDay -> calendar.add(Calendar.YEAR, -3)
            KikiTabBarType.OneWeek, KikiTabBarType.OneMonth, KikiTabBarType.OneYear -> calendar.add(
                Calendar.YEAR, -20
            )
            else -> calendar.add(Calendar.HOUR, -8)
        }
        val startTimestamp = calendar.timeInMillis.toString()
        return arrayOf(startTimestamp, endTimestamp)
    }

    @Throws(JSONException::class)
    private fun parseResponse(response: String): JSONArray? {
        val responseObject = JSONObject(response)
        val status = responseObject.getBoolean("success")
        if (status) {
            val obj = responseObject.getJSONObject("obj")
            return obj.getJSONArray("bars")
        }
        return null
    }

    private fun parseTimeTypes(response: String): JSONArray? {
        val responseObject = JSONObject(response)
        val status = responseObject.getBoolean("success")
        if (status) {
            val obj = responseObject.getJSONObject("obj")
            return obj.getJSONArray("klineTypes")
        }
        return null
    }

    @Throws(JSONException::class)
    private fun handleChartsData(
        array: JSONArray, timeTypeArray: JSONArray, length: Int
    ): KChatViewBaseBean {
        val chartModels = ArrayList<KChartNewBean>()
        for (i in 0 until array.length()) {
            val bar = array.getString(i)
            val values = bar.split(",").toTypedArray()
            if (values.size >= length) {
                val open = when (values[2].isEmpty()) {
                    true -> BigDecimal("0")
                    else -> {
                        BigDecimal(
                            BigDecimal(values[2]).getScale(
                                config?.coinPrecision?.toInt() ?: 0
                            )
                        )
                    }
                }
                val close = when (values[1].isEmpty()) {
                    true -> BigDecimal("0")
                    else -> BigDecimal(
                        BigDecimal(values[1]).getScale(
                            config?.coinPrecision?.toInt() ?: 0
                        )
                    )
                }
                val high = when (values[3].isEmpty()) {
                    true -> BigDecimal("0")
                    else -> BigDecimal(
                        BigDecimal(values[3]).getScale(
                            config?.coinPrecision?.toInt() ?: 0
                        )
                    )
                }
                val low = when (values[4].isEmpty()) {
                    true -> BigDecimal("0")
                    else -> BigDecimal(
                        BigDecimal(values[4]).getScale(
                            config?.coinPrecision?.toInt() ?: 0
                        )
                    )
                }
                val volume = when (length) {
                    5 -> BigDecimal("0")
                    6 -> when (values[5].isEmpty()) {
                        true -> BigDecimal("0")
                        else -> BigDecimal(values[5]).setScale(
                            config?.tradeVolumePrecision?.toInt() ?: 0,RoundingMode.HALF_UP
                        )
                    }
                    else -> BigDecimal("0")
                }
                val chart = KChartNewBean(
                    values[0].toLong() * 1000, open, close, high = high, low, volume, ""
                )
                chartModels.add(chart)
            }
        }
        chartModels.sortBy {
            it.closeTime
        }
        val timeTypes = mutableListOf<String>()
        for (i in 0 until timeTypeArray.length()) {
            val time = timeTypeArray.getString(i)
            timeTypes.add(time)
        }
        return KChatViewBaseBean(chartModels, timeTypes)
    }
}


data class DataKlineTab(val name: String, val tab: KikiTabBarType)

enum class KikiTabBarType(
    private val time: String,
    private val resolution: String,
    private val timeRepeat: Int
) {
    OneMinute("1", "P1m", 1),
    FiveMinutes("5", "P5m", 5),
    FifteenMinutes("15", "P15m", 15),
    ThirtyMinutes("30", "P30m", 30),
    OneHour("60", "P1h", 1),
    FourHours("4H", "P4h", 4),
    OneDay("D", "P1d", 1),
    OneWeek("W", "P1w", 1),
    OneMonth("M", "P1M", 1),
    OneYear("12M", "P1y", 1);

    fun getTime(): String = time

    fun getResolution(): String = resolution

    fun getTimeRepeat(): Int = timeRepeat
}

enum class KikiMainIndicator(private val des: String) {
    MA("MA"),
    BOLL("BOLL"),
    EMA("EMA"),
    TD("TD");

    fun getMainIndicatorDes(): String = des
}

enum class KikiOtherIndicator(private val des: String) {
    MACD("MACD"),
    KDJ("KDJ"),
    RSI("RSI"),
    WR("WR");

    fun getOtherIndicatorDes(): String = des
}

enum class KikiShowType(private val type: String) {
    NormalKline("normal_kline"),// 正常模式，有 socket
    DataKline("data_kline");// data 模式，无 socket

    fun getKikiShowType(): String = type
}

enum class UnitShowType(private val type: String) {
    NormalUnit("normal_unit"),
    DecimalUnit("BigDecimal");

    fun getUnitType(): String = type
}