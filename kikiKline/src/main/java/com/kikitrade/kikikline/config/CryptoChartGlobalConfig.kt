package com.kikitrade.kikikline.config

import androidx.annotation.Keep

/**
 * 图表初始化设置类
 */

@Keep
class CryptoChartGlobalConfig @JvmOverloads constructor(
    var locale: String,
    var timeType: String,
    var coinPrecision: String,
    var environment: String?,
    var coinCode: String?,
    var jwtToken: String ="",
    var fromSymbol: String = "",
    var tradeVolumePrecision: String =""
) {

    @JvmField
    var needUnitFormat:Boolean=false

    companion object {


        fun setEnvironment(env: KKCryptoChartEnvironment,coinCode: String,coinPrecision: String): CryptoChartGlobalConfig {
            return when (env) {
                KKCryptoChartEnvironment.Dev->{
                    CryptoChartGlobalConfig("kk", "D", coinPrecision, "dev", coinCode, "", "", "5")
                }
                KKCryptoChartEnvironment.Beta -> {
                    CryptoChartGlobalConfig("hk", "D", coinPrecision, "beta", coinCode, "", "", "5")
                }
                else -> {
                    CryptoChartGlobalConfig("kk", "15", coinPrecision, "pro", coinCode, "", "", "5")
                }
            }
        }
    }


    fun getLocal(): KKCryptoChartLocale {
        return when (locale) {
            "zh" -> KKCryptoChartLocale.SimplifiedChinese
            "en" -> KKCryptoChartLocale.English
            "hk" -> KKCryptoChartLocale.TraditionalChinese
            else -> KKCryptoChartLocale.SimplifiedChinese
        }
    }

    fun getEnv(): KKCryptoChartEnvironment {
        return when (environment) {
            "pro", "prod", "prodgreen" -> KKCryptoChartEnvironment.Product
            "beta" -> KKCryptoChartEnvironment.Beta
            "dev" ->KKCryptoChartEnvironment.Dev
            else -> KKCryptoChartEnvironment.Beta
        }
    }

    fun getLocalizableStringWithKey(key: String?): String {
        return if (key == null || key.isEmpty()) {
            ""
        } else ""
    }

}

enum class KKCryptoChartLocale {
    SimplifiedChinese, TraditionalChinese, English
}

enum class KKCryptoChartEnvironment {
    Beta, Product, ProdGreen,Dev,
}