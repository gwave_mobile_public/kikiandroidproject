package com.kiki.kikiandroidproject
import Audience
import android.app.Application
import android.graphics.Color
import android.util.Log
import com.kiki.commonlib.utils.ActivityUtils

/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/10/27 at 10:03 上午
 * @Description:
 */

class KApplication :Application(){

    val TAG="KApplication"
    override fun onCreate() {
        super.onCreate()
        ActivityUtils.init(this)
//        Cat.newInstance(this)?.apply {
//            seat(Seat.TOP_RIGHT)
//            setInterval(1000)
//            color(Color.WHITE)
//            size(24f)
//            alpha(0.5f)
//            setCustomControl(true)
//            listener(object :Audience{
//                override fun heartbeat(fps: Double) {
//                    Log.i(TAG, "heartbeat----->${ActivityUtils.getTopActivity()?.localClassName}")
//                }
//            })
//        }
    }
}