package com.kiki.kikiandroidproject

import android.annotation.SuppressLint
import android.os.SystemClock
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kiki.commonlib.BaseActivity
import com.kiki.kikiandroidproject.databinding.ActivityFpsactivityBinding

class FPSActivity : BaseActivity() {

    private lateinit var binding:ActivityFpsactivityBinding


    override fun init() {
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.isItemPrefetchEnabled=false

        binding.recy.apply {
            adapter = object : RecyclerView.Adapter<holder>() {


                override fun getItemCount(): Int {
                    return 1000
                }

                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
                    return holder(TextView(this@FPSActivity))
                }

                @SuppressLint("SetTextI18n")
                override fun onBindViewHolder(holder: holder, position: Int) {

                    if (position > 10) SystemClock.sleep(300)
                    (holder.itemView as TextView).height = 500
                    holder.itemView.text = "位置 $position"
                }
            }
            layoutManager = linearLayoutManager
        }
    }

    override fun getLayoutView(): View {
        binding = ActivityFpsactivityBinding.inflate(layoutInflater)
        return binding.root
    }


    class holder constructor(private val view:View) :RecyclerView.ViewHolder(view){

    }

    override fun onStart() {
        super.onStart()
        Cat.startCollect(this.localClassName)
    }

    override fun onPause() {
        super.onPause()
        Cat.finishCollect(this.localClassName)
    }
}