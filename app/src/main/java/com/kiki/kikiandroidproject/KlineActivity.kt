package com.kiki.kikiandroidproject


import android.content.Intent
import android.util.Log
import android.view.View

import com.kiki.commonlib.BaseActivity
import com.kiki.kikiandroidproject.databinding.ActivityKlineBinding
import com.kikitrade.kikikline.config.CryptoChartGlobalConfig
import com.kikitrade.kikikline.config.KKCryptoChartEnvironment

class KlineActivity : BaseActivity(), View.OnClickListener {

    lateinit var binding: ActivityKlineBinding
    val currentCode = "BTC_USDT"

    override fun init() {

        binding.apply {
            container.config(
                CryptoChartGlobalConfig.setEnvironment(
                    KKCryptoChartEnvironment.Beta,
                    coinCode = currentCode,
                    coinPrecision = "1"
                )
            )
            currentCoin.text = currentCode
            btcBtn.setOnClickListener(this@KlineActivity)
            shibiBtn.setOnClickListener(this@KlineActivity)
            ethBtn.setOnClickListener(this@KlineActivity)
            avaxBtn.setOnClickListener(this@KlineActivity)

        }

        setLeftTitle("kikitrade KLine")
    }

    override fun getLayoutView(): View {
        binding = ActivityKlineBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btc_btn -> {
                binding.apply {
                    container.config(
                        CryptoChartGlobalConfig.setEnvironment(
                            KKCryptoChartEnvironment.Beta,
                            "BTC_USDT",
                            "2"
                        )
                    )
                    currentCoin.text = "BTC_USDT"
                }
            }
            R.id.eth_btn -> {
                binding.apply {

                    startActivity(Intent(this@KlineActivity, TestMainActivity::class.java))
//                    container.config(CryptoChartGlobalConfig.setEnvironment(KKCryptoChartEnvironment.Beta, "ETH_USDT", "2"))
//                    currentCoin.text = "ETH_USDT"
                }
            }
            R.id.shibi_btn -> {
                binding.apply {
                    container.config(
                        CryptoChartGlobalConfig.setEnvironment(
                            KKCryptoChartEnvironment.Beta,
                            "SHIB_USDT",
                            "8"
                        )
                    )
                    currentCoin.text = "SHIB_USDT"
                }
            }
            R.id.avax_btn -> {
                binding.apply {
                    container.config(
                        CryptoChartGlobalConfig.setEnvironment(
                            KKCryptoChartEnvironment.Beta,
                            "AVAX_USDT",
                            "3"
                        )
                    )
                    currentCoin.text = "AVAX_USDT"
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
//        Cat.startCollect(this.localClassName)
    }

    override fun onPause() {
        super.onPause()
//        Cat.finishCollect(this.localClassName)
    }
}