package com.kiki.kikiandroidproject

import android.content.Intent
import android.util.Log
import android.view.View
import com.kiki.commonlib.BaseActivity
import com.kiki.kikiandroidproject.databinding.ActivityMainBinding
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode

class MainActivity : BaseActivity(), View.OnClickListener {


    lateinit var binding: ActivityMainBinding
    override fun init() {
        binding.apply {
            clickKline.setOnClickListener(this@MainActivity)
            clickDataKline.setOnClickListener(this@MainActivity)
        }

        setLeftTitle("kiki Native")
        binding.clickKline.typeface= null
    }

    override fun getLayoutView(): View {
        binding = ActivityMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.click_kline -> {
                startActivity(Intent(this, KlineActivity::class.java))
            }
            R.id.click_data_kline -> {
                startActivity(Intent(this, DataKlineActivity::class.java))
            }
        }
    }

    override fun showToolBar(): Boolean = false

    override fun onStart() {
        super.onStart()
        Cat.startCollect(this.localClassName)
    }

    override fun onPause() {
        super.onPause()
        Cat.finishCollect(this.localClassName)
    }
}

class ParameterNotMatchException() : Exception("parameter not matched")