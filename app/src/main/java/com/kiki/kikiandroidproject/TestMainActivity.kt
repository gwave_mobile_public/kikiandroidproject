package com.kiki.kikiandroidproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.kiki.commonlib.BaseActivity
import com.kiki.kikiandroidproject.databinding.ActivityTestMainBinding
import com.kikitrade.kikikline.config.CryptoChartGlobalConfig
import com.kikitrade.kikikline.config.KKCryptoChartEnvironment

class TestMainActivity : BaseActivity() {

    lateinit var binding:ActivityTestMainBinding

    override fun init() {
        binding.container.config(CryptoChartGlobalConfig.setEnvironment(KKCryptoChartEnvironment.Beta, coinCode = "ETH_USDT", coinPrecision = "2"))
    }

    override fun getLayoutView(): View {
        binding = ActivityTestMainBinding.inflate(layoutInflater)
        return binding.root
    }
}