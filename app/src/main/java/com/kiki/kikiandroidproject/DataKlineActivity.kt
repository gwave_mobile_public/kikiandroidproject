package com.kiki.kikiandroidproject

import android.util.Log
import android.view.View
import com.kiki.commonlib.BaseActivity
import com.kiki.kikiandroidproject.databinding.ActivityDataKlineBinding
import com.kikitrade.kikikline.config.CryptoChartGlobalConfig
import com.kikitrade.kikikline.config.KKCryptoChartEnvironment
import com.kikitrade.kikikline.utils.Utils
import java.math.BigDecimal

class DataKlineActivity : BaseActivity() {

    lateinit var binding: ActivityDataKlineBinding

    val TAG = "DataKlineActivity"

    //MSP_ETH
    override fun init() {
        setLeftTitle("show data kline")
        val config =CryptoChartGlobalConfig.setEnvironment(
            KKCryptoChartEnvironment.Beta,
            coinCode = "TVL_ETH",
            coinPrecision = "4"
        )
        config.needUnitFormat=true
        binding.container.config(
            config
        )
//        Log.i(TAG, Utils.formatNum(BigDecimal("1999"),3))
//        Log.i(TAG, Utils.formatNum(BigDecimal("19990"),4))
//        Log.i(TAG, Utils.formatNum(BigDecimal("1999999"),3))
//        Log.i(TAG, Utils.formatNum(BigDecimal("199999999")))
//        Log.i(TAG, Utils.formatNum(BigDecimal("199999999999")))
    }

    override fun getLayoutView(): View {
        binding = ActivityDataKlineBinding.inflate(layoutInflater)
        return binding.root
    }
}