/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/10/21 at 4:18 下午
 * @Description:
 */

interface LifecycleCallbackListener {

    fun onAppForeground()

    fun onAppBackground()

}