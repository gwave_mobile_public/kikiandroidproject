import android.app.Application
import android.util.Log
import android.view.Choreographer
import kotlin.math.min

/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/10/21 at 4:23 下午
 * @Description:
 */


class MyChoreographer @JvmOverloads constructor() : Choreographer.FrameCallback {

    private val TAG = "MyChoreographer"
    private var choreographer: Choreographer = Choreographer.getInstance()
    var frameStartTime = 0L
    private var framesRendered = 0
    private var listeners = mutableListOf<Audience>()
    var interval: Int = 500
    var className: String? = null
    var startTime: Long = 0
    var costTime: Long = 0
    private val dataList = mutableListOf<Int>()
    var mLastFrameTime = 0L
    var mFrameCount = 0L
    var application: Application? = null

    // 每帧标准耗时
    val standard = 16.67

    fun start(className: String?) {
        // 作为页面切换的 开始 标志
        startTime = System.currentTimeMillis()
        dataList.clear()
        this.className = className
        choreographer.postFrameCallback(this)
        Log.i(TAG, "----start-----${dataList.size}")
    }

    fun stop(className: String?) {
        frameStartTime = 0
        framesRendered = 0
        choreographer.removeFrameCallback(this)

        if (className.isNullOrEmpty()) {
            return
        }
        application?.applicationContext?.let {
            if (this.className == className) {
                // 作为 页面结束的标志
                costTime = (System.currentTimeMillis() - startTime)
                val avgFps = dataList.sum() / (costTime / 1000)
                val collectItem = CollectItem(
                    className,
                    avgFps.toDouble(),
                    Utils.getSystemModel() ?: "",
                    Utils.getSystemVersion() ?: "",
                    "Android",
                    Utils.getVersionName(it) ?: "",
                    0,
                    costTime
                )

                Log.i(
                    TAG,
                    "stop: ------>$costTime,   求和-->${dataList.sum()}   平均值-->$avgFps   --->${dataList}"
                )
            }
        }

    }

    fun addListener(lis: Audience) {
        listeners.add(lis)
    }


    override fun doFrame(frameTimeNanos: Long) {
        if (mLastFrameTime == 0L) {
            mLastFrameTime = frameTimeNanos
        }
        //本次帧开始时间减去上次的时间除以100万，得到毫秒的差值
        val diff: Float = (frameTimeNanos - mLastFrameTime) / 1000000.0f
        //这里是500毫秒输出一次帧率
        //这里是500毫秒输出一次帧率
        if (diff > interval) {
            val fps = min(60.0f, (mFrameCount * 1000L) / diff)
            mFrameCount = 0
            mLastFrameTime = 0
//            Log.d("doFrame", "doFrame: $fps")
            listeners.forEach {
                it.heartbeat(fps.toDouble())
            }
            dataList.add(fps.toInt())
        } else {
            ++mFrameCount
        }
        choreographer.postFrameCallback(this)
    }
}