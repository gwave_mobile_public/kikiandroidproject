/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/10/27 at 3:57 下午
 * @Description:
 */

class CollectItem constructor(
    @JvmField var pageName: String,
    @JvmField var avgFps: Double,
    @JvmField var model: String,
    @JvmField var system: String,
    @JvmField var platform: String,
    @JvmField var version: String,
    @JvmField var faultTime: Int,
    @JvmField var totalTime: Long,
    @JvmField var createFps: String = "",
    @JvmField var refreshFps: String = "",
    @JvmField var scrollFps: String=""

)