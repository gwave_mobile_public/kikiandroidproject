import android.view.Gravity

/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/10/24 at 3:30 下午
 * @Description:
 */
enum class Seat( private val gravity: Int) {

    TOP_RIGHT(Gravity.TOP or  Gravity.END),
    TOP_LEFT(Gravity.TOP or Gravity.START),
    TOP_CENTER(Gravity.TOP or Gravity.CENTER_HORIZONTAL),

    RIGHT_CENTER(Gravity.END or Gravity.CENTER_VERTICAL),
    LEFT_CENTER(Gravity.START or Gravity.CENTER_VERTICAL),
    CENTER(Gravity.CENTER),

    BOTTOM_RIGHT(Gravity.BOTTOM or Gravity.END),
    BOTTOM_LEFT(Gravity.BOTTOM or Gravity.START),
    BOTTOM_CENTER(Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL);

    fun getGravity():Int = gravity
}