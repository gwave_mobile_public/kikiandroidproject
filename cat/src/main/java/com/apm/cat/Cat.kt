import android.app.Application
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.SurfaceControl
import android.view.View
import android.view.WindowManager
import android.widget.RelativeLayout
import android.widget.TextView
import com.apm.cat.R
import java.text.DecimalFormat

/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/10/21 at 4:58 下午
 * @Description:
 */

class Cat {

    companion object {
        private var program: Program? = null
        const val TAG = "Cat"
        var className: String? = null

        @JvmStatic
        fun newInstance(application: Application): Program? {
            program = Program()
            return program?.prepare(application)
        }

        @JvmStatic
        fun startCollect(className: String?) {
            this.className = className
            program?.startCollect()
        }

        @JvmStatic
        fun finishCollect(className: String?) {
            this.className = className
            program?.finishCollect()
        }
    }

    // kotlin 静态内部类
    class Program @JvmOverloads constructor() : LifecycleCallbackListener {


        val decimal = DecimalFormat("#' fps'")

        // 是否 自定义控制
        private var customControl = false

        private var metronome: MyChoreographer? = null

        private lateinit var params: WindowManager.LayoutParams

        var app: Application? = null

        private lateinit var wm: WindowManager

        private lateinit var stageView: View

        private lateinit var fpsText: TextView

        private val showSetting = true

        private var show = true
        private var isPlaying = false

        fun prepare(application: Application): Program {
            Log.i(TAG, "------prepare-------")
            metronome = MyChoreographer()
            params = WindowManager.LayoutParams()
            params.width = WindowManager.LayoutParams.WRAP_CONTENT
            params.height = WindowManager.LayoutParams.WRAP_CONTENT
            application.registerActivityLifecycleCallbacks(LifecycleListener(this))

            if (isOverlayApiDeprecated()) {
                params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            } else {
                params.type = WindowManager.LayoutParams.TYPE_TOAST
            }
            params.flags =
                (WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        or WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            params.format = PixelFormat.TRANSLUCENT
            params.gravity = Seat.BOTTOM_RIGHT.getGravity()
            params.x = 10

            app = application

            metronome?.application= app
            Log.i(TAG, "prepare: ")
            wm = application.getSystemService(Context.WINDOW_SERVICE) as WindowManager


            val inflater = LayoutInflater.from(app)
            stageView = inflater.inflate(R.layout.stage, RelativeLayout(app))
            fpsText = stageView.findViewById(R.id.takt_fps)
            listener(object : Audience {
                override fun heartbeat(fps: Double) {
                    fpsText.text = decimal.format(fps)
                }
            })

            return this
        }


        fun startCollect() {
            if (!hasOverlayPermission()) {
                if (showSetting) {
                    startOverlaySettingActivity()
                } else {
                    Log.w("takt", "Application has no Overlay permission")
                }
                return
            }

            metronome?.start(className)

            if (show && !isPlaying) {
                wm.addView(stageView, params)
                isPlaying = true
            }
        }

        fun finishCollect() {
            metronome?.stop(className)

            if (show && isPlaying) {
                wm.removeView(stageView)
                isPlaying = false
            }
        }

        // 设置采集的 频率
        fun setInterval(ms: Int): Program {
            metronome?.interval = ms
            return this
        }

        // 添加监听
        fun listener(audience: Audience): Program {
            metronome?.addListener(audience)
            return this
        }



        fun color(color: Int): Program {
            fpsText.setTextColor(color)
            return this
        }

        fun size(size: Float): Program {
            fpsText.textSize = size
            return this
        }

        fun setCustomControl(control: Boolean): Program {
            this.customControl = control
            return this
        }

        /*
     * alpha from = 0.0, to = 1.0
     */
        fun alpha(alpha: Float): Program {
            fpsText.alpha = alpha
            return this
        }

        fun hide(): Program {
            show = false
            return this
        }

        fun seat(seat: Seat): Program {
            params.gravity = seat.getGravity()
            return this
        }

        override fun onAppForeground() {
            if (!customControl) startCollect(className)
        }

        override fun onAppBackground() {
            if (!customControl) finishCollect(className)
        }

        private fun isOverlayApiDeprecated(): Boolean {
            return Build.VERSION.SDK_INT >= 26
        }


        private fun hasOverlayPermission(): Boolean {
            app?.let {
                return Build.VERSION.SDK_INT < 23 || Settings.canDrawOverlays(it)
            }
            return true
        }

        private fun startOverlaySettingActivity() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                app?.startActivity(
                    Intent(
                        Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + app?.packageName)
                    ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                )
            }
        }
    }


}