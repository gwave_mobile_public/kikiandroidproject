import android.app.Activity
import android.app.Application
import android.os.Bundle

/**
 * @author: peter.qin@kikitrade.com
 * @date: 2022/10/21 at 4:16 下午
 * @Description:
 */

class LifecycleListener @JvmOverloads constructor(private  val listener:LifecycleCallbackListener?)
    :Application.ActivityLifecycleCallbacks{

    var startedActivityCounter =0


    override fun onActivityCreated(p0: Activity, p1: Bundle?) {

    }

    override fun onActivityStarted(p0: Activity) {
        synchronized(this) {
            startedActivityCounter ++
            if (startedActivityCounter == 1 && listener !=null){
                listener.onAppForeground()
            }
        }
    }

    override fun onActivityStopped(p0: Activity) {
        startedActivityCounter--
        if (startedActivityCounter ==0 && listener!=null){
            listener.onAppBackground()
        }
    }

    override fun onActivityResumed(p0: Activity) {

    }

    override fun onActivityPaused(p0: Activity) {

    }



    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {

    }

    override fun onActivityDestroyed(p0: Activity) {

    }


}

